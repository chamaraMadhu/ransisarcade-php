<?php 
	include "include/head.php";
	include "include/navbar.php";
  include "include/shop_carousel.php";
?>

	
<!-- starts breadcrumb -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-light">
    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500">Home</a></li>
    <li class="breadcrumb-item"><a href="shop.php" style="color: #422C2F; font-weight: 500">shop</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php $category = $_GET['category']; echo $category ?></li>
  </ol>
</nav>
<!-- ends breadcrumb -->

<?php 
  include "include/sort_by.php";
  include "include/shop_side_bar.php";
?>

        <div class="col-md-9">

          <?php

            $get_product = "SELECT * FROM product  WHERE cat_name = '$category'";
            $run_product = mysqli_query($con,$get_product);


            while($res_product = mysqli_fetch_array($run_product)){

              $pro_id= $res_product['id'];
              $product_name = $res_product['name'];
              $pro_cat_name = $res_product['cat_name'];
              $status = $res_product['status'];
              $img_front = $res_product['image_front'];
              $price = $res_product['price'];
              $color = $res_product['color'];


              if ($res_product['status'] == '1') {

            ?>
          
          <div class="card product  ml-4 my-2 border" style="height: 375px">
            <a href="productDetail.php?id=<?php echo $pro_id ?>"><img src="img/product/front/<?php echo $img_front ?>" alt="Card image cap" width="100%" height="220"></a>
            <div class="px-3 py-2">
              <p class="text-muted text-sm mb-0 pt-1" style="height: 25px; font-size: 13px"><?php echo $pro_cat_name ?></p>
              <h6 class="mb-0" style="height: 50px; font-size: 14px"><?php echo $product_name ?></h6>
              <p class="card-text text-sm mb-1 text-muted" style="height: 25px; font-size: 13px">Rs. <?php echo $price ?></p>
              <div>

                <a class="btn py-1" href="productDetail.php?id=<?php echo $pro_id ?>" style="width: 100%; background-color: gray; color: #fff; font-size: 11px">View</a>
                <!-- <button class="btn btn-danger btn-sm p-0" type="button" data-toggle="modal" data-target="#Modal1<?php echo $pro_id ?>" style="height: 22px; width: 50px; font-size: 12px">Inquiry</button> -->

              </div>
            </div>
          </div>

          <?php } } ?>

          <!-- popper for Inqiry form -->
          <div class="modal fade" id="Modal1<?php echo $pro_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true ">
            <div class="modal-dialog" role="document ">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Inquery of <?php echo $product_name ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true ">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form method="POST" action="contact_form.php">
                            <div class="row mb-4">
                                <div class="col">
                                    <input type="text" class="form-control" name="fname" placeholder="First name" required>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" name="lname" placeholder="Last name" required>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <div class="col">
                                    <input type="email" class="form-control" name="mail" placeholder="Email" required>
                                </div>
                            </div>

                            <div class="row mb-4">
                                <div class="col">
                                    <input type="text" class="form-control" name="subject" placeholder="Subject" value="<?php echo $product_name.' - color: '.$color ?>" required>
                                </div>
                            </div>

                            <div class="row mb-4" id="map">
                                <div class="col">
                                    <textarea class="form-control" placeholder="Massage" name="msg" style="height:150px" required></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                <a href="#"><input type="submit" class="btn mb-4" value="Send" name="submit" style="background-color:#EF5B00; color: #fff; width:100%"></a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
          </div>
          <!-- popper for Inqiry form -->
          
        </div>

      </div><!-- sidebar end-->
    </div><!-- ends container-->

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>