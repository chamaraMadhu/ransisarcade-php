<?php 
	include "include/head.php";
	include "include/navbar.php";
?>
  
  <!-- starts jumbotron -->
	<div class="jumbotron mb-0" style="background-image:url(img/page_banner/contact.jpg); height: 250px;">
	    <div class="container">
	        <div class="pt-5">
	          <h2 class="text-center" style="color: #fff">Contact Us</h2>
	        </div>
    	</div>
    </div>
    <!-- ends jumbotron -->
	
	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-white">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="contact.php" style="text-decoration: none">Contact us</a></li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

    <div class="container"><!-- starts container-->

      <div class="row">
        <div class="col-sm-6 col-md-6 px-4">
        	<h3 class="text-muted mb-3 mt-3">Opening Hours</h3>
        	<p><span style="font-size: 14px; font-weight: 500; color: #6D6D6D">We will happy to accommedate your wishes.If you need further information, don't hesitate to contact us for free consultation.</span></p>
        	<ul>
        		<li><span style="font-size: 14px; font-weight: 700; color: #353535">Monday-Sunday: 9.30AM - 7.30PM</span></li>
            <li><span style="font-size: 14px; font-weight: 700; color: #353535">Poya days: Close</span></li>
            <li><span style="font-size: 14px; font-weight: 700; color: #353535">Bank & Mercantile Holidays: Open</span></li>
        		<li><span style="font-size: 14px; font-weight: 700; color: #353535">Special times can be arranged.</span></li>
        	</ul>

        	<table>
            <tr>
              <td><i class="fa fa-map-marker mr-4" aria-hidden="true"></i></td>
              <td style="font-size: 14px; color: #6D6D6D">No. 72/2,</td>
            </tr>

            <tr>
              <td></td>
              <td style="font-size: 14px; color: #6D6D6D">Buthgamuwa Road,</td>
            </tr>

            <tr>
              <td></td>
              <td style="font-size: 14px; color: #6D6D6D">Rajagiriya.</td>
            </tr>

            <tr>
              <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
              <td style="font-size: 14px; color: #6D6D6D"><a href="mailto:ransisarcade@gmail.com">ransir@yahoo.com</a></td>
            </tr>

            <tr>
              <td><i class="fa fa-phone" aria-hidden="true"></i></td>
              <td style="font-size: 14px; color: #6D6D6D">(+011) 2 793 889 (General line)</td>
            </tr>

            <tr>
              <td><i class="fa fa-mobile" aria-hidden="true"></i></td>
              <td style="font-size: 14px; color: #6D6D6D">  (+94) 727 352 576 / (+94) 772 725 205 (Sales)
              </td>
            </tr>
          </table>
        </div>

        <div class="col-sm-6 col-md-6 mt-5 px-4">
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Success </strong> 
                </div>

                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Fail</strong> 
                </div>

        	<form method="POST" action="contact_form_query_contact.php">
    			  <div class="row mb-3">
    			    <div class="col">
    			      <input type="text" class="form-control" name="fname" placeholder="First name" required>
    			    </div>
    			    <div class="col">
    			      <input type="text" class="form-control" name="lname" placeholder="Last name" required>
    			    </div>
    			  </div>

    			  <div class="row mb-3">
    			    <div class="col">
    			      <input type="email" class="form-control" name="mail" placeholder="Email" required>
    			    </div>
    			  </div>

            <div class="row mb-3">
              <div class="col">
                <input type="text" class="form-control" name="phone" placeholder="Phone" required>
              </div>
            </div>

            <div class="row mb-3">
              <div class="col">
                <input type="text" class="form-control" name="subject" placeholder="Subject" required>
              </div>
            </div>

    			  <div class="row mb-3" id="map">
    			    <div class="col">
    			      <textarea class="form-control" placeholder="Massage" name="msg" style="height:150px" required></textarea>
    			    </div>
    			  </div>

    			  <div class="row mb-5">
    			    <div class="col">
    			      <a href="#"><input type="submit" class="btn mb-4" value="Send" name="submit" style="background-color:#EE3B3B; color: #fff; width:100%"></a>
			        </div>
            </div>

			    </form>
        </div>
      </div>
  	</div>

  	<div class="container-fluid">
      <div class="row">
      	<iframe src="https://www.google.com/maps/d/embed?mid=1-jxhsdRqukkHMEHn-0_h2wU1EjRXNecq" width="100%" height="300"></iframe>
      </div>
    </div><!-- ends container-->

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>