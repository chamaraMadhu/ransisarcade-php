 <?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Page Banner</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit About Us Page</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>PAGE BANNER</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit About Us Page</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="page_banner/edit_page_banner_about_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$get_banner = "SELECT about FROM page_banner";
	                $run_banner = mysqli_query($con,$get_banner);

	                while($res_banner = mysqli_fetch_array($run_banner)){

	                    $about = $res_banner['about'];

					?>
						
						<div class="col-md-12 form-group row">
							<label class="col-sm-2 col-form-label text-right">About Us page Banner :</label>
							<div class="col-sm-5">
								<img src="../img/page_banner/<?php echo $about ?>" width="500px" class="border py-1">
								<p class="text-muted" style="font-size: 14px">(Please choose 1380px * 250px resolution image)</p>
								<input class="" type="file" name="img" id="validationCustom02"/>
								<div class="invalid-feedback">
								Please choose image.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 mt-3 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<a href="page_banner_about.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Home Banner</button>
							</div>
						</div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>