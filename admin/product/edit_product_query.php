<?php

include "../inc/db_conn.php";

if(isset($_POST['submit'])){

	$id = $_POST['id'];
	$name = $_POST['name'];
	$style = $_POST['style'];
	$color = $_POST['color'];
	$price = $_POST['price'];
	$qty = $_POST['qty'];
	$desc = $_POST['desc'];
	$key = $_POST['keywords'];

	// select old product images to delete
	$select_pro_image = mysqli_query($con,"SELECT * FROM product WHERE id = '$id'");
	while($res_pro_image = mysqli_fetch_array($select_pro_image)){

		$img_front = $res_pro_image ['image_front'];
		$img_jacket = $res_pro_image ['image_jacket'];
		$img_border = $res_pro_image ['image_border'];
		$img_back = $res_pro_image ['image_back'];
	}
	// select old product images to delete

	// front image
	if($_FILES['img_front']['size'] == 0){
		
		}else{

        //delete old category image
		if(file_exists("../../img/product/front/".$img_front)){
		    unlink("../../img/product/front/".$img_front);
		}
		//delete old category image

		// save new category image
		$img_front_file_name = $_FILES['img_front']['name'];
	   	$file_tmp = $_FILES['img_front']['tmp_name'];
	   	$file_size = $_FILES['img_front']['size'];
	   	$file_type= $_FILES['img_front']['type'];

	    move_uploaded_file($file_tmp,"../../img/product/front/".$img_front_file_name);
	    // save new category image

		mysqli_query($con,"UPDATE product SET image_front = '$img_front_file_name' WHERE id = $id");
	}
	// front image 

	// jacket image
	if($_FILES['img_jacket']['size'] == 0){
		
		}else{

        //delete old category image
		if(file_exists("../../img/product/jacket/".$img_jacket)){
		    unlink("../../img/product/jacket/".$img_jacket);
		}
		//delete old category image

		// save new category image
		$img_jacket_file_name = $_FILES['img_jacket']['name'];
	   	$file_tmp = $_FILES['img_jacket']['tmp_name'];
	   	$file_size = $_FILES['img_jacket']['size'];
	   	$file_type= $_FILES['img_jacket']['type'];

	    move_uploaded_file($file_tmp,"../../img/product/jacket/".$img_jacket_file_name);
	    // save new category image

		mysqli_query($con,"UPDATE product SET image_jacket = '$img_jacket_file_name' WHERE id = $id");
	} 
	// jacket image

	// border image
	if($_FILES['img_border']['size'] == 0){
		
		}else{

        //delete old category image
		if(file_exists("../../img/product/border/".$img_border)){
		    unlink("../../img/product/border/".$img_border);
		}
		//delete old category image

		// save new category image
		$img_border_file_name = $_FILES['img_border']['name'];
	   	$file_tmp = $_FILES['img_border']['tmp_name'];
	   	$file_size = $_FILES['img_border']['size'];
	   	$file_type= $_FILES['img_border']['type'];

	    move_uploaded_file($file_tmp,"../../img/product/border/".$img_border_file_name);
	    // save new category image

		mysqli_query($con,"UPDATE product SET image_border = '$img_border_file_name' WHERE id = $id");
	} 
	// border image

	// back image
	if($_FILES['img_back']['size'] == 0){
		
		}else{

        //delete old category image
		if(file_exists("../../img/product/back/".$img_back)){
		    unlink("../../img/product/back/".$img_back);
		}
		//delete old category image

		// save new category image
		$img_back_file_name = $_FILES['img_back']['name'];
	   	$file_tmp = $_FILES['img_back']['tmp_name'];
	   	$file_size = $_FILES['img_back']['size'];
	   	$file_type= $_FILES['img_back']['type'];

	    move_uploaded_file($file_tmp,"../../img/product/back/".$img_back_file_name);
	    // save new category image

		mysqli_query($con,"UPDATE product SET image_back = '$img_back_file_name' WHERE id = $id");
	} 
	// back image
	
	if(isset($_POST['status'])){
		$status = 1;
	}else{
		$status = 0;
	}

	$update_product = "UPDATE product SET pro_id = '$_POST[pro_id]', name = '$name', style = '$style', status = '$status', color = '$color', old_price = '$_POST[old_price]', price = '$price', fabric = '$_POST[fabric]', casual_wear = '$_POST[casual]', party_wear = '$_POST[party]', office_wear = '$_POST[office]', cocktail_wear = '$_POST[cocktail]', wedding_wear = '$_POST[wedding]', wash_care = '$_POST[wash_care]', description = '$desc', keywords = '$key' WHERE id = $id";

	$run_product = mysqli_query($con,$update_product);

	if($run_product > 0){

		$message = "Product has been updated successfully !";
		header("location:../view_product.php?succcess_pro_edit_msg=".$message);

	}else{

		$message = "Product not has been updated successfully !";
		header("location:../edit_product.php?fail_pro_edit_msg=".$message);

	}
}

?>