<?php

include "../inc/db_conn.php";

if(isset($_POST['submit'])){

	// front image upload
	if(isset($_FILES['img_front'])){

		if(file_exists("../../img/product/front/".'$_FILES[img_front]')){

		}else{
	    $file_name_front = $_FILES['img_front']['name'];
	    $file_tmp_front = $_FILES['img_front']['tmp_name'];
	    $file_size = $_FILES['img_front']['size'];
	    $file_type= $_FILES['img_front']['type'];

	    move_uploaded_file($file_tmp_front,"../../img/product/front/".$file_name_front);  

		}
	}
	// front image upload

	// jacket image upload
	if(isset($_FILES['img_jacket'])){

		if(file_exists("../../img/product/jacket/".'$_FILES[img_jacket]')){

		}else{
	   	$file_name_jacket = $_FILES['img_jacket']['name'];
	   	$file_tmp_jacket = $_FILES['img_jacket']['tmp_name'];
	   	$file_size = $_FILES['img_jacket']['size'];
	   	$file_type= $_FILES['img_jacket']['type'];

	   	move_uploaded_file($file_tmp_jacket,"../../img/product/jacket/".$file_name_jacket);  

		}
	}
	// jacket image upload

	// border image upload
	if(isset($_FILES['img_border'])){

		if(file_exists("../../img/product/border/".'$_FILES[img_border]')){

		}else{

	   	$file_name_border = $_FILES['img_border']['name'];
	   	$file_tmp_border = $_FILES['img_border']['tmp_name'];
	   	$file_size = $_FILES['img_border']['size'];
	   	$file_type= $_FILES['img_border']['type'];

	    move_uploaded_file($file_tmp_border,"../../img/product/border/".$file_name_border); 
	    } 
	}
	// border image upload

	// back image upload
	if(isset($_FILES['img_back'])){

		if(file_exists("../../img/product/back/".'$_FILES[img_back]')){

		}else{
	    $file_name_back = $_FILES['img_back']['name'];
	    $file_tmp_back = $_FILES['img_back']['tmp_name'];
	    $file_size = $_FILES['img_back']['size'];
	    $file_type= $_FILES['img_back']['type'];

	    move_uploaded_file($file_tmp_back,"../../img/product/back/".$file_name_back);  
		}
	}
	// back image upload

	// status 
	if(isset($_POST['status'])){
		$status = 1;
	}else{
		$status = 0;
	}
	// status 

	$add_product = "INSERT INTO product (pro_id, name, style, fabric, status, image_front, image_jacket, image_border, image_back, casual_wear, party_wear, office_wear, cocktail_wear, wedding_wear, color, old_price, price, wash_care, description, keywords) VALUES('$_POST[pro_id]','$_POST[name]','$_POST[style]','$_POST[fabric]','$status','$file_name_front','$file_name_jacket','$file_name_border','$file_name_back','$_POST[casual]','$_POST[party]','$_POST[office]','$_POST[cocktail]','$_POST[wedding]','$_POST[color]','$_POST[old_price]','$_POST[price]','$_POST[wash_care]','$_POST[desc]','$_POST[keywords]')";

	$run_add_product = mysqli_query($con,$add_product);

	if($run_add_product>0){
		$message = "Product has been added successfully !";
		header("location:../add_product.php?success_msg=".$message);
	
	}else{

		$message = "Product has not been added successfully !";
		header("location:../add_product.php?fail_msg=".$message);
	}
}

?>