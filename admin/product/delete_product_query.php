<?php

include "../inc/db_conn.php";

if(isset($_GET['submit'])){

	$id = $_GET['id']; // get product id

	// select product images to delete
	$select_pro_image = mysqli_query($con,"SELECT * FROM product WHERE id = $id");

	while($res_pro_image = mysqli_fetch_array($select_pro_image)){

		$img_front = $res_pro_image ['image_front'];
		$img_jacket = $res_pro_image ['image_jacket'];
		$img_border = $res_pro_image ['image_border'];
		$img_back = $res_pro_image ['image_back'];
	}
	// select product images to delete

	// delete product 
	$del_product = "DELETE FROM product WHERE id = $id";
	$run_del_product = mysqli_query($con,$del_product);

	if($run_del_product>0){
		// delete product images
		if(file_exists("../../img/product/front/".$img_front)){
			    unlink("../../img/product/front/".$img_front);
			}

		if(file_exists("../../img/product/jacket/".$img_jacket)){
			    unlink("../../img/product/jacket/".$img_jacket);
			}

		if(file_exists("../../img/product/border/".$img_border)){
			    unlink("../../img/product/border/".$img_border);
			}

		if(file_exists("../../img/product/back/".$img_back)){
			    unlink("../../img/product/back/".$img_back);
			}
		// delete product images
	
		$message = "Product has been deleted successfully !";
		header("location:../view_product.php?success_del_msg=".$message);

	}else{

		$message = "Product has not been deleted successfully !";
		header("location:../view_product.php?fail_del_msg=".$message);
	}
	// delete product 
}
?>