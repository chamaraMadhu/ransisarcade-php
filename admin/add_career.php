<?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
if($_SESSION['role']=='Owner'){
	header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Career</li>
            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="add_career.php" style="text-decoration: none">Add Career</a></li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>CAREER</h2><hr>
			<?php
				if(isset($_GET['success_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color: gray">
	                <h6 class="col-12 text-white">ADD CAREER</h6>
	            </div>

	           	<form action="career/add_career_query.php" method="POST" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
				<div class="form-row">

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Career ID <b class="text-danger">*</b></label>
						<div class="col-sm-1">
							<input type="text" name="id" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the ID.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Job Type <b class="text-danger">*</b></label>
						<div class="col-sm-2">
							<!-- <input type="text" name="type" class="form-control" id="validationCustom02" required> -->
							<select name="type" class="form-control" id="validationCustom02" required>
								<option></option>
								<option value="Full Time">Full Time</option>
								<option value="Part Time">Part Time</option>
							</select>
							<div class="invalid-feedback">
							Please choose job type.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Position <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="position" class="form-control" id="validationCustom02" required>
							<div class="invalid-feedback">
							Please choose position.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Advertisement <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="file" name="advert" class="form-control" id="validationCustom02" required>
							<div class="invalid-feedback">
							Please choose advertisement.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Closing Date <b class="text-danger">*</b></label>
						<div class="col-sm-3">
							<input type="date" name="closing_date" class="form-control" id="validationCustom02" required>
							<div class="invalid-feedback">
							Please choose job type.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Activate </label>
						<div class="col-sm-4">
							<input type="checkbox" class="mt-2" name="status"/>
						</div>
					</div>

					<div class="col-md-12 mb-0 form-group row">
						<label class="col-sm-2 col-form-label text-right"></label>
						<div class="col-sm-4">
							<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Add Career</button>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
                      <label class="col-sm-2 col-form-label text-right"></label>
                      <div class="col-sm-4">
                        <b class="text-danger" style="font-size: 14px">* Denotes Required</b>
                      </div>
                    </div>
				
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>