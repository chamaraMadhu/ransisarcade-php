<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');	 
}
if($_SESSION['role']<>'Owner'){
	 header('location:dashboard.php');
 }

if($_SESSION['add']<>'1'){
 	 header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Setting</li>
            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="add_user.php" style="text-decoration: none">Add User</a></li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>SETTING</h2><hr>
			<?php
				if(isset($_GET['success_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color:#CC0003">
	                <h5 class="col-12 text-white">Add User</h5>
	            </div>

	           	<form action="setting/add_user_query.php" method="POST" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
				<div class="form-row">

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">User name <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="uname" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the user name.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Role <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="role" class="form-control" id="validationCustom02" required>
							<div class="invalid-feedback">
							Please insert a role.
							</div>
						</div>
					</div>
					
					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Image <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="file" name="img" class="form-control" id="validationCustom03" required>
							<div class="invalid-feedback">
							Please choose user image.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Mail <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="email" name="mail" class="form-control" id="validationCustom04" required>
							<div class="invalid-feedback">
							Please insert a mail.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Password <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="Password" name="pwd" class="form-control" id="validationCustom05" required>
							<div class="invalid-feedback">
							Please insert a password.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Privilege <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							View - <input type="checkbox" class="mt-2" name="view"/> &nbsp; &nbsp;
							Add - <input type="checkbox" class="mt-2" name="add"/> &nbsp; &nbsp;
							Edit - <input type="checkbox" class="mt-2" name="edit"/> &nbsp; &nbsp;
							Del - <input type="checkbox" class="mt-2" name="del"/> &nbsp; &nbsp;
						</div>
					</div>

					<div class="col-md-12 mb-0 form-group row">
						<label class="col-sm-2 col-form-label text-right"></label>
						<div class="col-sm-4">
							<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Add User</button>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
                      	<label class="col-sm-2 col-form-label text-right"></label>
                      	<div class="col-sm-4">
                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
                      	</div>
                    </div>
				
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>