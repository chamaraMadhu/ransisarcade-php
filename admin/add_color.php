<?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
if($_SESSION['role']=='Owner'){
	header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Color Range</li>
            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="add_color.php" style="text-decoration: none">Add Color</a></li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>COLOR RANGE</h2><hr>
			<?php
				if(isset($_GET['success_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color: gray">
	                <h6 class="col-12 text-white text-uppercase">Add Color</h6>
	            </div>

	           	<form action="color_range/add_color_query.php" method="GET" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
				<div class="form-row">

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Color ID <b class="text-danger">*</b></label>
						<div class="col-sm-1">
							<input type="text" name="id" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the color ID.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Color Name <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="name" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the color.
							</div>
						</div>
					</div>
					
					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Color Code <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="code" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the color code.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Activate </label>
						<div class="col-sm-4">
							<input type="checkbox" class="mt-2" name="status"/>
						</div>
					</div>

					<div class="col-md-12 mb-0 form-group row">
						<label class="col-sm-2 col-form-label text-right"></label>
						<div class="col-sm-4">
							<button class="btn btn-success mb-3" type="submit" name="submit">Add Color</button>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
                      <label class="col-sm-2 col-form-label text-right"></label>
                      <div class="col-sm-4">
                        <b class="text-danger" style="font-size: 14px">* Denotes Required</b>
                      </div>
                    </div>
				
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>