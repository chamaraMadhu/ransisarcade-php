<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php');
}
if($_SESSION['view']==0){
	header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
        <div class="col-10 bg-danger p-0">
        	<div class="container-fluid p-0">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
					<li class="breadcrumb-item text-light active" aria-current="page">Product</li>
		            <li class="breadcrumb-item text-light active" aria-current="page">Search Product</li>
		        </ol>
		    </nav>

			    <div class="container-fluid">					
			        <h2>PRODUCT</h2><hr>
		            
		            <?php 
		            // aleart massages for editing product
		            if(isset($_GET['succcess_pro_edit_msg'])){ ?>

		                <div class="alert alert-success alert-block">
		                  <button type="button" class="close" data-dismiss="alert">x</button>
		                  <strong><?php echo $_GET['succcess_pro_edit_msg']; ?></strong> 
		                </div>
		            <?php 
		                }
		                // aleart massages for editing product 


		            // aleart massages for deleting product
		              if(isset($_GET['success_del_msg'])){
		            ?>
		                <div class="alert alert-success alert-block">
		                    <button type="button" class="close" data-dismiss="alert">x</button>
		                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
		                </div>

		            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

		                <div class="alert alert-danger alert-block">
		                  <button type="button" class="close" data-dismiss="alert">x</button>
		                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
		                </div>
		            <?php 
		                }
		                // aleart massages for deleting product
		             ?>

		            <div class="container-fluid bg-white mb-4">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-white">SEARCH PRODUCT</h6>
			            </div>

			           	<div class="">
                            <div class="card-body">                               
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-hover">
                                        <thead>
                                            <tr style="background-color: gray; color:#fff">
                                                <td width="50px">Pro ID</td>
                                                <td>Product Name</td>
                                                <td>Style Name</td>
                                                <td>Image</td>
                                                <td>Price</td>
                                                <td>Status</td>
                                                <td class="text-center" style="width: 75px">Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php

                                              include "inc/db_conn.php";

                                              $get_product = "SELECT * FROM product WHERE pro_id = '$_GET[pro_id]'";
                                              $run_product = mysqli_query($con,$get_product);

                                              while($res_product = mysqli_fetch_array($run_product)){

                                                $id = $res_product['id'];
                                                $pro_id = $res_product['pro_id'];
                                                $name = $res_product['name'];
                                                $style = $res_product['style'];
                                                $status = $res_product['status'];
                                                $img_front = $res_product['image_front'];
                                                $img_jacket = $res_product['image_jacket'];
                                                $img_border = $res_product['image_border'];
                                                $img_back = $res_product['image_back'];
                                                $color = $res_product['color'];
                                                $price = $res_product['price'];
                                                $desc = $res_product['description'];
                                                $keywords = $res_product['keywords'];

                                              ?>

                                            <tr>
                                                <td align="center"><?php echo $pro_id ?></td>
                                                <td><?php echo $name ?></td>
                                                <td><?php echo $style?></td>
                                                <td align="center"><img src="../img/product/front/<?php echo $img_front ?>" width="60px" class="py-1"></td>
                                                <td>Rs. <?php echo $price ?></td>
                                                <td><?php if($status == 1 ){ ?><span style="color:darkgreen;font-weight:bold">Active</span><?php }else{ ?> <span style="color:red;font-weight:bold">Deactive</span> <?php } ?></td>
                                                <td>
                                                	<?php if (isset($_SESSION['email'])){ if ($_SESSION['view']=='1'){ ?>
                                                    <button type="button" class="btn btn-info btn-sm mr-1" data-toggle="modal" data-target="#Modal1<?php echo $id ?>" style="float: left"><i class="fa fa-info"></i></button>
													<?php } } ?>

													<?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ ?>
														<?php if ($_SESSION['edit']=='1'){ ?>
														<form method="GET" action="edit_product.php" style="float: left">
		                                                    <input type="hidden" name="id" value="<?php echo $id ?>"/>
	                                                    	<button href="" type="submit" class="btn btn-secondary btn-sm mr-1"><i class="fa fa-pencil"></i></button>
														</form>
														<?php } ?>

														<?php if ($_SESSION['del']=='1'){ ?>
	                                                    <form method="GET" action="product/delete_product_query.php" style="float: left">
		                                                    <input type="hidden" name="id" value="<?php echo $id ?>"/>
		                                                    <button type="submit" name="submit" class="btn btn-danger btn-sm del_product"><i class="fa fa-trash"></i></button>
		                                                </form>
		                                                <?php } ?>
		                                            <?php } } ?>
	                                            </td>
											</tr>

                                            <!-- Modal -->
                                            <div class="modal fade" id="Modal1<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document ">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h6 class="modal-title text-muted" id="exampleModalLabel">Full Details of <?php echo $name ?></h6>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-muted">

                                                            <p><b>Product ID:</b> <?php echo $pro_id ?> </p>
                                                            <p><b>Product Name:</b> <?php echo $name ?> </p>
                                                            <p><b>Style:</b> <?php echo $style ?> </p>
                                                            <p><b>Fabric:</b> <?php echo $res_product['fabric'] ?> </p>
                                                            <p><b>Occation:</b> <?php echo $res_product['occasion'] ?> </p>
                                                            <p><b>Image - Front:</b> &nbsp;&nbsp; &nbsp;<img src="../img/product/front/<?php echo $img_front ?>" width="60px" class="py-1"> &nbsp; <?php echo $img_front ?></p>
                                                            <p><b>Image - Jacket:</b>&nbsp; &nbsp; <img src="../img/product/jacket/<?php echo $img_jacket ?>" width="60px" class="py-1"> &nbsp; <?php echo $img_jacket ?></p>
                                                            <p><b>Image - Border:</b> &nbsp; <img src="../img/product/border/<?php echo $img_border ?>" width="60px" class="py-1"> &nbsp; <?php echo $img_border ?></p>
                                                            <p><b>Image -  Back:</b> &nbsp;&nbsp; &nbsp; <img src="../img/product/back/<?php echo $img_back ?>" width="60px" class="py-1"> &nbsp; <?php echo $img_back ?></p>
                                                            <p><b>Price:</b> Rs. <?php echo $price ?> </p>
                                                            <p><b>Color:</b> <?php echo $color ?> </p>
                                                            <p><b>Dimension:</b> <?php echo $res_product['dimension'] ?> </p>
                                                            <p><b>wash and Care:</b> <?php echo $res_product['wash_care'] ?> </p>
                                                            <p><b>Description:</b> <?php echo $desc ?> </p>
                                                            <p><b>Keywords:</b> <?php echo $keywords ?> </p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
											<?php

                                              } 

                                            ?>
                                            <!-- Modal -->  
                                            
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
		                </div>

			        </div>
			    </div>      
			</div>
		</div>
        <!-- content -->

<?php 
  include "inc/footer.php";
?>