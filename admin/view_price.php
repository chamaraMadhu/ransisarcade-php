<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php'); 
}
if($_SESSION['view']==0){
  header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

        <!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="page-wrapper">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
					      <li class="breadcrumb-item text-light active" aria-current="page">Price Range</li>
		            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="view_price.php" style="text-decoration: none">View Price</a></li>
		        </ol>
		    </nav>

		    <div class="container-fluid">					
		        <h2>PRICE RANGE</h2><hr>
		        <?php
            // aleart massages for editing category
              if(isset($_GET['success_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for editing category 


            // aleart massages for deleting category
              if(isset($_GET['success_del_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for deleting category
             ?>

		            <div class="container-fluid bg-white ">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-uppercase text-white">View Price</h6>
			            </div>

			           	<div class="mb-4">
                    <div class="card-body">
                        
                      <div class="table-responsive">
                          <table id="zero_config" class="table table-hover text-center">
                              <thead>
                                  <tr style="background-color: gray; color:#fff">
                                      <td>Minimum Price</td>
                                      <td>Maximum Price</td>
                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){  ?>
                                      <td>Action</td>
                                      <?php } } } ?>
                                  </tr>
                              </thead>
                              <tbody>

                              <?php

                                include "inc/db_conn.php";

                                $get_price_range = "SELECT * FROM price_range";
                                $run_price_range = mysqli_query($con,$get_price_range);

                                while($res_price_range = mysqli_fetch_array($run_price_range)){

                                  $id = $res_price_range['id'];
                                  $min = $res_price_range['min'];
                                  $max = $res_price_range['max'];

                                ?>

                                  <tr>
                                      <td><?php echo $min; ?></td>
                                      <td><?php echo $max;  ?></td>

                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
                                      <td>
                                        <?php if ($_SESSION['edit']=='1'){ ?>
                                      	 <form method="GET" action="edit_price.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                                          	<button type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></button>&nbsp;
                                          </form>
                                          <?php } ?>

                                          <?php if ($_SESSION['del']=='1'){ ?>
                                          <form method="GET" action="price_range/delete_price_query.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                                          	<button type="submit" name="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                          </form>
                                          <?php } ?>
                                      </td>
                                      <?php } } } ?>
						                      </tr>

                              <?php
                                } 
                              ?>

                              </tfoot>
                          </table>
                      </div>

                    </div>
                  </div>

		            </div>
		    </div>      
		</div>
   
 </div>
 <!-- content -->
 
<?php 
  include "inc/footer.php";
?>