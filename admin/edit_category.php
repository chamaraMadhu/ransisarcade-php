<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
	            <li class="breadcrumb-item"><a class="text-light" href="dashboard.php">Admin Panel</a></li>
				<li class="breadcrumb-item text-light active" aria-current="page">Category</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Category</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>CATEGORY</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit Category</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="category/edit_category_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_connection.php";

					$id = $_GET['id'];

					$get_category = "SELECT * FROM category WHERE id = $id";
	                $run_category = mysqli_query($con,$get_category);

	                while($res_category = mysqli_fetch_array($run_category)){

	                 	$id = $res_category['id'];
	                    $name = $res_category['name'];
	                    $img = $res_category['image'];
	                    $desc = $res_category['description'];
	                    $status = $res_category['status'];

					?>
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Category name :</label>
							<div class="col-sm-4">
								<input type="text" name="name" class="form-control" id="validationCustom01" value="<?php echo $name ?>" required>
								<div class="invalid-feedback">
								Please insert the category name.
								</div>
							</div>
						</div>
						
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Category Image :</label>
							<div class="col-sm-5">
								<input type="file" name="img" id="validationCustom02"/>
								<img src="../img/category/<?php echo $img ?>" width="40px" class="border py-1">
								<div class="invalid-feedback">
								Please choose image.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Description :</label>
							<div class="col-sm-4">
								<textarea class="form-control" name="desc" id="validationCustom03" required><?php echo $desc ?></textarea>
								<div class="invalid-feedback">
								Please insert a description.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Activate :</label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($status == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="view_category.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Category</button>
							</div>
						</div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>