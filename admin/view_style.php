<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php'); 
}
if($_SESSION['view']==0){
  header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

        <!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="page-wrapper">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
					      <li class="breadcrumb-item text-light active" aria-current="page">Style</li>
		            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="view_style.php" style="text-decoration: none">View Style</a></li>
		        </ol>
		    </nav>

		    <div class="container-fluid">					
		        <h2>STYLE</h2><hr>
		        <?php
            // aleart massages for editing category
              if(isset($_GET['success_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for editing category 


            // aleart massages for deleting category
              if(isset($_GET['success_del_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for deleting category
             ?>

		            <div class="container-fluid bg-white ">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-white">View Style</h6>
			            </div>

			           	<div class="mb-4">
                    <div class="card-body">  
                      <div class="table-responsive"> 

                          <table id="zero_config" class="table table-hover">
                              <thead>
                                  <tr style="background-color: gray; color: #fff">
                                      <td>Style Name</td>
                                      <td>Style Image</td>
                                      <td align="center">Status</td>
                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){  ?>
                                      <td width="60px">Action</td>
                                      <?php } } } ?>
                                  </tr>
                              </thead>
                              <tbody>

                              <?php

                                include "inc/db_conn.php";

                                $get_style = "SELECT * FROM style";

                                $run_style = mysqli_query($con,$get_style);

                                while($res_style = mysqli_fetch_array($run_style)){

                                  $style = $res_style['style'];
                                  $img = $res_style['image'];
                                ?>

                                  <tr>
                                      <td><?php echo $style; ?></td>
                                      <td align="center"><img src="../img/style/<?php echo $img; ?>" width="70px" class="py-1"></td>
                                      <td><?php if($res_style['status']==1) { ?> <span style="color:darkgreen; font-weight:bold">Active</span> <?php }else{ ?> <span style="color:red; font-weight:bold">Deactive</span> <?php } ?> </td>

                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
                                      <td>
                                        <?php if ($_SESSION['edit']=='1'){ ?>
                                      	 <form method="GET" action="edit_style.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $res_style['id'] ?>"/>
                                          	<button type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></button>&nbsp;
                                          </form>
                                          <?php } ?>

                                          <?php if ($_SESSION['del']=='1'){ ?>
                                          <form method="GET" action="style/delete_style_query.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $res_style['id'] ?>"/>
                                          	<button type="submit" name="submit" class="btn btn-danger btn-sm del_style"><i class="fa fa-trash"></i></button>
                                          </form>
                                          <?php } ?>
                                      </td>
                                      <?php } } } ?>
						                      </tr>

                                <?php
                            } 

                            ?>

                              </tfoot>
                          </table>
                      </div>

                    </div>
                  </div>

		            </div>
		    </div>      
		</div>
   
 </div>
 <!-- content -->
 
<?php 
  include "inc/footer.php";
?>