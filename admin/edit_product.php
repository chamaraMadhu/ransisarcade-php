<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Product</li>
            <li class="breadcrumb-item text-light active" aria-current="page">Edit Product</li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>PRODUCT</h2><hr>

			<?php
            // aleart massages for editing category
              if(isset($_GET['fail_pro_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['fail_pro_edit_msg']; ?> </strong> 
                </div>

            <?php } ?>

            <div class="container-fluid bg-white">

	            <div class="row pt-2" style="background-color: grey">
	                <h6 class="col-12 text-white">Edit Product</h6>
	            </div>

	           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="product/edit_product_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_product = "SELECT * FROM product WHERE id = '$_GET[id]'";
	                $run_product = mysqli_query($con,$get_product);

	                while($res_product = mysqli_fetch_array($run_product)){

	                	$id = $res_product['id'];
	                	$pro_id = $res_product['pro_id'];
	                    $name = $res_product['name'];
	                    $style = $res_product['style'];
	                    $img_front = $res_product['image_front'];
	                    $img_jacket = $res_product['image_jacket'];
	                    $img_border = $res_product['image_border'];
	                    $img_back = $res_product['image_back'];
	                    $color = $res_product['color'];
	                    $price = $res_product['price'];
	                    $desc = $res_product['description'];
	                    $key = $res_product['keywords'];
	                    $status = $res_product['status'];

					?>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Product ID <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<input type="text" name="pro_id" class="form-control" id="validationCustom01" value="<?php echo $pro_id ?>" required>
								<div class="invalid-feedback">
								Please insert the product ID.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Product Name <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<input type="text" name="name" class="form-control" id="validationCustom01" value="<?php echo $name ?>" required>
								<div class="invalid-feedback">
								Please insert the product name.
								</div>
							</div>
						</div>
						
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Style <b class="text-danger">*</b></label>
							<div class="col-sm-3">
								<select name="style" class="form-control" id="validationCustom02" required>
									
									<option style="background-color:lightgray; color:red; font-weight:bold"><?php echo $style ?></option>
									

									<?php
									$get_style = "SELECT * FROM style WHERE style <> '$style'";
									$run_style = mysqli_query($con,$get_style);

	                					while($res_style = mysqli_fetch_array($run_style)){

	                						$style = $res_style['style'];
									?>

										<option><?php echo $style ?></option>
									
									<?php
				                      	} 
				                    ?>

								</select>
								<div class="invalid-feedback">
								Please insert the category name.
								</div>
							</div>
						</div>
						
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Product Image - Front </label>
							<div class="col-sm-6">
								<input  class="col-sm-6" type="file" name="img_front" class="form-control" id="validationCustom03">
								<img src="../img/product/front/<?php echo $img_front ?>" width="40px" class="py-1">
								<div class="invalid-feedback">
								Please choose front image.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Product Image - Jacket </label>
							<div class="col-sm-6">
								<input class="col-sm-6" type="file" name="img_jacket" class="form-control" id="validationCustom04">
								<img src="../img/product/jacket/<?php echo $img_jacket ?>" width="40px" class="py-1">
								<div class="invalid-feedback">
								Please choose jacket image.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Product Image - Border </label>
							<div class="col-sm-6">
								<input class="col-sm-6" type="file" name="img_border" class="form-control" id="validationCustom05">
								<img src="../img/product/border/<?php echo $img_border ?>" width="40px" class="py-1">
								<div class="invalid-feedback">
								Please choose border image.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Product Image - Back </label>
							<div class="col-sm-6">
								<input class="col-sm-6" type="file" name="img_back" class="form-control" id="validationCustom06" >
								<img src="../img/product/back/<?php echo $img_back ?>" width="40px" class="py-1">
								<div class="invalid-feedback">
								Please choose back image.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Fabric <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="fabric" class="form-control" id="validationCustom02" required>

                              <option style="background-color:lightgray; color:red; font-weight:bold"><?php echo $res_product['fabric'] ?></option>
                              <?php

                                $get_fabric = "SELECT * FROM fabric";
                                $run_fabric = mysqli_query($con,$get_fabric);

                                while($res_fabric = mysqli_fetch_array($run_fabric)){

                                  $fabric = $res_fabric['fabric'];

                                  if ($res_fabric['status'] == '1') {

                                ?>
                                  <option><?php echo $fabric ?></option> 

                                <?php
                                   } }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the fabric name.
                            </div>
                          </div>
                        </div>


						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Color <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="color" class="form-control" id="validationCustom07" value="<?php echo $color ?>" required>
								<div class="invalid-feedback">
								Please insert the color.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Old Price (LKR) <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="old_price" class="form-control" id="validationCustom08" value="<?php echo $res_product['old_price'] ?>" required>
								<div class="invalid-feedback">
								Please insert the price.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Discounted Price (LKR) <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="price" class="form-control" id="validationCustom08" value="<?php echo $price ?>" required>
								<div class="invalid-feedback">
								Please insert the price.
								</div>
							</div>
						</div>

						<!-- <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Occasion <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="occasion" class="form-control" id="validationCustom02" required>

                              <option style="background-color:lightgray; color:red; font-weight:bold"><?php echo $res_product['occasion'] ?></option>
                              <?php

                                $get_occasion = "SELECT * FROM occasion";
                                $run_occasion = mysqli_query($con,$get_occasion);

                                while($res_occasion = mysqli_fetch_array($run_occasion)){

                                  $occasion = $res_occasion['occasion'];

                                  if ($res_occasion['status'] == '1') {

                                ?>
                                  <option><?php echo $occasion ?></option> 

                                <?php
                                   } }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the occasion name.
                            </div>
                          </div>
                        </div> -->

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Occasion <b class="text-danger">*</b></label>
                          <div class="col-sm-5">

                            <table class="text-muted" style="line-height: 30px; font-weight: 600; font-size: 15px">
                              <tr>
                                <td width="150px">Casual </td>
                                <td><input type="checkbox" name="casual" value="casual" <?php if($res_product['casual_wear'] == "casual"){ ?> checked <?php } ?>></td>
                              </tr>
                              <tr>
                                <td>Party </td>
                                <td><input type="checkbox" name="party" value="party" <?php if($res_product['party_wear'] == "party"){ ?> checked <?php } ?>></td>
                              </tr>
                              <tr>
                                <td>Office</td>
                                <td><input type="checkbox" name="office" value="office" <?php if($res_product['office_wear'] == "office"){ ?> checked <?php } ?>></td>
                              </tr>
                              <tr>
                                <td>Cocktail</td>
                                <td><input type="checkbox" name="cocktail" value="cocktail" <?php if($res_product['cocktail_wear'] == "cocktail"){ ?> checked <?php } ?>></td>
                              </tr>
                              <tr>
                                <td>Wedding & Engagement </td>
                                <td><input type="checkbox" name="wedding" value="wedding & engagement" <?php if($res_product['wedding_wear'] == "wedding & engagement"){ ?> checked <?php } ?>></td>
                              </tr>
                            </table>
                            
                            <div class="invalid-feedback">
                            Please insert the occasion.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Wash and Care <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <select name="wash_care" class="form-control" id="validationCustom02">

                                <option style="background-color:lightgray; color:red; font-weight:bold"><?php echo $res_product['wash_care'] ?></option>
                                <option>Hand Wash Separately In Cold Water</option>
                                <option>Dry Clean Recommended</option>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the wash and care.
                            </div>
                          </div>
                        </div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Description <b class="text-danger">*</b></label>
							<div class="col-sm-8">
								<textarea class="form-control" name="desc" rows="5" id="validationCustom10" required><?php echo $desc ?></textarea>
								<div class="invalid-feedback">
								Please insert description.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Keywords <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<input type="text" name="keywords" class="form-control" id="validationCustom11" value="<?php echo $key ?>" required>
								<div class="invalid-feedback">
								Please insert keywords.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-3 col-form-label text-right">Activate </label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($status == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<input type="hidden" name="id" value="<?php echo $_GET['id'] ?>"/>
							<div class="col-sm-4">
								<a href="view_product.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Product</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>
					<?php

                      	} 

                    ?>
				</form>

        </div>
    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>