<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Advertisement</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Advertisement</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>ADVERTISEMENT</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit Advertisement</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="advertisement/edit_advertisement_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_advertisement = "SELECT * FROM advertisement WHERE id = $id";
	                $run_advertisement = mysqli_query($con,$get_advertisement);

	                while($res_advertisement = mysqli_fetch_array($run_advertisement)){

	                    $advertisement = $res_advertisement['advertisement'];
	                    $desc = $res_advertisement['descreption'];
	                    $status = $res_advertisement['status'];

					?>
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">ID <b class="text-danger">*</b></label>
							<div class="col-sm-1">
								<input type="text" name="id" class="form-control" id="validationCustom01" value="<?php echo $id ?>" required>
								<div class="invalid-feedback">
								Please insert the category name.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-0 form-group row">
							<label class="col-sm-2 col-form-label text-right">Advertisement <b class="text-danger">*</b></label>
							<div class="col-sm-5 mb-3">
								<input type="file" name="advert" id="validationCustom02"  value="<?php echo $advertisement ?>">
								<img src="../img/advertisement/<?php echo $advertisement ?>" width="150px" class="py-2">
								<div class="invalid-feedback">
								Please choose job type.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Description <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<textarea class="form-control" name="desc" id="validationCustom03" required><?php echo $desc ?></textarea>
								<div class="invalid-feedback">
								Please insert a description.
								</div>
							</div>
						</div>	

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Activate </label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($status == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-0 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="view_advertisement.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Advertisement</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>