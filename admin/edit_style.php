<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Category</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Category</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>CATEGORY</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit Category</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="style/edit_style_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_style = "SELECT * FROM style WHERE id = $id";
	                $run_style = mysqli_query($con,$get_style);

	                while($res_style = mysqli_fetch_array($run_style)){

	                 	$id = $res_style['id'];
	                    $style = $res_style['style'];
	                    $img = $res_style['image'];
	                    $status = $res_style['status'];

					?>
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Category <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<input type="text" name="style" class="form-control" id="validationCustom01" value="<?php echo $style ?>" required>
								<div class="invalid-feedback">
								Please insert the style name.
								</div>
							</div>
						</div>
						
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Category Image <b class="text-danger">*</b></label>
							<div class="col-sm-5">
								<input type="file" name="img" id="validationCustom02"/ required>
								<img src="../img/style/<?php echo $img ?>" width="40px" class="border py-1">
								<div class="invalid-feedback">
								Please choose image.
								</div>
							</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Activate </label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($status == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="view_style.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Category</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>