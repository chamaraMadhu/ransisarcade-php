 text-light<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Color Range</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Color</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>COLOR RANGE</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit Color</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="color_range/edit_color_query.php" method="GET" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_color_range = "SELECT * FROM color_range WHERE id = $id";
	                $run_color_range = mysqli_query($con,$get_color_range);

	                while($res_color_range = mysqli_fetch_array($run_color_range)){

	                 	$id = $res_color_range['id'];
	                    $name = $res_color_range['name'];
	                    $code = $res_color_range['code'];
	                    $status = $res_color_range['status'];
					?>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Color ID <b class="text-danger">*</b></label>
							<div class="col-sm-1">
								<input type="text" name="id" class="form-control" id="validationCustom01" value="<?php echo $id ?>" required>
								<div class="invalid-feedback">
								Please insert the color ID.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Color <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="name" class="form-control" id="validationCustom01" value="<?php echo $name ?>" required>
								<div class="invalid-feedback">
								Please insert the color name.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Color Code <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="code" class="form-control" id="validationCustom01" value="<?php echo $code ?>" required>
								<div class="invalid-feedback">
								Please insert the color code.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Activate </label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($status == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-0 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="view_color.php" class="btn btn-danger mb-3 btn-sm" type="submit">Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Color</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>