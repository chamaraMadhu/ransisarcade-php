<?php

include "../inc/db_conn.php";

if(isset($_POST['submit'])){

	if($_FILES['img']['size'] == 0){

		$message = "Home page banner has not been updated successfully !";
		header("location:../page_banner_home.php?fail_edit_msg=".$message);
		
	}else{

	// select old banner
	$select_home_banner = mysqli_query($con,"SELECT home FROM page_banner");
	while($res_home_banner = mysqli_fetch_array($select_home_banner)){

		$home_banner = $res_home_banner['home'];
	}
	// select old banner

    //delete old banner
	if(file_exists("../../img/page_banner/".$home_banner)){
	    unlink("../../img/page_banner/".$home_banner);
	}
	//delete old banner

	// save new banner
	$file_name = $_FILES['img']['name'];
   	$file_tmp = $_FILES['img']['tmp_name'];
   	$file_size = $_FILES['img']['size'];
   	$file_type= $_FILES['img']['type'];

    move_uploaded_file($file_tmp,"../../img/page_banner/".$file_name);
    // save new banner

	$run_update_banner = mysqli_query($con,"UPDATE page_banner SET home = '$file_name'");

		if($run_update_banner > 0){

		$message = "Home page banner has been updated successfully";
		header("location:../page_banner_home.php?success_edit_msg=".$message);

		}else{

		$message = "Home page banner has not been updated successfully !";
		header("location:../page_banner_home.php?fail_edit_msg=".$message);

		}

	}
}

?>