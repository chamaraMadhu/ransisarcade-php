<?php

include "../inc/db_conn.php";

if(isset($_POST['submit1'])){

	if($_FILES['img']['size'] == 0){

		$message = "Shop page banner 1 has not been updated successfully !";
		header("location:../page_banner_shop.php?fail_edit_msg=".$message);
		
	}else{

	// select old banner
	$select_shop_banner1 = mysqli_query($con,"SELECT * FROM page_banner");
	while($res_shop_banner1 = mysqli_fetch_array($select_shop_banner1)){

		$shop_banner1 = $res_shop_banner1['shop1'];
		echo $shop_banner1;
	}
	// select old banner

    //delete old banner
	if(file_exists("../../img/page_banner/".$shop_banner1)){
	    unlink("../../img/page_banner/".$shop_banner1);
	}
	//delete old banner

	// save new banner
	$file_name = $_FILES['img']['name'];
   	$file_tmp = $_FILES['img']['tmp_name'];
   	$file_size = $_FILES['img']['size'];
   	$file_type= $_FILES['img']['type'];

    move_uploaded_file($file_tmp,"../../img/page_banner/".$file_name);
    // save new banner

	$run_update_banner = mysqli_query($con,"UPDATE page_banner SET shop1 = '$file_name'");

		if($run_update_banner > 0){

		$message = "Shop page banner 1 has been updated successfully";
		header("location:../page_banner_shop.php?success_edit_msg=".$message);

		}else{

		$message = "Shop page banner 1 has not been updated successfully !";
		header("location:../page_banner_shop.php?fail_edit_msg=".$message);

		}

	}
}

if(isset($_POST['submit2'])){

	if($_FILES['img']['size'] == 0){

		$message = "Shop page banner 2 has not been updated successfully !";
		header("location:../page_banner_shop.php?fail_edit_msg=".$message);
		
	}else{

	// select old banner
	$select_shop_banner2 = mysqli_query($con,"SELECT * FROM page_banner");
	while($res_shop_banner2 = mysqli_fetch_array($select_shop_banner2)){

		$shop_banner2 = $res_shop_banner2['shop2'];
	}
	// select old banner

    //delete old banner
	if(file_exists("../../img/page_banner/".$shop_banner2)){
	    unlink("../../img/page_banner/".$shop_banner2);
	}
	//delete old banner

	// save new banner
	$file_name = $_FILES['img']['name'];
   	$file_tmp = $_FILES['img']['tmp_name'];
   	$file_size = $_FILES['img']['size'];
   	$file_type= $_FILES['img']['type'];

    move_uploaded_file($file_tmp,"../../img/page_banner/".$file_name);
    // save new banner

	$run_update_banner = mysqli_query($con,"UPDATE page_banner SET shop2 = '$file_name'");

		if($run_update_banner > 0){

		$message = "Shop page banner 2 has been updated successfully";
		header("location:../page_banner_shop.php?success_edit_msg=".$message);

		}else{

		$message = "Shop page banner 2 has not been updated successfully !";
		header("location:../page_banner_shop.php?fail_edit_msg=".$message);

		}

	}
}

if(isset($_POST['submit3'])){

	if($_FILES['img']['size'] == 0){

		$message = "Shop page banner 3 has not been updated successfully !";
		header("location:../page_banner_shop.php?fail_edit_msg=".$message);
		
	}else{

	// select old banner
	$select_shop_banner3 = mysqli_query($con,"SELECT * FROM page_banner");
	while($res_shop_banner3 = mysqli_fetch_array($select_shop_banner3)){

		$shop_banner3 = $res_shop_banner3['shop3'];
	}
	// select old banner

    //delete old banner
	if(file_exists("../../img/page_banner/".$shop_banner3)){
	    unlink("../../img/page_banner/".$shop_banner3);
	}
	//delete old banner

	// save new banner
	$file_name = $_FILES['img']['name'];
   	$file_tmp = $_FILES['img']['tmp_name'];
   	$file_size = $_FILES['img']['size'];
   	$file_type= $_FILES['img']['type'];

    move_uploaded_file($file_tmp,"../../img/page_banner/".$file_name);
    // save new banner

	$run_update_banner = mysqli_query($con,"UPDATE page_banner SET shop3 = '$file_name'");

		if($run_update_banner > 0){

		$message = "Shop page banner 3 has been updated successfully";
		header("location:../page_banner_shop.php?success_edit_msg=".$message);

		}else{

		$message = "Shop page banner 3 has not been updated successfully !";
		header("location:../page_banner_shop.php?fail_edit_msg=".$message);

		}

	}
}

?>