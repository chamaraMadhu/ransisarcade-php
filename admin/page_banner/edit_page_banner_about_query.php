<?php

include "../inc/db_conn.php";

if(isset($_POST['submit'])){

	if($_FILES['img']['size'] == 0){

		$message = "About page banner has not been updated successfully !";
		header("location:../page_banner_about.php?fail_edit_msg=".$message);
		
	}else{

	// select old banner
	$select_about_banner = mysqli_query($con,"SELECT about FROM page_banner");
	while($res_about_banner = mysqli_fetch_array($select_about_banner)){

		$about_banner = $res_about_banner['about'];
	}
	// select old banner

    //delete old banner
	if(file_exists("../../img/page_banner/".$about_banner)){
	    unlink("../../img/page_banner/".$about_banner);
	}
	//delete old banner

	// save new banner
	$file_name = $_FILES['img']['name'];
   	$file_tmp = $_FILES['img']['tmp_name'];
   	$file_size = $_FILES['img']['size'];
   	$file_type= $_FILES['img']['type'];

    move_uploaded_file($file_tmp,"../../img/page_banner/".$file_name);
    // save new banner

	$run_update_banner = mysqli_query($con,"UPDATE page_banner SET about = '$file_name'");

		if($run_update_banner > 0){

		$message = "About page banner has been updated successfully";
		header("location:../page_banner_about.php?success_edit_msg=".$message);

		}else{

		$message = "About page banner has not been updated successfully !";
		header("location:../page_banner_about.php?fail_edit_msg=".$message);

		}

	}
}

?>