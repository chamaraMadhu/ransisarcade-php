<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">My Profile</li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>MY PROFILE</h2><hr>
			<?php
				if(isset($_GET['fail_update_user_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_update_user_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

			<?php

			include "inc/db_conn.php";

				$id = $_SESSION['id'];

				$sel_user = "SELECT * FROM user WHERE id ='$id'";
				$run_user = mysqli_query($con,$sel_user);

				while($res_user = mysqli_fetch_array($run_user)){

					$uname = $res_user ['user_name'];
					$role = $res_user ['role'];
					$img = $res_user ['image'];
					$mail = $res_user ['email'];
					$pwd = base64_decode($res_user ['password']);
				}

			?>
				<div class="row">
					<table class="table text-left table-hover col-6">
					  <tbody class="table-striped ">
					    <tr>
					      <td><img src="../img/user/<?php echo $img ?>" width="150px" /></td>
					      <td style="padding-top: 40px">
					      	<h2 class="text-capitalize"><?php echo $role ?></h2>
					      	<p class="text-capitalize" style="font-size: 18px"><?php echo $uname ?></p>
					      </td>
					    </tr>
					    <tr>
					      <td><b>Privileges</b></td>
					      <td><?php if($_SESSION['add']=='1') echo 'add,' ?> <?php if($_SESSION['view']=='1') echo 'view,' ?> <?php if($_SESSION['edit']=='1') echo 'edit,' ?> <?php if($_SESSION['del']=='1') echo 'delete' ?> <?php if($_SESSION['role']=='Owner') echo '(only for user functions)' ?><?php if($_SESSION['role']<>'Owner') echo '(only for product functions)' ?></td>
					    </tr>
					    <tr>
					      <td><b>Mail</b></td>
					      <td><?php echo $mail ?></td>
					    </tr>
					    <tr>
					      <td><b>Password</b></td>
					      <td><?php echo $pwd ?></td>
					    </tr>

					  </tbody>
					</table>
					
				</div>
				<a href="edit_my_profile.php" class="btn btn-success mb-3 btn-sm" type="submit" name="submit">Edit Profile</a>
				
	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>