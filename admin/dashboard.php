<?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>
<!-- content -->
<div class="col-sm-3 col-md-10 bg-danger p-0">
    <div class="page-wrapper">

      <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-danger" style="font-size: 14px">
              <li class="breadcrumb-item text-light"><a class="text-light" href="dashboard.php" style="text-decoration: none">Dashboard</a></li>
          </ol>
      </nav>

      <div class="container-fluid">

      <h2>STATISTICS</h2><hr>

        <div class="container-fluid bg-white mb-4 p-2">

        <?php 

        include "inc/db_conn.php";

        $tot_product = "SELECT * FROM product";
        $run_tot_product = mysqli_query($con,$tot_product);
        $res_tot_product = mysqli_num_rows($run_tot_product);
        ?>

        <!-- Style wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Style Wise</td></tr>

          <?php 

          $get_style_row = "SELECT style, count(style) FROM product GROUP BY style";
          $run_style_row = mysqli_query($con,$get_style_row);
          $res_style_row = mysqli_num_rows($run_style_row);

          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">Style</td>
            <td width="175px">No. of Products</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_style_row + 2 ) ?>" bgcolor="#fff"><div id="piechart1" style="width: 100%; min-height: <?php echo (($res_style_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_style = "SELECT style, count(style) AS count_style FROM product GROUP BY style";
          $run_style = mysqli_query($con,$get_style);

          while($res_style = mysqli_fetch_array($run_style )){

          ?>

          <tr align="center">
            <td><?php echo $res_style['style'] ?></td>
            <td><?php echo $res_style['count_style'] ?></td>
            <td><?php echo round(($res_style['count_style']/$res_tot_product*100),2) ?>%</td>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_product ?></td>
            <td>100%</td>
          </tr>

        </table>
        <!-- Style wise -->
        
        <!-- Fabric wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Fabric Wise</td></tr>

          <?php 

          $get_fabric_row = "SELECT fabric, count(fabric) FROM product GROUP BY fabric";
          $run_fabric_row = mysqli_query($con,$get_fabric_row);
          $res_fabric_row = mysqli_num_rows($run_fabric_row);

          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">Fabric</td>
            <td width="175px">No. of Products</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_fabric_row + 2 ) ?>" bgcolor="#fff"><div id="piechart2" style="width: 100%; min-height: <?php echo (($res_fabric_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_fabric = "SELECT fabric, count(fabric) AS count_fabric FROM product GROUP BY fabric";
          $run_fabric = mysqli_query($con,$get_fabric);

          while($res_fabric = mysqli_fetch_array($run_fabric )){

          ?>

          <tr align="center">
            <?php if(empty($res_fabric['fabric'])){

            }else{ 

            ?>

            <td><?php echo $res_fabric['fabric'] ?></td>
            <td><?php echo $res_fabric['count_fabric'] ?></td>
            <td><?php echo round(($res_fabric['count_fabric']/$res_tot_product*100),2) ?>%</td>

            <?php }  ?>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_product ?></td>
            <td>100%</td>
          </tr>

        </table>
        <!-- Fabric wise -->

        <!-- Occasion wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Occasion Wise</td></tr>

          <?php 

          $get_occasion_row = "SELECT occasion, count(occasion) FROM product GROUP BY occasion";
          $run_occasion_row = mysqli_query($con,$get_occasion_row);
          $res_occasion_row = mysqli_num_rows($run_occasion_row);


          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">occasion</td>
            <td width="175px">No. of Products</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_occasion_row + 2 ) ?>" bgcolor="#fff"><div id="piechart3" style="width: 100%; height: <?php echo (($res_occasion_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_occasion = "SELECT occasion, count(occasion) AS count_occasion FROM product GROUP BY occasion";
          $run_occasion = mysqli_query($con,$get_occasion );

          while($res_occasion = mysqli_fetch_array($run_occasion)){

          ?>

          <tr align="center">
            <td><?php echo $res_occasion['occasion'] ?></td>
            <td><?php echo $res_occasion['count_occasion'] ?></td>
            <td><?php echo round(($res_occasion['count_occasion']/$res_tot_product*100),2) ?>%</td>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_product ?></td>
            <td>100%</td>
          </tr>
        </table>
        <!-- Occasion wise -->

        <!-- Color wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Color Wise</td></tr>

          <?php 

          $get_color_row = "SELECT color, count(color) FROM product GROUP BY color";
          $run_color_row = mysqli_query($con,$get_color_row);
          $res_color_row = mysqli_num_rows($run_color_row);


          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">color</td>
            <td width="175px">No. of Products</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_color_row + 2 ) ?>" bgcolor="#fff"><div id="piechart4" style="width: 100%; height: <?php echo (($res_color_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_color = "SELECT color, count(color) AS count_color FROM product GROUP BY color";
          $run_color = mysqli_query($con,$get_color );

          while($res_color = mysqli_fetch_array($run_color)){

          ?>

          <tr align="center">
            <td><?php echo $res_color['color'] ?></td>
            <td><?php echo $res_color['count_color'] ?></td>
            <td><?php echo round(($res_color['count_color']/$res_tot_product*100),2) ?>%</td>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_product ?></td>
            <td>100%</td>
          </tr>
        </table>
        <!-- Color wise -->

        <!-- price wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Price Wise</td></tr>

          <?php 

          $get_price_row = "SELECT price, count(price) FROM product GROUP BY price";
          $run_price_row = mysqli_query($con,$get_price_row);
          $res_price_row = mysqli_num_rows($run_price_row);


          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">Price</td>
            <td width="175px">No. of Products</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_price_row + 2 ) ?>" bgcolor="#fff"><div id="piechart5" style="width: 100%; height: 400px"></div></td>
          </tr>

          <?php 

          $get_price_range = "SELECT * FROM price_range";
          $run_price_range = mysqli_query($con,$get_price_range);

            while($res_price_range = mysqli_fetch_array($run_price_range)){

              $min = $res_price_range['min'];
              $max = $res_price_range['max'];

              $count_price_product = "SELECT * FROM product  WHERE price >= '$min' AND price < '$max'";
              $run_count_price_product = mysqli_query($con,$count_price_product);
              $res_count_price_product = mysqli_num_rows($run_count_price_product);
          ?>

          <tr align="center">
            <td>Rs. <?php echo $min ?> - Rs. <?php echo $max ?></td>
            <td><?php echo $res_count_price_product ?></td>
            <td><?php echo round(($res_count_price_product/$res_tot_product*100),2) ?>%</td>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_product ?></td>
            <td>100%</td>
          </tr>
        </table>
        <!-- Color wise -->

        </div>
      </div>
      
    </div>
</div>  


<!-- content -->

<!-- chart.js -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

      <!-- style wise -->
      <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Style', 'Products'],

         <?php 

        $get = "SELECT style, count(style) AS count FROM product GROUP BY style";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          ?>
          ['<?php echo $res['style'] ?>',<?php echo $res['count'] ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart1'));

        chart.draw(data, options);
      }
    </script>
    <!-- style wise -->

    <!-- fabric wise -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Fabric', 'Products'],

         <?php 

        $get = "SELECT fabric, count(style) AS count FROM product GROUP BY fabric";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          ?>
          ['<?php echo $res['fabric'] ?>',<?php echo $res['count'] ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
      }
    </script>
    <!-- fabric wise -->

    <!-- Occasion wise -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['occasion', 'Products'],

         <?php 

        $get = "SELECT occasion, count(occasion) AS count FROM product GROUP BY occasion";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          ?>
          ['<?php echo $res['occasion'] ?>',<?php echo $res['count'] ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

        chart.draw(data, options);
      }
    </script>
    <!-- Occasion wise -->

    <!-- Color wise -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['color', 'Products'],

         <?php 

        $get = "SELECT color, count(color) AS count FROM product GROUP BY color";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          ?>
          ['<?php echo $res['color'] ?>',<?php echo $res['count'] ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart4'));

        chart.draw(data, options);
      }
    </script>
    <!-- Color -->

    <!-- Color wise -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Price Range', 'Products'],

         <?php 

          $get_price_range = "SELECT * FROM price_range";
          $run_price_range = mysqli_query($con,$get_price_range);

            while($res_price_range = mysqli_fetch_array($run_price_range)){

              $min = $res_price_range['min'];
              $max = $res_price_range['max'];

              $count_price_product = "SELECT * FROM product  WHERE price >= '$min' AND price < '$max'";
              $run_count_price_product = mysqli_query($con,$count_price_product);
              $res_count_price_product = mysqli_num_rows($run_count_price_product);

          ?>
          ['Rs. <?php echo $min ?> - Rs. <?php echo $max ?>',<?php echo $res_count_price_product ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart5'));

        chart.draw(data, options);
      }
    </script>
    <!-- Color -->
<!-- chart.js -->


<?php 
  include "inc/footer.php";
?>

