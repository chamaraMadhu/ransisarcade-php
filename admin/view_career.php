<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php'); 
}
if($_SESSION['view']==0){
  header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

        <!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="page-wrapper">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
					      <li class="breadcrumb-item text-light active" aria-current="page">Career</li>
		            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="view_career.php" style="text-decoration: none">View Career</a></li>
		        </ol>
		    </nav>

		    <div class="container-fluid">					
		        <h2>CAREER</h2><hr>
		        <?php
            // aleart massages for editing category
              if(isset($_GET['success_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for editing category 


            // aleart massages for deleting category
              if(isset($_GET['success_del_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for deleting category
             ?>

		            <div class="container-fluid bg-white ">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-uppercase text-white">View Career</h6>
			            </div>

			           	<div class="mb-4">
                    <div class="card-body">
                        
                      <div class="table-responsive">
                          <table id="zero_config" class="table table-hover">
                              <thead>
                                  <tr style="background-color: gray; color:#fff">
                                      <td>Job Type</td>
                                      <td>Position</td>
                                      <td>Advertisement</td>
                                      <td>Closing Date</td>
                                      <td>Status</td>
                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){  ?>
                                      <td width="60px">Action</td>
                                      <?php } } } ?>
                                  </tr>
                              </thead>
                              <tbody>

                              <?php

                                include "inc/db_conn.php";

                                $get_career = "SELECT * FROM career";
                                $run_career = mysqli_query($con,$get_career);

                                while($res_career = mysqli_fetch_array($run_career)){

                                  $id = $res_career['id'];
                                  $type = $res_career['job_type'];
                                  $position = $res_career['position'];
                                  $advert = $res_career['advertisement'];
                                  $closing_date = $res_career['closing_date'];
                                  $status = $res_career['status'];

                                ?>

                                  <tr>
                                      <td><?php echo $type; ?></td>
                                      <td><?php echo $position; ?></td>
                                      <td align="center"><img src="../img/career/<?php echo $advert; ?>" width="70px" class="py-1"></td>
                                      <td><?php echo $closing_date;  ?></td>
                                      <td><?php if($res_career['status']==1) { ?> <span style="color:darkgreen;font-weight:bold; float: left">Active</span> <?php }else{ ?> <span style="color:red;font-weight:bold">Deactive</span> <?php } ?> </td>

                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
                                      <td>
                                        <?php if ($_SESSION['edit']=='1'){ ?>
                                      	 <form method="GET" action="edit_career.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                                          	<button type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></button>&nbsp;
                                          </form>
                                          <?php } ?>

                                          <?php if ($_SESSION['del']=='1'){ ?>
                                          <form method="GET" action="career/delete_career_query.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                                          	<button type="submit" name="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                          </form>
                                          <?php } ?>
                                      </td>
                                      <?php } } } ?>
						                      </tr>

                                <?php
                            } 

                            ?>

                              </tfoot>
                          </table>
                      </div>

                    </div>
                  </div>

		            </div>
		    </div>      
		</div>
   
 </div>
 <!-- content -->
 
<?php 
  include "inc/footer.php";
?>