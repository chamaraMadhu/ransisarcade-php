<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Bootstrap CSS -->

    <!-- favicon -->
    <link href="img/logo.png" rel="icon"/>
    <!-- favicon -->

    <!--<link rel="stylesheet" href="css/custom.css" />-->
    <title>Admin login</title>
  </head>

  <body style="background-color: gray">

    <div class="container-fluid">

      <form class="px-4 py-3 border col-sm-3 col-md-5 col-lg-3 bg-white" method="POST" action="login/login_query.php" style="margin:auto;margin-top:150px; border-radius: 2%">
        

        <img class="ml-3 mb-3" src="../img/logo.png" width="75px">
        <span class="text-center text-muted" style="font-size: 25px">ADMIN LOGIN</span>

        <?php
        if(isset($_GET['logout_msg'])){
        ?>
          <div class="alert alert-success alert-block" style="line-height: 15px">
              <button type="button" class="close" data-dismiss="alert" style="font-size: 15px">x</button>
              <strong><?php echo $_GET['logout_msg']; ?> </strong> 
          </div>

        <?php } ?>

        <?php
        if(isset($_GET['invalid_msg'])){
        ?>
          <div class="alert alert-danger alert-block" style="line-height: 15px">
              <button type="button" class="close" data-dismiss="alert" style="font-size: 15px">x</button>
              <strong><?php echo $_GET['invalid_msg']; ?> </strong> 
          </div>

        <?php } ?>

        <?php
        if(isset($_GET['bypass_msg'])){
        ?>
          <div class="alert alert-danger alert-block"  style="line-height: 15px">
              <button type="button" class="close" data-dismiss="alert" style="font-size: 15px">x</button>
              <strong><?php echo $_GET['bypass_msg']; ?> </strong> 
          </div>

        <?php } ?>

        <div class="form-group">
          <label for="exampleDropdownFormEmail1">Email address</label>
          <input type="email" name="email" class="form-control" id="exampleDropdownFormEmail1" placeholder="email@example.com" required="">
        </div>
        <div class="form-group">
          <label for="exampleDropdownFormPassword1">Password</label>
          <input type="password" name="password" class="form-control" id="exampleDropdownFormPassword1" placeholder="Password" required="">
        </div>
        <button type="submit" class="btn btn-sm mr-2" style="background-color: gray; color: #fff">Sign in</button>
        <input class="btn btn-sm btn-danger" type="reset" value="Reset">

        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

  </body>
</html>