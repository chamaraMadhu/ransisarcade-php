<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <!-- Bootstrap CSS -->

    <!-- favicon -->
    <link href="../img/logo.png" rel="icon"/>
    <!-- favicon -->

    <!-- datatable -->
    <link href="../css/backend_css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- datatable -->

    <!-- <link rel="stylesheet" href="../css/backend_css/custom.css" /> -->
    <title>Ransi's Arcade - Admin Pannel</title>
  </head>
  <style>
    #header > form{
      margin-left: 225px;
    }

    #header form > input{
      padding-left: 10px;
      line-height: 25px;
      width: 250px;
      font-size: 14px;
      border-radius: 5px;
      border-style: solid;
      border-width: 1px;
    }

    #btn{
      background-color: #000;
      border-style: none;
      color: #fff;
      cursor: pointer;
    }

    .profile_feature a,span,ul{
      float: right;
    }

    #pofile-img{
      float: right;
    }

    .accordion .card-header:after{
      font-family: 'fontAwesome';
      content: "\f106";
      float: right;
      color: #fff;
    }

    .accordion .collapsed:after{
        content: "\f107";
        color: #fff;
    }
  </style>

  <body>
    
     <!-- starts navbar -->
    <nav class="navbar sticky-top navbar-expand-lg" style="background-color: #000" >
      <a href="dashboard.php"><img src="../img/logo1.png" width="90" height="50"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto container-fluid">

          <li class="nav-item" >
            <p  style="color: #fff; margin: 10px 0 0 15px"><a href="dashboard.php"  style="color: #fff; text-decoration: none"> ADMIN PANEL</a></p>
          </li>

          <div class="mt-2" id="header">
            <form action="search_product.php" method="GET">
              <input type="search" name="pro_id" placeholder="Product ID">
              <button name="submit" id="btn"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
          </div>
        </ul>

        <div class="nav_item profile_feature" style="width: 600px">
  
          <a href="my_profile.php"><img id="pofile-img" src="../img/user/<?php  if (isset($_SESSION['role'])){echo  $_SESSION['image']; } ?>" alt="user" class="rounded-circle ml-2 mt-1" width="30" height="30" style="cursor: pointer"></a>

          <a class="btn btn-link text-light" style="font-size: 13px; text-decoration: none" href="login/logout_query.php"><i class="fa fa-lock" aria-hidden="true"></i> Logout</a>

          <a class="btn btn-link text-light" style="font-size: 13px; text-decoration: none" href="my_profile.php"><i class="fa fa-user"></i> My Profile</a>

          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']=='Owner'){ ?>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle text-light" style="font-size: 13px; cursor: pointer" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Setting</a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                <?php if($_SESSION['add']=='1'){ ?>
                  <a class="dropdown-item" href="add_user.php" style="font-size: 13px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; &nbsp; Add User</a>
                <?php } ?>

                <?php if($_SESSION['view']=='1'){ ?>
                  <a class="dropdown-item" href="view_user.php" style="font-size: 13px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; &nbsp; View Other User</a>
                <?php } ?>
              </div>
            </li>
          </ul>
          <?php } } ?>

          <span class="btn text-white" style="font-size: 13px; cursor: default">Role : <?php  if (isset($_SESSION['role'])){echo  $_SESSION['role'];}else { echo "none";} ?> </span>

        </div>

      </div>
    </nav>
    <!-- ends navbar -->