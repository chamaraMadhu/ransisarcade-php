<!-- container-->
    <div class="container-fluid">
        <div class="row" style="min-height: 700px">

            <!-- sidebar -->
            <div class="col-2 p-0" style="background-color: #000">
              <div class="list-group ">

                <a href="dashboard.php" class="list-group-item list-group-item-action justify-content-between text-light" style="border-bottom-color: gray; border-bottom-style:groove; font-weight: 500; background-color: #000; font-size: 14px" ><i class="fa fa-pie-chart" style="color: red"></i> &nbsp; Dash board</a>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse"  data-parent="#accordion" href="#collapseOne" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-product-hunt" aria-hidden="true" style="color: red"></i> &nbsp; Product</a>
                      </div>

                      <div class="card-block collapse" id="collapseOne">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_product.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Product</a>
                           <?php } } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="view_product.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Product</a>
                           <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

				        <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; font-weight: 500; border-bottom-style: groove; font-size: 14px" data-toggle="collapse"  data-parent="#accordion" href="#collapseTwo" aria-expanded="true">
                        <a class="card-title text-light" style="cursor: pointer"><i class="fa fa-star" aria-hidden="true" style="color: red"> </i> &nbsp; Style</a>
                      </div>

                      <div class="card-block collapse" id="collapseTwo">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_style.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Style</a>
                          <?php } } } ?>

                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                            <a href="view_style.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Style</a>
                          <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; font-weight: 500; border-bottom-style: groove; font-size: 14px" data-toggle="collapse"  data-parent="#accordion" href="#collapseThree" aria-expanded="true">
                        <a class="card-title text-light" style="cursor: pointer"><i class="fa fa-female" aria-hidden="true" style="color: red"> </i> &nbsp; Fabric</a>
                      </div>

                      <div class="card-block collapse" id="collapseThree">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_fabric.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Fabric</a>
                          <?php } } } ?>

                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                            <a href="view_fabric.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Fabric</a>
                          <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion"  style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse"  data-parent="#accordion" href="#collapse5" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-usd" style="color: red"></i> &nbsp; Price Range</a>
                      </div>

                      <div class="card-block collapse" id="collapse5">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_price.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add price</a>
                           <?php } } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="view_price.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View price</a>
                           <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse"  data-parent="#accordion" href="#collapse6" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-adjust" style="color: red"></i> &nbsp; Color Range</a>
                      </div>

                      <div class="card-block collapse" id="collapse6">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_color.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Color</a>
                           <?php } } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="view_color.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Color</a>
                           <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse" style="border-bottom-color: gray; border-bottom-style:groove;"  data-parent="#accordion" href="#collapse8" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-facebook" aria-hidden="true"  style="color: red"></i> &nbsp; Social Media</a>
                      </div>

                      <div class="card-block collapse" id="collapse8">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_social_media.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Social Media</a>
                           <?php } } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="view_social_media.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Social Media</a>
                           <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse"  data-parent="#accordion" href="#collapse9" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-suitcase" style="color: red"></i> &nbsp; Career</a>
                      </div>

                      <div class="card-block collapse" id="collapse9">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_career.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Career</a>
                           <?php } } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="view_career.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Career</a>
                           <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse"  data-parent="#accordion" href="#collapse10" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-adn" style="color: red"></i> &nbsp; Advertisement</a>
                      </div>

                      <div class="card-block collapse" id="collapse10">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Owner'){ if($_SESSION['add']=='1'){ ?>
                            <a href="add_advertisement.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-plus" aria-hidden="true" style="color: #099"></i> &nbsp; Add Advertisement</a>
                           <?php } } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="view_advertisement.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; View Advertisement</a>
                           <?php } } ?>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="accordion" style="cursor: pointer">
                    <div class="">
                      <div class="card-header collapsed" style="border-bottom-color: gray; border-bottom-style:groove;" data-toggle="collapse"  data-parent="#accordion" href="#collapse11" aria-expanded="true">
                        <a class="card-title text-light" style="font-weight: 500; font-size: 14px; cursor: pointer"><i class="fa fa-image" style="color: red"></i> &nbsp; Page Banner</a>
                      </div>

                      <div class="card-block collapse" id="collapse11">
                        <div class="card-body p-0">
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                            <a href="page_banner_home.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; Home Page</a>
                           <?php } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="page_banner_about.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; About Us Page</a>
                           <?php } } ?>

                           <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                            <a href="page_banner_shop.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; Shop Page</a>
                           <?php } } ?>
                          
                          <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="page_banner_faq.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; FAQ Page</a>
                           <?php } } ?>

                           <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="page_banner_career.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; Career Page</a>
                           <?php } } ?>

                           <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                           <a href="page_banner_contact.php" class="list-group-item list-group-item-action justify-content-between bg-dark text-light pl-5" style="font-size: 12px"><i class="fa fa-pencil" aria-hidden="true" style="color: #099"></i> &nbsp; Contact us Page</a>
                           <?php } } ?>

                        </div>
                      </div>
                    </div>
                </div>

                <a href="view_customer_like.php" class="list-group-item list-group-item-action justify-content-between text-light" style="border-bottom-color: gray; border-bottom-style:groove;font-weight: 500; background-color: #000; font-size: 14px" ><i class="fa fa-thumbs-up" aria-hidden="true" style="color: red"></i> &nbsp; Customer Likes</a>

                <a href="view_customer_comment.php" class="list-group-item list-group-item-action justify-content-between text-light" style="border-bottom-color: gray; border-bottom-style:groove;font-weight: 500; background-color: #000; font-size: 14px" ><i class="fa fa-comments" aria-hidden="true" style="color: red"></i> &nbsp; Customer Comments</a>

                <a href="contact_info.php" class="list-group-item list-group-item-action justify-content-between text-light" style="border-bottom-color: gray; border-bottom-style:groove;font-weight: 500; background-color: #000; font-size: 14px" ><i class="fa fa-info-circle" style="color: red"></i> &nbsp; Contact Info</a>

            </div>
        </div>
        <!-- sidebar -->