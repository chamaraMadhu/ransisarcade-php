    </div>
</div>
<!-- container-->  

<!-- footer -->
    <footer class="text-center text-light pt-1" style="font-size: 12px;height: 25px; background-color: #000">
        All Rights Reserved by Ransi's Arcade. Designed and Developed by Chamara Madhushanka.
    </footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

 <!-- data table Files -->
<script src="../js/backend_js/datatables.min.js"></script>
<script>
  $('#zero_config').DataTable();
</script>
<!-- data table Files -->

<!-- popper -->
<script src="../js/backend_js/popper.min.js"></script>
<!-- popper -->

<!-- alert for product deletion -->
<script>
    $(".del_product").click(function(){
        if(confirm("Are you sure to delete this products?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for product deletion -->

<!-- alert for all product deletion -->
<script>
    $(".del_all_product").click(function(){
        if(confirm("Are you sure to delete all products?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for all product deletion -->

<!-- alert for user deletion -->
<script>
    $(".del_user").click(function(){
        if(confirm("Are you sure to delete this user?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for user deletion -->

<!-- alert for style deletion -->
<script>
    $(".del_style").click(function(){
        if(confirm("Are you sure to delete this style?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for style deletion -->

<!-- alert for fabric deletion -->
<script>
    $(".del_fabric").click(function(){
        if(confirm("Are you sure to delete this fabric?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for fabric deletion -->

<!-- alert for occasion deletion -->
<script>
    $(".del_occasion").click(function(){
        if(confirm("Are you sure to delete this occasion?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for occasion deletion -->


<!-- alert for like deletion -->
<script>
    $(".del_like").click(function(){
        if(confirm("Are you sure to delete this customer like?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for like deletion -->

<!-- alert for comment deletion -->
<script>
    $(".del_comment").click(function(){
        if(confirm("Are you sure to delete this customer comment?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for comment deletion -->

</body>
</html>

