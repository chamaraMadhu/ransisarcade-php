<?php

include "../inc/db_conn.php";

if(isset($_GET['submit'])){

	$id = $_GET['id']; // get user id

	// select user image to delete
	$select_user_image = mysqli_query($con,"SELECT image FROM user WHERE id = '$id'");
	while($res_user_image = mysqli_fetch_array($select_user_image)){

		$user_image = $res_user_image ['image'];
	}
	// select user image to delete

	// delete user 
	$del_user = "DELETE FROM user WHERE id = $id";
	$run_del_user = mysqli_query($con,$del_user);

	if($run_del_user > 0){

		// delete user image
		if(file_exists("../../img/user/".$user_image)){
		    unlink("../../img/user/".$user_image);
		}
		// delete user image

		$message = "User has been deleted successfully !";
		header("location:../view_user.php?success_del_msg=".$message);

	}else{

		$message = "User has not been deleted successfully !";
		header("location:../view_user.php?fail_del_msg=".$message);
	}
	// delete user 
}

?>