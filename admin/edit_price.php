<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Price Range</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Price</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>PRICE RANGE</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit Price</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="price_range/edit_price_query.php" method="GET" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_price_range = "SELECT * FROM price_range WHERE id = $id";
	                $run_price_range = mysqli_query($con,$get_price_range);

	                while($res_price_range = mysqli_fetch_array($run_price_range)){

	                 	$id = $res_price_range['id'];
	                    $min = $res_price_range['min'];
	                    $max = $res_price_range['max'];
					?>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Price ID <b class="text-danger">*</b></label>
							<div class="col-sm-1">
								<input type="text" name="min" class="form-control" id="validationCustom01" value="<?php echo $id ?>" required>
								<div class="invalid-feedback">
								Please insert the price ID.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Min (Rs.) <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="min" class="form-control" id="validationCustom01" value="<?php echo $min ?>" required>
								<div class="invalid-feedback">
								Please insert the minimum price.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Max (Rs.) <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<input type="text" name="max" class="form-control" id="validationCustom01" value="<?php echo $max ?>" required>
								<div class="invalid-feedback">
								Please insert the maximum price.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="view_price.php" class="btn btn-danger mb-3 btn-sm" type="submit">Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Price</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>