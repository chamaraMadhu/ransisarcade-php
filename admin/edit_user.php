<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Owner'){
	 	header('location:dashboard.php');
}
if($_SESSION['edit']<>'1'){
 	 header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Setting</li>
            <li class="breadcrumb-item text-light active" aria-current="page">Edit User</li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>SETTING</h2><hr>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color: gray">
	                <h6 class="col-12 text-white">EDIT USER</h6>
	            </div>

	           	<form action="setting/edit_user_query.php" method="POST" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
				<div class="form-row">

					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_user = "SELECT * FROM user WHERE id = $id";
	                $run_user = mysqli_query($con,$get_user);

	                while($res_user = mysqli_fetch_array($run_user)){

	                 	$id = $res_user['id'];
	                    $uname = $res_user['user_name'];
	                    $role = $res_user['role'];
	                    $img = $res_user['image'];
	                    $mail = $res_user['email'];
	                    $pwd = base64_decode($res_user['password']);
	                    $add = $res_user['addition'];
	                    $view = $res_user['view'];
	                    $edit = $res_user['edit'];
	                    $del = $res_user['deletion'];

					?>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">User name <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="uname" value="<?php echo $uname ?>" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the user name.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Role <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="role" value="<?php echo $role ?>" class="form-control" id="validationCustom02" required>
							<div class="invalid-feedback">
							Please insert a role.
							</div>
						</div>
					</div>
					
					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Image <b class="text-danger">*</b></label>
						<div class="col-sm-6">
							<input class="col-sm-6"  type="file" name="img" class="form-control"  id="validationCustom03">
							<img src="../img/user/<?php echo $img ?>" width="40px" class="py-1"/>
							<div class="invalid-feedback">
							Please choose user image.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Mail <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="email" name="mail" value="<?php echo $mail ?>" class="form-control" id="validationCustom04" required>
							<div class="invalid-feedback">
							Please insert a mail.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Password <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="Password" name="pwd" value="<?php echo $pwd ?>" class="form-control" id="validationCustom05" required>
							<div class="invalid-feedback">
							Please insert a password.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Privilege <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							View - <input type="checkbox" class="mt-2" name="view" <?php if($view == "1"){ ?> checked <?php } ?> /> &nbsp; &nbsp;
							Add - <input type="checkbox" class="mt-2" name="add" <?php if($add == "1"){ ?> checked <?php } ?> /> &nbsp; &nbsp;
							Edit - <input type="checkbox" class="mt-2" name="edit" <?php if($edit == "1"){ ?> checked <?php } ?> /> &nbsp; &nbsp;
							Del - <input type="checkbox" class="mt-2" name="del" <?php if($del == "1"){ ?> checked <?php } ?> > &nbsp; &nbsp;
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right"></label>
						<input type="hidden" name="id" value="<?php echo $id ?>"/>
						<div class="col-sm-4">
							<a href="view_user.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
							<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit User</button>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
                      	<label class="col-sm-2 col-form-label text-right"></label>
                      	<div class="col-sm-4">
                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
                      	</div>
                    </div>

				<?php } ?>
				
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>