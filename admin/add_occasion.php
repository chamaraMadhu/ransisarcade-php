<?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
if($_SESSION['role']=='Owner'){
	header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Occasion</li>
            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="add_occasion.php" style="text-decoration: none">Add Occasion</a></li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>OCCASION</h2><hr>
			<?php
				if(isset($_GET['success_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color: gray">
	                <h6 class="col-12 text-uppercase text-white">Add Occation</h6>
	            </div>

	           	<form action="occasion/add_occasion_query.php" method="POST" class="needs-validation mt-3" novalidate>
				<div class="form-row">

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Occasion <b class="text-danger">*</b></label>
						<div class="col-sm-4">
							<input type="text" name="occasion" class="form-control" id="validationCustom01" required>
							<div class="invalid-feedback">
							Please insert the occasion.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Activate </label>
						<div class="col-sm-4">
							<input type="checkbox" class="mt-2" name="status"/>
						</div>
					</div>

					<div class="col-md-12 mb-0 form-group row">
						<label class="col-sm-2 col-form-label text-right"></label>
						<div class="col-sm-4">
							<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Add Occasion</button>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
                      <label class="col-sm-2 col-form-label text-right"></label>
                      <div class="col-sm-4">
                        <b class="text-danger" style="font-size: 14px">* Denotes Required</b>
                      </div>
                    </div>
                </div>
				
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>