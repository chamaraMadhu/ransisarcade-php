text-light <?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
if($_SESSION['role']=='Owner'){
	header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Social Media</li>
            <li class="breadcrumb-item text-light active" aria-current="page">Edit Social Media</li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>SOCIAL MEDIA</h2><hr>
			<?php
				if(isset($_GET['success_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color: grey">
	                <h6 class="col-12 text-white">Edit Social Media</h6>
	            </div>

	           	<form action="social_media/edit_social_media_query.php" method="GET" class="needs-validation mt-3" novalidate>
				<div class="form-row">

					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_social_media = "SELECT * FROM social_media WHERE id = $id";
	                $run_social_media = mysqli_query($con,$get_social_media);

	                while($res_social_media = mysqli_fetch_array($run_social_media)){

	                 	$id = $res_social_media['id'];
	                    $title = $res_social_media['title'];
	                    $url = $res_social_media['url'];
	                    $code = $res_social_media['font_awesome_code'];
					?>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">ID <b class="text-danger">*</b></label>
						<div class="col-sm-1">
							<input type="text" name="id" class="form-control" id="validationCustom01" value="<?php echo $id ?>" required>
							<div class="invalid-feedback">
							Please insert the social media ID.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Title <b class="text-danger">*</b></label>
						<div class="col-sm-3">
							<input type="text" name="title" class="form-control" id="validationCustom01" value="<?php echo $title ?>" required>
							<div class="invalid-feedback">
							Please insert the title.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">URL </label>
						<div class="col-sm-6">
							<input type="text" name="url" class="form-control" id="validationCustom01" value="<?php echo $url ?>">
							<div class="invalid-feedback">
							Please insert the URL.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Code <b class="text-danger">*</b></label>
						<div class="col-sm-6">
							<input type="text" name="code" class="form-control" id="validationCustom01" value='<?php echo $code ?>'/>
							<div class="invalid-feedback">
							Please insert the font awesome Code.
							</div>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right"></label>
						<input type="hidden" name="id" value="<?php echo $id ?>"/>
						<div class="col-sm-4">
							<a href="view_social_media.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
							<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Social Media</button>
						</div>
					</div>

					<div class="col-md-12 mb-3 form-group row">
                      	<label class="col-sm-2 col-form-label text-right"></label>
                      	<div class="col-sm-4">
                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
                      	</div>
                    </div>

					<?php
						}
					?>
				
			</form>

        </div>
    </div>      
</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>