<?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page">Contact Info</li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>CONTACT INFO</h2><hr>
			<?php
				if(isset($_GET['success_edit_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
	                </div>

			<?php } ?>

            <div class="container-fluid bg-white ">

	            <div class="row pt-2" style="background-color: gray">
	                <h6 class="col-12 text-white text-uppercase">Add Contact</h6>
	            </div>

				<?php

                include "inc/db_conn.php";

                $get_contact = "SELECT * FROM contact";

                $run_contact = mysqli_query($con,$get_contact);

                while($res_contact = mysqli_fetch_array($run_contact)){

                  $id = $res_contact['id'];
                  $house_no = $res_contact['house_no'];
                  $street = $res_contact['street'];
                  $city = $res_contact['city'];
                  $general = $res_contact['general_line'];
                  $mobi1 = $res_contact['mobile1'];
                  $mobi2 = $res_contact['mobile2'];
                  $mail = $res_contact['mail'];

                }

                ?>
				<div class="row">
					<table class="table text-left table-hover col-5">
					  <tbody class="table-striped ">
					    <tr>
					      <td><b>Address</b></td>
					      <td><?php echo $house_no ?></td>
					    </tr>
					    <tr>
					      <td></td>
					      <td><?php echo $street ?></td>
					    </tr>
					    <tr>
					      <td></td>
					      <td><?php echo $city ?></td>
					    </tr>
					    <tr>
					      <td><b>Contact No</b></td>
					      <td><?php echo $general ?> - (General Line)</td>
					    </tr>
					    <tr>
					      <td></td>
					      <td><?php echo $mobi1 ?> - (Sales)</td>
					    </tr>
					    <tr>
					      <td></td>
					      <td><?php echo $mobi2 ?></td>
					    </tr>
					    <tr>
					      <td><b>Mail</b></td>
					      <td><?php echo $mail ?></td>
					    </tr>
					  </tbody>
					</table>
					
				</div>
				<?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1'){ ?>
				<a href="edit_contact_info.php" class="btn btn-success mb-3 btn-sm" type="submit" name="submit">Edit Contact</a>
				<?php } } } ?>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>