<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php'); 
}
if($_SESSION['view']==0){
  header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

        <!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="page-wrapper">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
		            <li class="breadcrumb-item"><a class="text-light" href="dashboard.php">Admin Panel</a></li>
					      <li class="breadcrumb-item text-light active" aria-current="page">Category</li>
		            <li class="breadcrumb-item text-light active" aria-current="page">View Category</li>
		        </ol>
		    </nav>

		    <div class="container-fluid">					
		        <h2>CATEGORY</h2><hr>
		        <?php
            // aleart massages for editing category
              if(isset($_GET['success_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for editing category 


            // aleart massages for deleting category
              if(isset($_GET['success_del_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for deleting category
             ?>

		            <div class="container-fluid bg-white ">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-white">View Category</h6>
			            </div>

			           	<div class="mb-4">
                    <div class="card-body">  
                      <div class="table-responsive"> 

                        <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ if ($_SESSION['del']=='1'){ ?>
                            <form method="GET" action="category/delete_all_category_query.php">
                              <button type="submit" name="submit" class="btn btn-dark btn-sm mb-4 ml-3"><i class="fa fa-trash"></i> Delete All Caegories</button>
                            </form>
                            <?php } } } } ?>

                          <table id="zero_config" class="table table-bordered table-hover">
                              <thead>
                                  <tr style="background-color:#099;color:#fff">
                                      <th>Category ID</th>
                                      <th>Category Name</th>
                                      <th>Category Image</th>
                                      <th>Description</th>
                                      <th>Status</th>
                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){  ?>
                                      <th>Action</th>
                                      <?php } } } ?>
                                  </tr>
                              </thead>
                              <tbody>

                              <?php

                                $con = mysqli_connect('localhost','root','') or die ("Could not connect to the server");
                                mysqli_select_db($con,'diwe') or die ("Could not select database");

                                $get_category = "SELECT * FROM category";

                                $run_category = mysqli_query($con,$get_category);

                                while($res_category = mysqli_fetch_array($run_category)){

                                  $id = $res_category['id'];
                                  $name = $res_category['name'];
                                  $img = $res_category['image'];
                                  $desc = $res_category['description'];

                                ?>

                                  <tr>
                                      <td align="center"><?php echo $id ?></td>
                                      <td><?php echo $name; ?></td>
                                      <td align="center"><img src="../img/category/<?php echo $img; ?>" width="70px" class="border py-1"></td>
                                      <td><?php echo $desc;  ?></td>
                                      <td><?php if($res_category['status']==1) { ?> <span style="color:darkgreen;font-weight:bold; float: left">Active</span> <?php }else{ ?> <span style="color:red;font-weight:bold">Deactive</span> <?php } ?> </td>

                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
                                      <td>
                                        <?php if ($_SESSION['edit']=='1'){ ?>
                                      	 <form method="GET" action="edit_category.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                                          	<button type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></button>&nbsp;
                                          </form>
                                          <?php } ?>

                                          <?php if ($_SESSION['del']=='1'){ ?>
                                          <form method="GET" action="category/delete_category_query.php" style="float: left">
                                            <input type="hidden" name="id" value="<?php echo $id ?>"/>
                                          	<button type="submit" name="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                          </form>
                                          <?php } ?>
                                      </td>
                                      <?php } } } ?>
						                      </tr>

                                <?php
                            } 

                            ?>

                              </tfoot>
                          </table>
                      </div>

                    </div>
                  </div>

		            </div>
		    </div>      
		</div>
   
 </div>
 <!-- content -->
 
<?php 
  include "inc/footer.php";
?>