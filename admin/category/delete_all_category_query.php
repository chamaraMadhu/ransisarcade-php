<?php

include "../inc/db_connection.php";

if(isset($_GET['submit'])){


	// select category image to delete
	$select_cat_image = mysqli_query($con,"SELECT image FROM category");
	while($res_cat_image = mysqli_fetch_array($select_cat_image)){

		$cat_image = $res_cat_image ['image'];

		// delete category image
		if(file_exists("../../img/category/".$cat_image)){
		    unlink("../../img/category/".$cat_image);
		}
		// delete category image
	}
	// select category image to delete

	// delete category 
	$del_category = "DELETE FROM category";
	$run_del_category= mysqli_query($con,$del_category);

	if($run_del_category > 0){

		$message = "Category has been deleted successfully !";
		header("location:../view_category.php?success_del_msg=".$message);

	}else{

		$message = "Category has not been deleted successfully !";
		header("location:../view_category.php?fail_del_msg=".$message);
	}
	// delete category 
}

?>