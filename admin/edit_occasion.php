<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Occasion</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Occasion</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>STYLE</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-uppercase text-white">Edit Occasion</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="occasion/edit_occasion_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

                        $get_occasion = "SELECT * FROM occasion WHERE id = '$_GET[id]'";
                        $run_occasion = mysqli_query($con,$get_occasion);

                        while($res_occasion = mysqli_fetch_array($run_occasion)){

                        ?>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Occasion <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<input type="text" name="occasion" class="form-control" id="validationCustom01" value="<?php echo $res_occasion['occasion'] ?>" required>
								<div class="invalid-feedback">
								Please insert the occasion name.
								</div>
							</div>
						</div>						

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Activate </label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($res_occasion['status'] == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $res_occasion['id'] ?>"/>
								<a href="view_occasion.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Occasion</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>