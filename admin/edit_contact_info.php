<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Contact</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Contact</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>CONTACT</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: grey">
		                <h6 class="col-12 text-white">Edit Contact</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="contact/edit_contact_info_query.php" method="GET" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$get_contact = "SELECT * FROM contact WHERE id = '1'";
	                $run_contact = mysqli_query($con,$get_contact);

	                while($res_contact = mysqli_fetch_array($run_contact)){

	                 	$id = $res_contact['id'];
	                    $house_no = $res_contact['house_no'];
	                    $street = $res_contact['street'];
	                    $city = $res_contact['city'];
	                    $general = $res_contact['general_line'];
	                    $mobi1 = $res_contact['mobile1'];
	                    $mobi2 = $res_contact['mobile2'];
	                    $mail = $res_contact['mail'];
	                }
					?>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Address <b class="text-danger">*</b></label>
							<div class="col-sm-3">
								<input type="text" name="house_no" class="form-control" id="validationCustom01" placeholder="House No" value="<?php echo $house_no ?>" required>
								<input type="text" name="street" class="form-control mt-2" id="validationCustom01" placeholder="Street" value="<?php echo $street ?>" required>
								<input type="text" name="city" class="form-control mt-2" id="validationCustom01" placeholder="City" value="<?php echo $city ?>" required>
								<div class="invalid-feedback">
								Please insert the address.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Contact No <b class="text-danger">*</b></label>
							<div class="col-sm-3">
								<input type="text" name="general" class="form-control" id="validationCustom01" placeholder="General Line" value="<?php echo $general ?>" required>
								<input type="text" name="mobi1" class="form-control mt-2" id="validationCustom01" placeholder="Mobile 1" value="<?php echo $mobi1 ?>" required>
								<input type="text" name="mobi2" class="form-control mt-2" id="validationCustom01" placeholder="Mobile 2" value="<?php echo $mobi2 ?>" required>
								<div class="invalid-feedback">
								Please insert the address.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Mail <b class="text-danger">*</b></label>
							<div class="col-sm-3">
								<input type="text" name="mail" class="form-control" id="validationCustom01" value="<?php echo $mail ?>" required>
								<div class="invalid-feedback">
								Please insert the href.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-0 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="contact_info.php" class="btn btn-danger mb-3 btn-sm" type="submit">Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Contact</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>