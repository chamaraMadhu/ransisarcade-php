<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
if($_SESSION['role']<>'Admin'){
 	header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
	<div class="page-wrapper">

	    <nav aria-label="breadcrumb">
	        <ol class="breadcrumb bg-danger" style="font-size: 14px">
				<li class="breadcrumb-item text-light active" aria-current="page">Career</li>
	            <li class="breadcrumb-item text-light active" aria-current="page">Edit Career</li>
	        </ol>
	    </nav>

	    <div class="container-fluid">

			<h2>CAREER</h2><hr>

	            <div class="container-fluid bg-white ">

		            <div class="row pt-2" style="background-color: gray">
		                <h6 class="col-12 text-white">Edit Career</h6>
		            </div>

		           	<form enctype="multipart/form-data" class="needs-validation mt-3" action="career/edit_career_query.php" method="POST" novalidate>

					<div class="form-row">
					
					<?php 
					include "inc/db_conn.php";

					$id = $_GET['id'];

					$get_career = "SELECT * FROM career WHERE id = $id";
	                $run_career = mysqli_query($con,$get_career);

	                while($res_career = mysqli_fetch_array($run_career)){

	                    $job_type = $res_career['job_type'];
	                    $position = $res_career['position'];
	                    $advertisement = $res_career['advertisement'];
	                    $closing_date = $res_career['closing_date'];
	                    $status = $res_career['status'];

					?>
						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Career ID <b class="text-danger">*</b></label>
							<div class="col-sm-1">
								<input type="text" name="id" class="form-control" id="validationCustom01" value="<?php echo $id ?>" required>
								<div class="invalid-feedback">
								Please insert the category name.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
						<label class="col-sm-2 col-form-label text-right">Job Type <b class="text-danger">*</b></label>
							<div class="col-sm-2">
								<select name="type" class="form-control" id="validationCustom02" value="<?php echo $job_type ?>" required>
									<option value="Full Time">Full Time</option>
									<option value="Part Time">Part Time</option>
								</select>
								<div class="invalid-feedback">
								Please choose job type.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Position <b class="text-danger">*</b></label>
							<div class="col-sm-4">
								<input type="text" name="position" class="form-control" id="validationCustom02"  value="<?php echo $position ?>" required>
								<div class="invalid-feedback">
								Please choose position.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Advertisement <b class="text-danger">*</b></label>
							<div class="col-sm-5">
								<input type="file" name="advert" id="validationCustom02"  value="<?php echo $advertisement ?>">
								<img src="../img/career/<?php echo $advertisement ?>" width="40px" class="border py-1">
								<div class="invalid-feedback">
								Please choose job type.
								</div>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Closing Date <b class="text-danger">*</b></label>
							<div class="col-sm-3">
								<input type="date" name="closing_date" class="form-control" id="validationCustom02"  value="<?php echo $closing_date ?>" required>
								<div class="invalid-feedback">
								Please choose job type.
								</div>
							</div>
						</div>			

						<div class="col-md-12 mb-3 form-group row">
							<label class="col-sm-2 col-form-label text-right">Activate </label>
							<div class="col-sm-4">
								<input type="checkbox" class="mt-2" name="status" <?php if($status == "1"){ ?> checked <?php } ?> />
							</div>
						</div>

						<div class="col-md-12 mb-0 form-group row">
							<label class="col-sm-2 col-form-label text-right"></label>
							<div class="col-sm-4">
								<input type="hidden" name="id" value="<?php echo $id ?>"/>
								<a href="view_career.php" class="btn btn-danger mb-3 btn-sm" type="submit"><i class="far fa-window-close"></i> Cancel</a> &nbsp; 
								<button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Career</button>
							</div>
						</div>

						<div class="col-md-12 mb-3 form-group row">
	                      	<label class="col-sm-2 col-form-label text-right"></label>
	                      	<div class="col-sm-4">
	                        	<b class="text-danger" style="font-size: 14px">* Denotes required.</b>
	                      	</div>
	                    </div>

					<?php

						}

					?>
					
				</form>

	        </div>
	    </div>      
	</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>