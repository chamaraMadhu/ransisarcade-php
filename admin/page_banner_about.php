text-light <?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php'); 
}
if($_SESSION['view']==0){
  header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

        <!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="page-wrapper">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
					      <li class="breadcrumb-item text-light active" aria-current="page">Page Banner</li>
		            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="page_banner_about.php" style="text-decoration: none">About Us Page</a></li>
		        </ol>
		    </nav>

		    <div class="container-fluid">					
		        <h2>PAGE BANNER</h2><hr>
		        <?php
            // aleart massages for editing category
              if(isset($_GET['success_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                </div>
            <?php 
                // aleart massages for editing category 
            }  ?>

		            <div class="container-fluid bg-white ">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-white">About Us Page Banner</h6>
			            </div>

                  <?php

                  include "inc/db_conn.php";

                  $select_about_banner = mysqli_query($con,"SELECT about FROM page_banner");
                  while($res_about_banner = mysqli_fetch_array($select_about_banner)){

                    $about_banner = $res_about_banner['about'];
                  }
                  ?>

			           	<img class="mt-4 mb-3" src="../img/page_banner/<?php echo $about_banner ?>" width="100%">
                  <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1'){ ?>
                  <a href="edit_page_banner_about.php" class="btn btn-success btn-sm mb-3" type="submit" name="submit">Edit Home Banner</a>
                  <?php } } } ?>
		            </div>
		    </div>      
		</div>
   
 </div>
 <!-- content -->
 
<?php 
  include "inc/footer.php";
?>