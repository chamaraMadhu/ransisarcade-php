<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php');
}
if($_SESSION['role']=='Owner'){
    header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
        <div class="col-10 bg-danger p-0">
             <div class="page-wrapper">

          <nav aria-label="breadcrumb">
              <ol class="breadcrumb bg-danger" style="font-size: 14px">
                  <li class="breadcrumb-item text-light active" aria-current="page">Product</li>
                  <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="add_product.php" style="text-decoration: none">Add Product</a></li>
              </ol>
          </nav>

          <div class="container-fluid">

          <h2>PRODUCT</h2><hr>
                <?php
                  if(isset($_GET['success_msg'])){
                ?>
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong><?php echo $_GET['success_msg']; ?> </strong> 
                            </div>

                <?php }elseif(isset($_GET['fail_msg'])){ ?>

                    <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong><?php echo $_GET['fail_msg']; ?></strong> 
                            </div>
                <?php } ?>

                  <div class="container-fluid bg-white mb-4">

                    <div class="row pt-2" style="background-color: gray">
                        <h6 class="col-12 text-white">ADD PRODUCT</h6>
                    </div>

                    <form action="product/add_product_query.php" method="POST" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
                      <div class="form-row">

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Product ID <b class="text-danger">*</b></label>
                          <div class="col-sm-2">
                            <input type="text" name="pro_id" class="form-control" id="validationCustom01" required>
                            <div class="invalid-feedback">
                            Please insert the product ID.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Product name <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="text" name="name" class="form-control" id="validationCustom01" required>
                            <div class="invalid-feedback">
                            Please insert the product name.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Style <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="style" class="form-control" id="validationCustom02" required>
                              <option></option>
                              <?php

                                include "inc/db_conn.php";

                                $get_style = "SELECT * FROM style";
                                $run_style = mysqli_query($con,$get_style);

                                while($res_style = mysqli_fetch_array($run_style)){

                                  $style = $res_style['style'];

                                  if ($res_style['status'] == '1') {

                                ?>
                                  <option><?php echo $style ?></option> 

                                <?php
                                   } }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the style.
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Product Image - Front <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="file" name="img_front" class="form-control" id="validationCustom02" required>
                            <div class="invalid-feedback">
                            Please choose front image.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Product Image - Jacket <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="file" name="img_jacket" class="form-control" id="validationCustom02" required>
                            <div class="invalid-feedback">
                            Please choose jacket image.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Product Image - Border <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="file" name="img_border" class="form-control" id="validationCustom02" required>
                            <div class="invalid-feedback">
                            Please choose border image.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Product Image - Back <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="file" name="img_back" class="form-control" id="validationCustom02" required>
                            <div class="invalid-feedback">
                            Please choose back image.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Fabric <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="fabric" class="form-control" id="validationCustom02" required>
                              <option></option>
                              <?php

                                $get_fabric = "SELECT * FROM fabric";
                                $run_fabric = mysqli_query($con,$get_fabric);

                                while($res_fabric = mysqli_fetch_array($run_fabric)){

                                  $fabric = $res_fabric['fabric'];

                                  if ($res_fabric['status'] == '1') {

                                ?>
                                  <option><?php echo $fabric ?></option> 

                                <?php
                                   } }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the fabric.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Color <b class="text-danger">*</b></label>
                          <div class="col-sm-2">
                            <input type="text" name="color" class="form-control" id="validationCustom01" required>
                            <div class="invalid-feedback">
                            Please insert the style.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Old Price (LKR)</label>
                          <div class="col-sm-2">
                            <input type="text" name="old_price" class="form-control" id="validationCustom01">
                            <div class="invalid-feedback">
                            Please insert the old price.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Discounted Price (LKR) <b class="text-danger">*</b></label>
                          <div class="col-sm-2">
                            <input type="text" name="price" class="form-control" id="validationCustom01" required>
                            <div class="invalid-feedback">
                            Please insert the price.
                            </div>
                          </div>
                        </div>

                        <!-- <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Occasion <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="occasion" class="form-control" id="validationCustom02" required>
                              <option></option>
                              <?php

                                $get_occasion = "SELECT * FROM occasion";
                                $run_occasion = mysqli_query($con,$get_occasion);

                                while($res_occasion = mysqli_fetch_array($run_occasion)){

                                  $occasion = $res_occasion['occasion'];

                                  if ($res_occasion['status'] == '1') {

                                ?>
                                  <option><?php echo $occasion ?></option> 

                                <?php
                                   } }
                                ?>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the occasion.
                            </div>
                          </div>
                        </div> -->

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Occasion <b class="text-danger">*</b></label>
                          <div class="col-sm-5">

                            <table class="text-muted" style="line-height: 30px; font-weight: 600; font-size: 15px">
                              <tr>
                                <td width="150px">Casual </td>
                                <td><input type="checkbox" name="casual" value="casual"></td>
                              </tr>
                              <tr>
                                <td>Party </td>
                                <td><input type="checkbox" name="party" value="party"></td>
                              </tr>
                              <tr>
                                <td>Office</td>
                                <td><input type="checkbox" name="office" value="office"></td>
                              </tr>
                              <tr>
                                <td>Cocktail</td>
                                <td><input type="checkbox" name="cocktail" value="cocktail"></td>
                              </tr>
                              <tr>
                                <td>Wedding & Engagement </td>
                                <td><input type="checkbox" name="wedding" value="wedding & engagement"></td>
                              </tr>
                            </table>
                            
                            <div class="invalid-feedback">
                            Please insert the occasion.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Wash and Care <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <select name="wash_care" class="form-control" id="validationCustom02" required>
                              <option></option>
                              <option>Hand Wash Separately In Cold Water</option>
                              <option>Dry Clean Recommended</option>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the wash and care.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Description <b class="text-danger">*</b></label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="5" name="desc" id="validationCustom03" required></textarea>
                            <div class="invalid-feedback">
                            Please insert a description.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Keywords <b class="text-danger">*</b></label>
                          <div class="col-sm-5">
                            <input type="text" name="keywords" class="form-control" id="validationCustom01" required>
                            <div class="invalid-feedback">
                            Please insert keywords.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Activate </label>
                          <div class="col-sm-4">
                            <input type="checkbox" class="mt-2" name="status"/>
                          </div>
                        </div>

                        <div class="col-md-12 mb-0 form-group row">
                          <label class="col-sm-3 col-form-label text-right"></label>
                          <div class="col-sm-4">
                            <button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Add Product</button>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-2 col-form-label text-right"></label>
                          <div class="col-sm-4">
                            <b class="text-danger" style="font-size: 14px">* Denotes required.</b>
                          </div>
                        </div>
                    </form>

                    </div>
                </div>      
              </div>    
            </div>
                
          </div>
        </div>  
   
     </div>
    <!-- content -->

<?php 
  include "inc/footer.php";
?>