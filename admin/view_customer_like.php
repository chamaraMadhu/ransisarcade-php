<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php'); 
}
if($_SESSION['view']==0){
  header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

        <!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="page-wrapper">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
		            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="view_customer_like.php" style="text-decoration: none">Customer Likes</a></li>
		        </ol>
		    </nav>

		    <div class="container-fluid">					
		        <h2>LIKES</h2><hr>
		        <?php
            // aleart massages for editing category
              if(isset($_GET['success_edit_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for editing category 


            // aleart massages for deleting category
              if(isset($_GET['success_del_msg'])){
            ?>
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
                </div>

            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">x</button>
                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
                </div>
            <?php 
                }
                // aleart massages for deleting category
             ?>

		            <div class="container-fluid bg-white ">

			            <div class="row pt-2" style="background-color: gray">
			                <h6 class="col-12 text-white text-uppercase">view customer likes</h6>
			            </div>

			           	<div class="mb-4">
                    <div class="card-body">  
                      <div class="table-responsive"> 

                          <table id="zero_config" class="table table-hover">
                              <thead>
                                  <tr style="background-color: gray; color: #fff">
                                      <td>Product ID</td>
                                      <td>IP Address</td>
                                      <td>Type of Like</td>
                                      <td>Icon</td>
                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){  ?>
                                      <td width="60px">Action</td>
                                      <?php } } } ?>
                                  </tr>
                              </thead>
                              <tbody>

                              <?php

                                include "inc/db_conn.php";

                                $get_product_like = "SELECT * FROM product_like";
                                $run_product_like = mysqli_query($con,$get_product_like);

                                while($res_product_like = mysqli_fetch_array($run_product_like)){

                                    switch ($res_product_like['like_index']) {

                                        case "1":
                                            $img = 'a.png';
                                            break;
                                        case "2":
                                            $img = 'b.png';
                                            break;
                                        case "3":
                                            $img = 'c.png';
                                            break;
                                        case "4":
                                            $img = 'd.png';
                                            break;
                                        default:
                                            $img = 'e.png';
                                    }

                                ?>

                                  <tr>
                                      <td><?php echo $res_product_like['pro_id'] ?></td>
                                      <td><?php echo $res_product_like['ip'] ?></td>
                                      <td><?php echo $res_product_like['pro_like'] ?></td>
                                      <td align="center"><img src="../img/like/<?php echo $img ?>" width="30px" class="rounded-circle py-1"></td>

                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Owner'){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
                                      <td align="center">

                                          <?php if ($_SESSION['del']=='1'){ ?>
                                          <form method="GET" action="like/delete_like_query.php">
                                            <input type="hidden" name="id" value="<?php echo $res_product_like['id'] ?>"/>
                                          	<button type="submit" name="submit" class="btn btn-danger btn-sm del_like"><i class="fa fa-trash"></i></button>
                                          </form>
                                          <?php } ?>
                                      </td>
                                      <?php } } } ?>
						                      </tr>

                                <?php
                            } 

                            ?>

                              </tfoot>
                          </table>
                      </div>

                    </div>
                  </div>

		            </div>
		    </div>      
		</div>
   
 </div>
 <!-- content -->
 
<?php 
  include "inc/footer.php";
?>