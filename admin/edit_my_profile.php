<?php
session_start();
if(!isset($_SESSION['email'])){
	 header('location:login.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			<li class="breadcrumb-item text-light active" aria-current="page"><a href="my_profile.php">My Profile</a></li>
			<li class="breadcrumb-item text-light active" aria-current="page">Edit Profile</li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>EDIT PROFILE</h2><hr>
            <div class="container-fluid bg-white ">

			<?php

			include "inc/db_conn.php";

				$id = $_SESSION['id'];

				$sel_user = "SELECT * FROM user WHERE id ='$id'";
				$run_user = mysqli_query($con,$sel_user);

				while($res_user = mysqli_fetch_array($run_user)){

					$uname = $res_user ['user_name'];
					$role = $res_user ['role'];
					$img = $res_user ['image'];
					$mail = $res_user ['email'];
					$pwd = base64_decode($res_user ['password']);
				}

			?>
				<div class="row">
					<form  method="POST" action="profile/edit_profile_query.php" enctype="multipart/form-data">
						<table class="table text-left col-4">
						  <tbody>
						    <tr>
						      <td colspan="2"><img src="../img/user/<?php echo $img ?>" width="150px" /><br><br><input type="file" name="img"></td>
						    </tr>						   
						    <tr>
						      <td><b>User Name</b></td>
						      <td><input class="pl-2" type="text" name="uname" value="<?php echo $uname ?>" size="40"></td>
						    </tr>
						    <tr>
						      <td><b>Role</b></td>
						      <td><?php echo $role ?></td>
						    </tr>
						    <tr>
						      <td><b>Privileges</b></td>
						      <td>
								<?php if(isset($_SESSION['email'])){ if ($_SESSION['role']=='Owner'){ ?>

							      	View - <input type="checkbox" class="mt-2" name="view" <?php if($_SESSION['view'] == "1"){ ?> checked <?php } ?> /> &nbsp; &nbsp;
									Add - <input type="checkbox" class="mt-2" name="add" <?php if($_SESSION['add'] == "1"){ ?> checked <?php } ?> /> &nbsp; &nbsp;
									Edit - <input type="checkbox" class="mt-2" name="edit" <?php if($_SESSION['edit'] == "1"){ ?> checked <?php } ?> /> &nbsp; &nbsp;
									Del - <input type="checkbox" class="mt-2" name="del" <?php if($_SESSION['del'] == "1"){ ?> checked <?php } ?> > &nbsp; &nbsp;

									<?php }else{ ?>

						      	<?php if($_SESSION['add']=='1') echo 'add' ?> <?php if($_SESSION['view']=='1') echo 'view' ?> <?php if($_SESSION['edit']=='1') echo 'edit' ?> <?php if($_SESSION['del']=='1') echo 'delete' ?> <?php if($_SESSION['role']=='Owner') echo '(only for user functions)' ?><?php if($_SESSION['role']<>'Owner') echo '(only for product functions)' ?>
						      <?php } } ?>
						      </td>
						    </tr>
						    <tr>
						      <td><b>Mail</b></td>
						      <td><input class="pl-2 col-10" type="email" name="mail" value="<?php echo $mail ?>" size="40"></td>
						    </tr>
						    <tr>
						      <td><b>Password</b></td>
						      <td><input class="pl-2 col-6" type="password" name="pwd" value="<?php echo $pwd ?>" size="40"></td>
						    </tr>
						    <tr>
						      <td></td>

						      <td><a href="my_profile.php" class="btn btn-danger mb-3 btn-sm" type="submit">Cancel</a> &nbsp; <button class="btn btn-success mb-3 btn-sm" type="submit" name="submit">Edit Profile</button></td>
						    </tr>
						  </tbody>
						</table>
					</form>
				</div>
				
        </div>
    </div>      
</div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>