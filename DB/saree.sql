-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2019 at 10:39 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saree`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisement`
--

CREATE TABLE `advertisement` (
  `id` int(2) NOT NULL,
  `advertisement` text NOT NULL,
  `descreption` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisement`
--

INSERT INTO `advertisement` (`id`, `advertisement`, `descreption`, `status`) VALUES
(1, 'pic3.jpg', '30% discount', 1),
(2, 'pic4.jpg', '30% discount', 1),
(3, 'pic5.jpg', '30% discount', 1);

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(2) NOT NULL,
  `job_type` varchar(20) NOT NULL,
  `position` varchar(100) NOT NULL,
  `advertisement` text NOT NULL,
  `closing_date` date NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `color_range`
--

CREATE TABLE `color_range` (
  `id` int(2) NOT NULL,
  `name` varchar(40) NOT NULL,
  `code` varchar(40) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `color_range`
--

INSERT INTO `color_range` (`id`, `name`, `code`, `status`) VALUES
(1, 'Black', '#000', 1),
(2, 'White', '#fff', 1),
(3, 'Red', 'red', 1),
(4, 'Orange', 'Orange', 1),
(5, 'Green', 'Green', 1),
(6, 'Blue', 'Blue', 1),
(7, 'Yellow', 'Yellow', 1),
(8, 'Gold', 'Gold', 1),
(9, 'Silver', 'Silver', 1),
(10, 'Pink', 'Pink', 1),
(11, 'Purple', 'Purple', 1),
(12, 'Brown', 'Brown', 1),
(13, 'Gray', 'Gray', 1),
(14, 'Multi', 'Multi', 1),
(15, 'Magenta ', 'Magenta ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `pro_id` varchar(10) NOT NULL,
  `ip` varchar(200) NOT NULL,
  `name` varchar(50) NOT NULL,
  `date_time` varchar(50) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `icon_comment` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `pro_id`, `ip`, `name`, `date_time`, `comment`, `icon_comment`) VALUES
(13, 'P0009', '::1', 'Chamara Madhushanka', '18-Jan-2019 04:44:16am', 'Very pretty saree.', 'love'),
(14, 'P0009', '::1', 'Madhu Liyanage', '18-Jan-2019 05:22:12am', 'Cotton, Thread Border, 5.5m x 1.16m Sari Without Blouse Piece, Blouse shown in the image is only for shoot purpose, Hand Wash Separately in Cold Water Recommended', 'like'),
(16, 'P0004', '::1', 'Rashen Perera', '18-Jan-2019 10:10:37pm', 'Very nice saree..', 'wow');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(2) NOT NULL,
  `house_no` varchar(20) NOT NULL,
  `street` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `general_line` varchar(20) NOT NULL,
  `mobile1` varchar(20) NOT NULL,
  `mobile2` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `house_no`, `street`, `city`, `general_line`, `mobile1`, `mobile2`, `mail`) VALUES
(1, 'No. 72/2,', 'Buthgamuwa Road,', 'Rajagiriya.', '(+011) 2 793 889', '(+94) 727 352 576', '(+94) 772 725 205', 'ransir@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `dimension`
--

CREATE TABLE `dimension` (
  `id` int(2) NOT NULL,
  `dimension` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dimension`
--

INSERT INTO `dimension` (`id`, `dimension`, `status`) VALUES
(1, '6.4m x 1.16m', 1),
(2, '5.5m x 1.1m', 1),
(3, '5.5m x 1.16m', 1),
(4, '6.4m x 1.1m', 1),
(6, '6.35m x 1.16m', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fabric`
--

CREATE TABLE `fabric` (
  `id` int(2) NOT NULL,
  `fabric` varchar(50) NOT NULL,
  `image` text NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fabric`
--

INSERT INTO `fabric` (`id`, `fabric`, `image`, `status`) VALUES
(1, 'Cotton', '1.1.jpg', 1),
(2, 'Silk', '2.1.jpg', 1),
(4, 'Linen', '3.1.jpg', 1),
(5, 'Chiffon', '4.1.jpg', 0),
(6, 'Cotton Silk', '6.1.webp', 1),
(7, 'Pure Silk', '18.1.webp', 1);

-- --------------------------------------------------------

--
-- Table structure for table `occasion`
--

CREATE TABLE `occasion` (
  `id` int(2) NOT NULL,
  `occasion` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `occasion`
--

INSERT INTO `occasion` (`id`, `occasion`, `status`) VALUES
(2, 'Party', 1),
(3, 'Daily', 1),
(4, 'Office', 1),
(6, 'Wedding & Engagement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_banner`
--

CREATE TABLE `page_banner` (
  `id` int(2) NOT NULL,
  `home` text NOT NULL,
  `about` text NOT NULL,
  `shop1` text NOT NULL,
  `shop2` text NOT NULL,
  `shop3` text NOT NULL,
  `faq` text NOT NULL,
  `career` text NOT NULL,
  `contact` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_banner`
--

INSERT INTO `page_banner` (`id`, `home`, `about`, `shop1`, `shop2`, `shop3`, `faq`, `career`, `contact`) VALUES
(1, 'home.jpg', 'about.jpg', 'shop1.jpg', 'shop2.jpg', 'shop3.jpg', 'faq.jpg', 'career.jpg', 'contact.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `price_range`
--

CREATE TABLE `price_range` (
  `id` int(2) NOT NULL,
  `min` int(6) NOT NULL,
  `max` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_range`
--

INSERT INTO `price_range` (`id`, `min`, `max`) VALUES
(1, 0, 2500),
(2, 2500, 5000),
(3, 5000, 7500),
(4, 7500, 10000),
(5, 10000, 15000),
(6, 15000, 20000),
(7, 20000, 25000),
(8, 25000, 35000),
(9, 35000, 50000),
(10, 50000, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `pro_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `style` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fabric` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `image_front` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_jacket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_border` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_back` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `casual_wear` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `party_wear` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_wear` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cocktail_wear` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wedding_wear` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_price` double(8,2) NOT NULL,
  `price` double(8,2) NOT NULL,
  `wash_care` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `pro_id`, `name`, `style`, `fabric`, `status`, `image_front`, `image_jacket`, `image_border`, `image_back`, `casual_wear`, `party_wear`, `office_wear`, `cocktail_wear`, `wedding_wear`, `color`, `old_price`, `price`, `wash_care`, `description`, `keywords`) VALUES
(2, 'P0005', 'Cotton Silk Maheshwari Woven Saree', 'Kanchipuram Silk', 'Chiffon', 1, '2.1.jpg', '2.2.jpg', '2.3.jpg', '2.4.jpg', 'casual', '', 'office', '', '', 'Blue', 20999.00, 19999.99, 'Dry Clean Recommended', 'Cotton, Thread Border, 5.5m x 1.16m Sari Without Blouse Piece, Blouse shown in the image is only for shoot purpose, Hand Wash Separately in Cold Water Recommended', 'Cotton Silk Maheshwari Woven saree blue 19999'),
(3, 'P0004', 'Cotton Silk Maheshwari Printed Saree', 'Jamdani', 'Cotton', 1, '3.1.jpg', '3.2.jpg', '3.3.jpg', '3.4.jpg', '', 'party', '', 'cocktail', '', 'lightred', 15499.00, 14490.00, 'Dry Clean Recommended', 'Cotton, Thread Border, 5.5m x 1.16m Sari Without Blouse Piece, Blouse shown in the image is only for shoot purpose, Hand Wash Separately in Cold Water Recommended', 'Cotton Silk Maheshwari Printed Saree red 14490'),
(7, 'P0003', ' Pink Linen Woven Jamdani Circles Sari', 'Bangalore Silk', 'Silk', 1, '4.1.jpg', '4.2.jpg', '4.3.jpg', '4.4.jpg', '', 'party', '', 'cocktail', '', 'Pink', 0.00, 22890.00, 'Dry Clean Recommended', 'Cotton, Thread Border, 5.5m x 1.16m Sari Without Blouse Piece, Blouse shown in the image is only for shoot purpose, Hand Wash Separately in Cold Water Recommended', ' Pink Linen Woven Jamdani Circles Saree 22890'),
(8, 'P0002', 'Beige Viscose Modal Hand Block Printed Nargis ', 'Bangalore Silk', 'Linen', 1, '5.1.webp', '5.2.jpg', '5.3.webp', '5.4.webp', 'casual', 'party', 'office', '', '', 'White', 0.00, 15490.00, 'Dry Clean Recommended', 'Cotton, Thread Border, 5.5m x 1.16m Sari Without Blouse Piece, Blouse shown in the image is only for shoot purpose, Hand Wash Separately in Cold Water Recommended', 'Beige Viscose Modal Hand Block Printed Nargis Saree white 15490'),
(9, 'P0006', 'Cotton Bagru Printed Sari', 'Chanderi', 'Cotton', 1, '6.1.webp', '6.2.jpg', '6.3.webp', '6.4.webp', '', 'party', '', 'cocktail', 'wedding & engagement', 'White', 5999.00, 4999.00, 'Hand Wash Separately In Cold Water', 'Hand Wash Separately In Cold Water With Mild/Liquid Detergents Recommended. Do Not Bleach. Do Not Wring. Do Not Iron On Embellishments. Iron On Reverse. Dry In Shade.', 'Cotton Bagru Printed Sari'),
(10, 'P0007', 'Cotton Silk Maheshwari Woven Sari', 'Banarasi Silk', 'Cotton', 1, '7.1.webp', '7.2.webp', '7.3.webp', '7.4.webp', 'Daily', '', '', '', '', 'Blue', 0.00, 15599.00, 'Dry Clean Recommended', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Cotton Silk Maheshwari Woven Sari Blue'),
(11, 'P0008', 'Silk Tussar Contrast Palla Woven Sari', 'Jamdani', 'Silk', 1, '8.1.webp', '8.2.jpg', '8.3.webp', '8.4.webp', 'Office', '', '', '', '', 'Brown', 0.00, 11999.00, 'Dry Clean Recommended', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Silk Tussar Contrast Palla Woven Sari Brown'),
(12, 'P0009', 'Silk Georgette Banarasi Cutwork Sari', 'Kanchipuram Silk', 'Cotton Silk', 1, '9.1.webp', '9.2.jpg', '9.3.webp', '9.4.webp', 'Party', '', '', '', '', 'Green', 26499.00, 25499.00, 'Dry Clean Recommended', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Silk Georgette Banarasi Cutwork Sari'),
(13, 'P0010', ' Cotton Woven Loom Sari', 'Banarasi Silk', 'Cotton', 1, '10.1.webp', '10.2.jpg', '10.3.webp', '10.4.webp', 'Office', '', '', '', '', 'Yellow', 0.00, 9999.00, 'Hand Wash Separately In Cold Water', 'Hand Wash Separately In Cold Water With Mild/Liquid Detergents Recommended. Do Not Bleach. Do Not Wring. Do Not Iron On Embellishments. Iron On Reverse. Dry In', ' Cotton Woven Loom Sari'),
(14, 'P0011', 'Silk Tussar Printed Sari', 'Bangalore Silk', 'Linen', 1, '11.1.webp', '11.2.webp', '11.3.webp', '11.4.webp', 'Traditional', '', '', '', '', 'Red', 0.00, 17599.00, 'Dry Clean Recommended', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Silk Tussar Printed Sari red'),
(15, 'P0012', 'Silk Georgette Banarasi Cutwork Sari', 'Jamdani', 'Silk', 1, '12.1.webp', '12.2.webp', '12.3.webp', '12.4.webp', 'Traditional', '', '', '', '', 'Pink', 0.00, 19999.00, 'Dry Clean Recommended', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Silk Georgette Banarasi Cutwork Sari pink'),
(16, 'P0013', 'Silk Woven Jamdani Barfi Barfi Sari', 'Kanchipuram Silk', 'Cotton Silk', 1, '13.1.webp', '13.2.webp', '13.3.webp', '13.4.webp', 'Daily', '', '', '', '', 'Black', 0.00, 14899.00, 'Dry Clean Recommended', 'Dry Clean recommended for this product. Iron inside out. Do not bleach and expose this product to excessive heat and sunlight for long.', 'Silk Woven Jamdani Barfi Barfi Sari black'),
(26, 'P0001', 'Hand Block Printed Nargi', 'Bangalore Silk', 'Chiffon', 1, '1.1.jpg', '1.2.jpg', '1.3.jpg', '1.4.jpg', 'casual', 'party', 'office', 'cocktail', 'wedding & engagement', 'Blue', 12999.00, 11999.00, 'Hand Wash Separately In Cold Water', 'Cotton, Thread Border, 5.5m x 1.16m Sari Without Blouse Piece, Blouse shown in the image is only for shoot purpose, Hand Wash Separately in Cold Water Recommended', 'Hand Block Printed Nargis blue'),
(27, 'P0014', 'Silk Mulberry Ajrakh Print Sari', 'Chanderi', 'Silk', 1, '14.1.webp', '14.2.jpg', '14.3.webp', '14.4.webp', 'Office', '', '', '', '', 'Purple', 2499.00, 1999.00, 'Hand Wash Separately In Cold Water', 'Dry Clean Only. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Silk Mulberry Ajrakh Print Sari'),
(28, 'P0015', 'Silk Tussar Dabu Printed Sari', 'Kanchipuram Silk', 'Cotton Silk', 1, '15.1.webp', '15.2.webp', '15.3.webp', '15.4.webp', 'Office', '', '', '', '', 'Gray', 7599.00, 6599.00, 'Hand Wash Separately In Cold Water', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Silk Tussar Dabu Printed Sari'),
(29, 'P0016', ' Cotton Silk Printed Khari Sari', 'Chanderi', 'Chiffon', 1, '16.1.webp', '16.2.jpg', '16.3.webp', '16.4.webp', 'Traditional', '', '', '', '', 'Gold', 9999.00, 7590.00, 'Dry Clean Recommended', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', ' Cotton Silk Printed Khari Sari'),
(30, 'P0017', 'Cottton Silk Cutowrk Banarasi Sari', 'Banarasi Silk', 'Linen', 1, '17.1.webp', '17.2.jpg', '17.3.webp', '17.4.webp', 'Office', '', '', '', '', 'Orange', 6799.00, 5990.00, 'Hand Wash Separately In Cold Water', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Cottton Silk Cutowrk Banarasi Sari'),
(31, 'P0018', 'Cotton Silk Chanderi Ashrafi Sari', 'Banarasi Silk', 'Cotton Silk', 1, '18.1.webp', '18.2.webp', '18.3.webp', '18.4.webp', 'Daily', '', '', '', '', 'Red', 1599.00, 1490.00, 'Hand Wash Separately In Cold Water', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach And Expose This Product To Excessive Heat And Sunlight For Long.', 'Cotton Silk Chanderi Ashrafi Sari'),
(32, 'P0019', 'Cotton Silk Chanderi Diagonal Zari Sari', 'Chanderi', 'Cotton', 1, '19.1.webp', '19.2.webp', '19.3.webp', '19.4.webp', 'Traditional', '', '', '', '', 'Blue', 8990.00, 7990.00, 'Hand Wash Separately In Cold Water', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach. Dry in Shade!', 'Cotton Silk Chanderi Diagonal Zari Sari'),
(33, 'P0020', 'Silver Cotton Silk Chanderi Kirtika Palla Sari', 'Jamdani', 'Linen', 1, '20.1.webp', '20.2.jpg', '20.3.webp', '20.4.webp', 'Office', '', '', '', '', 'Silver', 7500.00, 6500.00, 'Hand Wash Separately In Cold Water', 'Dry Clean Recommended For This Product. Iron On Reverse. Do Not Bleach. Dry in Shade!', 'Silver Cotton Silk Chanderi Kirtika Palla Sari'),
(35, 'P0021', 'Cotton Bagru Print Sari', 'Banarasi Silk', 'Cotton', 1, '21.1.webp', '21.2.webp', '21.3.webp', '21.4.webp', 'casual', '', 'office', '', '', 'Red', 15999.00, 14999.00, 'Hand Wash Separately In Cold Water', 'Hand Wash Separately In Cold Water With Mild/Liquid Detergents Recommended. Do Not Bleach. Do Not Wring. Do Not Iron On Embellishments. Iron On Reverse. Dry In Shade.', 'Cotton Bagru Print Sari');

-- --------------------------------------------------------

--
-- Table structure for table `product_like`
--

CREATE TABLE `product_like` (
  `id` int(10) NOT NULL,
  `pro_id` varchar(10) NOT NULL,
  `ip` varchar(250) NOT NULL,
  `pro_like` varchar(20) NOT NULL,
  `like_index` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_like`
--

INSERT INTO `product_like` (`id`, `pro_id`, `ip`, `pro_like`, `like_index`) VALUES
(19, 'P0006', '::1', 'like', 1),
(20, 'P0006', '::1', 'love', 2),
(21, 'P0006', '::1', 'wow', 3),
(22, 'P0011', '::1', 'wow', 3),
(24, 'P0012', '::1', 'like', 1),
(25, 'P0013', '::1', 'love', 2),
(26, 'P0009', '::1', 'like', 1),
(28, 'P0013', '::1', 'like', 1),
(29, 'P0013', '::1', 'wow', 3),
(30, 'P0017', '::1', 'like', 1),
(31, 'P0017', '::1', 'love', 2),
(32, 'P0007', '::1', 'wow', 3),
(33, 'P0018', '::1', 'love', 2),
(34, 'P0018', '::1', 'wow', 3),
(36, 'P0010', '::1', 'like', 1),
(37, 'P0010', '::1', 'dislike', 5),
(38, 'P0005', '::1', 'like', 1),
(40, 'P0015', '::1', 'wow', 3),
(41, 'P0021', '::1', 'like', 1),
(42, 'P0021', '::1', 'love', 2),
(43, 'P0021', '::1', 'wow', 3),
(44, 'P0009', '::1', 'like', 1),
(45, 'P0009', '::1', 'love', 2),
(46, 'P0009', '::1', 'wow', 3),
(48, 'P0003', '::1', 'like', 1),
(49, 'P0003', '::1', 'like', 1),
(50, 'P0003', '::1', 'wow', 3),
(51, 'P0001', '::1', 'like', 1),
(52, 'P0001', '::1', 'love', 2),
(53, 'P0002', '::1', 'like', 1),
(54, 'P0002', '::1', 'love', 2),
(55, 'P0002', '::1', 'wow', 3),
(56, 'P0008', '::1', 'like', 1),
(59, 'P0020', '::1', 'like', 1),
(61, 'P0020', '::1', 'wow', 3),
(62, 'P0020', '::1', 'dislike', 5),
(63, 'P0019', '::1', 'like', 1),
(64, 'P0019', '::1', 'love', 2),
(65, 'P0019', '::1', 'wow', 3),
(66, 'P0014', '::1', 'like', 1),
(67, 'P0014', '::1', 'love', 2),
(68, 'P0016', '::1', 'like', 1),
(69, 'P0016', '::1', 'dislike', 5),
(70, 'P0004', '::1', 'love', 2),
(71, 'P0002', '::1', 'love', 2),
(72, 'P0003', '::1', 'love', 2),
(73, 'P0019', '::1', 'love', 2),
(74, 'P0002', '::1', 'love', 2),
(75, 'P0004', '::1', 'like', 1),
(76, 'P0014', '::1', 'love', 2),
(77, 'P0004', '::1', 'love', 2),
(78, 'P0021', '::1', 'like', 1),
(79, 'P0005', '::1', 'like', 1),
(80, 'P0005', '::1', 'love', 2),
(81, 'P0019', '::1', 'wow', 3);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` varchar(2) NOT NULL,
  `title` varchar(20) NOT NULL,
  `url` varchar(200) NOT NULL,
  `font_awesome_code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `title`, `url`, `font_awesome_code`) VALUES
('1', 'facebook', 'https://www.facebook.com/', '<i class=\"fab fa-facebook-square fa-3x mx-2\" aria-hidden=\"true\"></i>'),
('2', 'twitter', '', '<i class=\"fab fa-twitter-square  fa-3x mx-2\" aria-hidden=\"true\"></i>'),
('3', 'youtube', '', '<i class=\"fab fa-youtube-square fa-3x mx-2\" aria-hidden=\"true\"></i>'),
('4', 'Instagram', '', '<i class=\"fab fa-instagram fa-3x mx-2\"></i>'),
('5', 'google +', '', '<i class=\"fab fa-google-plus-square fa-3x mx-2\"></i>');

-- --------------------------------------------------------

--
-- Table structure for table `style`
--

CREATE TABLE `style` (
  `id` int(2) UNSIGNED NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `style` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `style`
--

INSERT INTO `style` (`id`, `status`, `style`, `image`) VALUES
(6, 1, 'Bangalore Silk', '5.1.webp'),
(7, 1, 'Banarasi Silk', '3.1.jpg'),
(8, 1, 'Chanderi', '1.1.jpg'),
(10, 1, 'Kanchipuram Silk', '2.1.jpg'),
(11, 1, 'Jamdani', '4.1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `addition` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletion` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `role`, `email`, `password`, `image`, `addition`, `view`, `edit`, `deletion`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Chamara Madhushanka Liyanage', 'Admin', 'nlc.madhushanka@gmail.com', 'Y2NjYw==', 'FB_IMG_15048716616161155.jpg', '1', '1', '1', '1', 'Q8E1XGoXs5CXeOxGntwYvkfGjv0Qtm7LOkw1y4Ep0bkTgUiKuA1MPezeLn6I', '2018-08-16 22:34:11', '2018-08-16 22:34:11'),
(5, 'Ransi Reeza', 'Owner', 'ransir@gmail.com', 'cnJycg==', '1.png', '1', '1', '1', '1', NULL, NULL, NULL),
(6, 'Kumari Kulathunga', 'Manager', 'kumari@gmail.com', 'a2traw==', '7.jpg', '0', '1', '1', '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`id`, `ip`) VALUES
(1552, '127.0.0.1'),
(1551, '::1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisement`
--
ALTER TABLE `advertisement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color_range`
--
ALTER TABLE `color_range`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dimension`
--
ALTER TABLE `dimension`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dimension` (`dimension`);

--
-- Indexes for table `fabric`
--
ALTER TABLE `fabric`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fabric` (`fabric`);

--
-- Indexes for table `occasion`
--
ALTER TABLE `occasion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `occasion` (`occasion`);

--
-- Indexes for table `page_banner`
--
ALTER TABLE `page_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_range`
--
ALTER TABLE `price_range`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pro_id` (`pro_id`);

--
-- Indexes for table `product_like`
--
ALTER TABLE `product_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `style`
--
ALTER TABLE `style`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`style`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ip` (`ip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisement`
--
ALTER TABLE `advertisement`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `dimension`
--
ALTER TABLE `dimension`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fabric`
--
ALTER TABLE `fabric`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `occasion`
--
ALTER TABLE `occasion`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `product_like`
--
ALTER TABLE `product_like`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `style`
--
ALTER TABLE `style`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1553;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
