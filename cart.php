<?php 
	include "include/head.php";
	include "include/navbar.php";
?>
	
	<!-- starts breadcrumb -->
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-light mb-5">
      <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
      <li class="breadcrumb-item"><a href="shop.php" style="color: #422C2F; font-weight: 500; text-decoration: none">shop</a></li>
      <li class="breadcrumb-item active" aria-current="page"><!-- <?php $name = $_GET['name']; echo $name ?> --></li>
      <li class="breadcrumb-item active" aria-current="page">My cart</li>
    </ol>
  </nav>
  <!-- ends breadcrumb -->

    <div class="container mb-5"><!-- starts container-->
      <h4 class="text-muted text-center mb-5">Shopping Cart</h4>
      <div class="row">
        <div class="col-sm-8 col-md-12 col-lg-8">
          <table class="text-center mt-4" width="100%">
            <tr class="text-muted border-bottom">
              <th>Product</th>
              <th>Name</th>
              <th>Unit Price</th>
              <th>Qty</th>
              <th>Total</th>
              <th>Del</th>
            </tr> 

            <tr class="text-muted border-bottom">
              <td><img class="border mr-2 mt-2 mb-2" src="img/pro10.jpg" width="80px" height="110px"></td>
              <td>Dark Handloom Saree</td>
              <td>Rs. 11,000</td>
              <td>1</td>
              <td>Rs. 11,000</td>
              <td><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
            </tr>

            <tr class="text-muted border-bottom">
              <td><img class="border mr-2 mt-2 mb-2" src="img/pro10.jpg" width="80px" height="110px"></td>
              <td>Dark Handloom Saree</td>
              <td>Rs. 11,000</td>
              <td>1</td>
              <td>Rs. 11,000</td>
              <td><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
            </tr>
          </table>

          <a href="shop.php"><button type="button" class="form-control mt-3" value="" style="background-color: gray; color: #fff; width: 200px"><i class="fa fa-arrow-left" aria-hidden="true"></i> Continue Shopping</button></a>

        </div>

        <div class="col-sm-4 col-md-12 col-lg-4 pt-2 pl-5 pr-5 bg-light">
          <table width="100%">
            <tr class="text-muted border-bottom" height="70px">
              <td colspan="2"><h5>Cart Totals</h5></td>
            </tr>

            <tr class="text-muted" height="60px">
              <td>Subtotal</td>
              <td class="text-right">Rs. 22,000</td>
            </tr>

            <tr class="text-muted border-bottom" height="60px">
              <td>Tax</td>
              <td class="text-right">Rs. 500</td>
            </tr>

            <tr class="text-muted" height="80px">
              <th>Total</th>
              <th class="text-right" style="font-size: 24px">Rs. 22,500</th>
            </tr>

            <tr class="text-muted border-bottom" height="70px">
              <td colspan="2"><button type="button" class="form-control" style="background-color: Gray; cursor: pointer"><a href="#" class=" text-light" style="text-decoration: none"><i class="fa fa-undo" aria-hidden="true"></i> Update Cart</a></button></td>
            </tr>

            <tr class="text-muted border-bottom" height="70px">
              <td colspan="2"><button type="button" class="form-control" style="background-color: #CC2C2F; cursor: pointer"><a href="#" class=" text-light" style="text-decoration: none">Proceed to Checkout <i class="fa fa-arrow-right" aria-hidden="true"></i></a></button></td>
            </tr>

          </table>
        </div>


        

      </div>

    </div><!-- ends container-->


<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>