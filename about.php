<?php 
	include "include/head.php";
	include "include/navbar.php";
?>

  <!-- starts jumbotron -->
	<div class="jumbotron mb-0" style="background-image:url(img/page_banner/about.jpg); height: 250px;">
    <div class="container">
        <div class="pt-5">
          <h2 class="text-center" style="color: #fff">About Us</h2>
        </div>
  	</div>
  </div>
  <!-- ends jumbotron -->
	
	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb" id="history">
	  <ol class="breadcrumb bg-white">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="about.php" style="text-decoration: none">About us</a></li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

    <div class="container"><!-- starts container-->

      <div class="row">
        <div class="col px-4 text-center">
        	<h3 class="text-muted mt-0 mb-3 mt-3">Our Story</h3>
        	<p><i class="fa fa-heart" aria-hidden="true" style="color: #2ABDFC"></i></p>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col px-4 text-center">
          <p style="color: #6D6D6D" id="vision">
            Most of the cloth shop sellers do not facilitate purchase clothes online from their own sites, the only facility which they providing to customer is to view the product online and go to their location and purchase it. Now days most people are like to purchase their products online rather than going and purchasing it from a cloth shop. 
          </p>
        </div>	
      </div>

      <div class="row mb-4">
        <div class="col px-4 text-center" id="mission">
          <h3 class="text-muted mt-0 mb-3 mt-3">Vision</h3>
          <p>Most of the cloth shop sellers do not facilitate purchase clothes online from their own sites, the only facility which they providing to customer is to view the product online and go to their location and purchase it.</p>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col px-4 text-center">
          <h3 class="text-muted mt-0 mb-3 mt-3">Mission</h3>
          <p>Most of the cloth shop sellers do not facilitate purchase clothes online from their own sites, </p>
        </div>
      </div>
    </div>
    <!-- ends container-->

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>