/* body{
	font-family: Comic Sans MS;
}  */

<!-- cursive -->

#nav-bar-top a{
	font-size: 12px;
	color: gray;
}

.nav-right > a{
	font-size: 13px;
	color: gray;
	float: right;
}

.navbar > a{
	font-size: 15px;
}

.navbar a:hover{
	color: #D20000;
	text-decoration: none;
}

.search-area{
	color: #6D6D6D;
	width: 300px;
	padding-left: 5px;
	border-radius: 5px;
	border-width: thin;
	border-style: solid;
	border-color: #CCC;
	margin-top: 05px;
}




.search-btn{
	background-color: transparent;
	border: none;
	font-size: 15px;
	margin-left: -30px;
}

.search-btn:hover{
	color: #CC2C2F;
}

#clock{
	position: fixed;
	float: left;
	top: 50%;
	right: 20px;
}

.card:hover{
	opacity: .7;
}

.advert{
	overflow: hidden;
	height: 120px;
}

.advert-img:hover{
	transform:scale(1.2);
	transition:all .4s ease;
}

.social_media{
	color: #000;
	margin: auto;
}

.social_media:hover{
	color: #fff;
}

.footer{
	color: #D3D3D3;
	line-height: 30px; 
	font-size: 14px;
}

.footer a{
	color: #D3D3D3;
}

.list-group-item{
	line-height:10px;
	padding:8px;
	border-width:0px;
}

.list-group a{
	color:#9B9B9B;
}

.product{
	width:175px;
	float:left;
}

.product:hover{
	opacity: .7;
}

.accordion .card-header:after{
	font-family: 'fontAwesome';
    content: "\f106";
	float:right;
}

.accordion .collapsed:after{
    content: "\f107";
}

.carousel-control-prev-icon{
	margin-left: -275px;
	height: 30px;
}

.carousel-control-next-icon{
	margin-right: -275px;
	height: 30px;
}

.back-to-top{
	 background: none;
	 margin: 0;
	 position: fixed;
	 bottom: 0;
	 right: 0;
	 width: 60px;
	 height: 60px;
	 z-index: 100;
	 display: none;
	 text-decoration: none;
	 color: #fc7700;
	 background-color: transparent;
 }

.back-to-top i {
	 font-size: 45px;
 }

/*pagination*/

#pagin{
	font-size: 14px;
	float: right;
}

#pagin ul{
	align: center;
}


#pagin ul li{
	margin: 2px;
	align: center;
}

#pagin ul li a{
	color:white;
	padding: 2px 15px 2px 15px;
	background-color: gray;
	height:10px;
	text-decoration:none;
	font-size: 16px;
}

#pagin ul li a:hover{
	background-color: #DA4747;
}
/*pagination*/