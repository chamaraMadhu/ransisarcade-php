    <!-- starts social media -->
    <div class="container-fluid bg-danger"><!-- starts container-->
          <div id="social_media" class="row py-2 bg-danger" style="height: 60px; width: 400px; margin: auto">
            <div class="col">
            <?php

            $get_social_media = "SELECT * FROM social_media";

            $run_social_media = mysqli_query($con,$get_social_media);

            while($res_social_media = mysqli_fetch_array($run_social_media)){

              $title = $res_social_media['title'];
              $url = $res_social_media['url'];
              $font_awesome_code = $res_social_media['font_awesome_code'];

            ?>

            <a href="<?php echo $url ?>" class="social_media" title="<?php echo $title ?>" target="_blank"><?php echo $font_awesome_code ?> </a>

            <?php } ?>
          </div>
        </div>
    </div><!-- ends container -->
    <!-- ends social media -->