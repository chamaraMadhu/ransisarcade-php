<body>
  
  <nav class="navbar navbar-expand-lg navbar-light bg-dark" style="z-index: 15">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <span style="color: #ccc; font-size: 12px"><?php
                date_default_timezone_set("Asia/Colombo");
                $sqll= time();
                $s2 = date("d-M-Y",$sqll);
                echo 'Date : '.$s2 ;
                ?> &nbsp;  - &nbsp; Time :  &nbsp; </span>
    <span id="txt" style="color: #ccc; font-size: 12px"></span>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto" style="font-size: 12px">
        <li class="nav-item mr-3">
          <a class="p-2" href="my_account.php" title="My account" style="color: #ccc"><i class="fa fa-user" aria-hidden="true"></i> My account</a>
        </li>
        <li class="nav-item mr-3">
          <a class="p-2" href="#" title="Wishlist" style="color: #ccc"><i class="far fa-heart"></i> Wishlist</a>
        </li>
        <li class="nav-item mr-3">
          <a class="p-1" href="admin/login.php" title="Login" target="_blank" style="color: #ccc"><i class="fas fa-sign-in-alt"></i> Sign in</a>
        </li>
        <li class="nav-item">
          <a class="p-2" data-toggle="modal" data-target="#Modal1" title="Register" target="_blank" style="cursor: pointer; color: #ccc"><i class="fas fa-edit"></i> Register</a>
        </li>       
      </ul>
    </div>
  </nav>

  <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white pb-0" style="z-index: 12">
    <a href="index.php"><img src="img/logo.png" width="100" height="60"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav navbar2 ml-auto">

        <li class="nav-item ml-4">
          <div class="row">
            <div class="col-md-3">
              <a class="nav-link navbar2-link" href="index.php"><i class="fas fa-truck"></i></a>
            </div>
            <div class="col ml-1">
              <p class="text-muted font-weight-bold"> FREE SHIPPING </p>
              <p>all orders over $150 </p>
            </div>
          </div>
        </li>
        <li class="nav-item ml-4">
          <div class="row">
            <div class="col-md-3">
              <a class="nav-link navbar2-link" href="index.php"><i class="fas fa-sync"></i></a>
            </div>
            <div class="col ml-1">
              <p class="text-muted font-weight-bold"> RETURN & EXCHANGE </p>
              <p>up to 125 days</p>
            </div>
          </div>
        </li>
        <li class="nav-item ml-4">
          <div class="row">
            <div class="col-md-3">
               <a class="nav-link navbar2-link" href="index.php"><i class="fas fa-dollar-sign"></i></a>
            </div>
            <div class="col ml-1">
              <p class="text-muted font-weight-bold"> GET 15% OFF </p>
              <p>orders over $100</p>
            </div>
          </div>
        </li>
      </ul>

      <ul class="navbar-nav navbar2 ml-auto">

      <li class="nav-item">
          <div class="row">
            <div class="col-md-3">
              <a class="nav-link navbar2-link bg-danger" href="index.php"><i class="fas fa-shopping-bag text-light"></i></a>
            </div>
            <div class="col ml-1">
              <p> SHOPPING CART </p>
              <p><span class="text-danger">5</span> item(s) - Rs. <span class="text-danger">8155,999.00</span></p>
            </div>
          </div>
        </li>
      </ul>

    </div>
  </nav>

  <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white border-bottom" style="z-index: 12">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto container-fluid" >

        <li class="nav-item">
          <a class="nav-link" href="index.php">Home </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="about.php">About us </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="shop.php">Shop</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="faq.php">FAQ</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="careers.php">Careers</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="contact.php">Contact us</a>
        </li>

        <div class="col-md-5 mr-auto ml-auto mb-1">  
          <form action="search.php" method="GET">
            <?php

              $count_product = "SELECT * FROM product WHERE status = '1'";
              $run_count_product  = mysqli_query($con,$count_product);
              $res_count_product = mysqli_num_rows($run_count_product);

              ?>
            <input class="search-area pl-3" type="search" name="keyword" style="height: 35px;font-size:14px" placeholder="<?php echo $res_count_product ?> sarees are available...">
            <button class="search-btn" type="submit" name="submit" style="height: 35px; cursor: pointer"><i class="fa fa-search" aria-hidden="true"></i></button>
          </form>
        </div>
      </ul>



    </div>
  </nav>
  <!-- ends navbar -->

  <!-- popper for Inqiry form -->
  <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true ">
    <div class="modal-dialog col-md-3" role="document ">
        <div class="modal-content">
            <div class="modal-header bg-danger text-light">
                <h5 class="modal-title text-center" id="exampleModalLabel">Customer Registraion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true ">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <form method="POST" action="contact_form_query_product_detail.php">
                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="fname" placeholder="Name" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="email" class="form-control" name="mail" placeholder="Email" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="conact" placeholder="Country" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="city" placeholder="City" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="contact" placeholder="Contact" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="add" placeholder="Address" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="password" class="form-control" name="pwd" placeholder="Password" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="password" class="form-control" name="pwd_confirm" placeholder="Confirm Password" required>
                        </div>
                    </div>

                    <!-- <input type="hidden" name="id" value="<?php echo $res_product['id'] ?>">
                    <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>"> -->

                    <div class="row">
                        <div class="col">
                        <a href="#"><input type="submit" class="btn" value="Send" name="submit" style="background-color:#EF5B00; color: #fff; width:100%"></a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
  </div>
  <!-- popper for Inqiry form -->

  <!-- clock-->
    <!-- <div class="" id="clock"></div>   -->
  <!-- clock-->

  <!-- side contact-->
  <div id="side-contact">
    <p class="side-contact-phone"><a href="#" style="text-decoration: none"><span>www.facebook.lk</span><i class="fab fa-facebook-f"></i></a></p>
    <p class="side-contact-phone"><span>(+011) 2 793 889 </span><i class="fas fa-phone" data-toggle="tooltip" data-placement="left"></i></p>
    <p class="side-contact-phone"><span>  (+94) 727 352 576</span><i class="fas fa-mobile-alt"></i></p>
    <p class="side-contact-phone"><a href="#" style="text-decoration: none"><span>ransir@yahoo.com</span><i class="fas fa-envelope"></i></a></p>
  </div>

  

