    <!-- starts footer -->
    <div class="container-fluid" style="background-image: url(img/footer.jpg) /*background-color: #000*/;"><!-- starts container-->
      <div class="row footer" style="height: auto;">

        <div class="col-sm-6 col-md-2 py-4 text-center">
          <h5 class="mb-4" style="color: #fff">Category</h5>
          <?php 

            $get_style = "SELECT * FROM style";
              $run_style = mysqli_query($con,$get_style);

                while($res_style = mysqli_fetch_array($run_style)){

                  $style = $res_style['style'];

                  if ($res_style['status'] == '1') {   
          ?>

          <a class="text-light" href="filter_by_style.php?style=<?php echo $style ?>" style="text-decoration: none"><?php echo $style ?></a><br/>

          <?php } } ?>
        </div>

        <div class="col-sm-6 col-md-2 py-4 text-center">
          <h5 class="mb-4" style="color: #fff">Fabric</h5>
          <?php 

            $get_fabric = "SELECT * FROM fabric";
              $run_fabric = mysqli_query($con,$get_fabric);

                while($res_fabric = mysqli_fetch_array($run_fabric)){

                  $fabric = $res_fabric['fabric'];

                  if ($res_fabric['status'] == '1') {   
          ?>

          <a class="text-light" href="filter_by_fabric.php?fabric=<?php echo $fabric ?>" style="text-decoration: none"><?php echo $fabric ?></a><br/>

          <?php } } ?>
        </div>

        <div class="col-sm-6 col-md-2 py-4 text-center">
          <h5 class="mb-4" style="color: #fff">Quick links</h5>
          <a class="text-light" href="index.php" style="text-decoration: none">Home</a><br/>
          <a class="text-light" href="about.php" style="text-decoration: none">About us</a><br/>
          <a class="text-light" href="shop.php" style="text-decoration: none">Shop</a><br/>
          <a class="text-light" href="careers.php" style="text-decoration: none">Careers</a><br/>
          <a class="text-light" href="admin/login.php" style="text-decoration: none">login</a><br/>
          <a class="text-light" href="cart.php" style="text-decoration: none">Cart</a><br/>
        </div>

        <div class="col-sm-6 col-md-2 py-4 text-center">
          <h5 class="mb-4" style="color: #fff">Support</h5>
          <a class="text-light" href="faq.php" style="text-decoration: none">FAQs</a><br/>
          <a class="text-light" href="contact.php" style="text-decoration: none">Contact us</a><br/>
          <a class="text-light" href="contact.php?#map" style="text-decoration: none">Site map</a><br/>
        </div>

<!--         <div class="col-sm-6 col-md-2 py-4 text-center">
          <h5 class="mb-4" style="color: #fff">Legal</h5>
          <a href="#">Payment methods</a><br/>
          <a href="#">Delivery method</a><br/>
          <a href="#">Return & exchange</a><br/>
          <a href="#">Terms & conditions</a><br/>
          <a href="#">Privacy policy</a>
        </div> -->

        <div class="col-sm-6 col-md-4 pt-4">
          <h3 class="mb-4 text-center" style="color: #fff">Ransi's Arcade</h3>      
          <table align="center">
            <?php 

              $get_contact = "SELECT * FROM contact";

              $run_contact = mysqli_query($con,$get_contact);

              while($res_contact = mysqli_fetch_array($run_contact)){

                $house_no = $res_contact['house_no'];
                $street = $res_contact['street'];
                $city = $res_contact['city'];
                $general = $res_contact['general_line'];
                $mobi1 = $res_contact['mobile1'];
                $mobi2 = $res_contact['mobile2'];
                $mail = $res_contact['mail'];

            ?>
            <tr>
              <td><i class="fa fa-map-marker mr-4" aria-hidden="true"></i></td>
              <td><?php echo $house_no ?></td>
            </tr>

            <tr>
              <td></td>
              <td><?php echo $street ?></td>
            </tr>

            <tr>
              <td></td>
              <td><?php echo $city ?></td>
            </tr>

            <tr>
              <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
              <td><a href="mailto:ransisarcade@gmail.com"><?php echo $mail ?></a></td>
            </tr>

            <tr>
              <td><i class="fa fa-phone" aria-hidden="true"></i></td>
              <td><?php echo $general ?> (General line)</td>
            </tr>

            <tr>
              <td><i class="fa fa-mobile-alt" aria-hidden="true"></i></td>
              <td><?php echo $mobi1 ?> (Sales)</td>
            </tr>

            <tr>
              <td></td>
              <td><?php echo $mobi2 ?> </td>
            </tr>

            <?php } ?>

            <tr>
              <td></td>
              <td><p class="bg-danger text-center text-light " style="border-radius: 2px; width: auto; height: auto">000<?php $ip = $_SERVER['REMOTE_ADDR'];

                mysqli_query($con,"INSERT INTO visitor(ip) VALUES('$ip')");

                $sql = "SELECT COUNT(DISTINCT ip) FROM visitor";
                $run = mysqli_query($con,$sql);

                echo $res = mysqli_num_rows($run);
                ?> : Unique Visitors</p> 
              </td>
            </tr>

            <tr>
              <td></td>
              <td align="center">
                <!--  date time-->
                <!-- <div style="padding:1em 0"><iframe src="https://www.zeitverschiebung.net/clock-widget-iframe-v2?language=en&size=medium&timezone=Asia%2FColombo" width="100%" height="115" seamless style="border-width: 1px; border-style: solid; color: #474747"> </iframe> </div> -->

                <!-- <?php
                // date_default_timezone_set("Asia/Colombo");
                // $sqll= time();
                // $s2 = date("d-M-Y h:i:s",$sqll);
                // echo $s2;
                // echo "The time is " . date("h:i:s");
                ?>  --> 
                <!-- date time-->
                <!-- <canvas id="canvas" width="100" height="100"></canvas> -->
              </td>
            </tr>

            
          </table>
        </div>

      </div>

      <div class="row footer mb-0" style="height: auto">
        <div class="col">
          <div class="dropdown-divider mb-2" style="border-color: #474747"></div>
          <!-- <img src="img/paypal.png" class="rounded mx-auto d-block" style="height: 50px; width:250px;"> -->
          <p class="text-center mb-2" style="font-size: 12px">All Rights Reserved by Ransi's Arcade. Designed and Developed by Chamara Madhushanka.</p>

          <a href="#" class="back-to-top" style="display: inline;"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
        </div>

      </div>
    </div><!-- ends container -->
    <!-- ends footer -->



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Image zoom -->
    <script src="js/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="js/jqzoom.js"></script>
    <script>
      $("#bzoom").zoom({
        zoom_area_width: 300,
        autoplay_interval :3000,
        small_thumbs : 4,
        autoplay : true
      });
    </script>
    <!-- Image zoom -->

    <!-- owl carousel -->
    <script type="text/javascript" src="js/owl.carousel.js"></script>
    
    <script>
      $('.owl-carousel').owlCarousel({
          loop:true,
          responsiveClass:true,
          mouseDrag:true,
          touchDrag:true,
          autoplay:true,
          dotsEach:true,
          responsive:{
              0:{
                  items:2,
                  margin:10,
              },
              600:{
                  items:4,
                  margin:10,
              },
              1000:{
                  items:6,
                  margin:25,
              }
          }
      })
    </script>
    <!-- owl carousel -->   

    <!-- go back to top --> 
    <script>
     jQuery(document).ready(function() {
     var offset = 250;
     var duration = 300;
     jQuery(window).scroll(function() {
     if (jQuery(this).scrollTop() > offset) {
     jQuery('.back-to-top').fadeIn(duration);
     } else {
     jQuery('.back-to-top').fadeOut(duration);
     }
     });

     jQuery('.back-to-top').click(function(event) {
     event.preventDefault();
     jQuery('html, body').animate({scrollTop: 0}, duration);
     return false;
     })
     });
    </script>
    <!-- go back to top --> 

    <!-- clock -->
<!--    <script src="js/frontend_js/radialIndicator.js"></script>
    <script src="js/frontend_js/angular.radialIndicator.js"></script>

    <script>
      var radialObj = radialIndicator('#clock', {
          radius: 25,
          barWidth: 5,
          barColor: '#FF0000',
          minValue: 0,
          maxValue: 60,
          fontWeight: 'normal',
          roundCorner: true,
          format: function (value) {
              var date = new Date();
              return date.getHours() + ':' + date.getMinutes();
          }
      });
       
      setInterval(function () {
          radialObj.value(new Date().getSeconds() + 1);
      }, 1000);
    </script> -->
    <!-- clock -->

    <!-- clock analog-->
<!--     <script>
      var canvas = document.getElementById("canvas");
      var ctx = canvas.getContext("2d");
      var radius = canvas.height / 2;
      ctx.translate(radius, radius);
      radius = radius * 0.90
      setInterval(drawClock, 1000);

      function drawClock() {
        drawFace(ctx, radius);
        drawNumbers(ctx, radius);
        drawTime(ctx, radius);
      }

      function drawFace(ctx, radius) {
        var grad;
        ctx.beginPath();
        ctx.arc(0, 0, radius, 0, 2*Math.PI);
        ctx.fillStyle = 'white';
        ctx.fill();
        grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
        grad.addColorStop(0, '#333');
        grad.addColorStop(0.5, 'white');
        grad.addColorStop(1, '#333');
        ctx.strokeStyle = grad;
        ctx.lineWidth = radius*0.1;
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
        ctx.fillStyle = '#333';
        ctx.fill();
      }

      function drawNumbers(ctx, radius) {
        var ang;
        var num;
        ctx.font = radius*0.15 + "px arial";
        ctx.textBaseline="middle";
        ctx.textAlign="center";
        for(num = 1; num < 13; num++){
          ang = num * Math.PI / 6;
          ctx.rotate(ang);
          ctx.translate(0, -radius*0.85);
          ctx.rotate(-ang);
          ctx.fillText(num.toString(), 0, 0);
          ctx.rotate(ang);
          ctx.translate(0, radius*0.85);
          ctx.rotate(-ang);
        }
      }

      function drawTime(ctx, radius){
          var now = new Date();
          var hour = now.getHours();
          var minute = now.getMinutes();
          var second = now.getSeconds();
          //hour
          hour=hour%12;
          hour=(hour*Math.PI/6)+
          (minute*Math.PI/(6*60))+
          (second*Math.PI/(360*60));
          drawHand(ctx, hour, radius*0.5, radius*0.07);
          //minute
          minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
          drawHand(ctx, minute, radius*0.8, radius*0.07);
          // second
          second=(second*Math.PI/30);
          drawHand(ctx, second, radius*0.9, radius*0.02);
      }

      function drawHand(ctx, pos, length, width) {
          ctx.beginPath();
          ctx.lineWidth = width;
          ctx.lineCap = "round";
          ctx.moveTo(0,0);
          ctx.rotate(pos);
          ctx.lineTo(0, -length);
          ctx.stroke();
          ctx.rotate(-pos);
      }
    </script>  -->

    <!-- data table Files -->
    <script src="js/frontend_js/datatables.min.js"></script>
    <script>
      $('#zero_config').DataTable();
    </script>
    <!-- data table Files -->
  </body>
</html>