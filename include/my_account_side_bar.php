<!-- my account -->
<div class="container my-4">
    <div class="row">
      <div class="col-sm-3 col-md-3 bg-dark">
        <div class="mt-4">
          <img class="col-sm-12" src="img/4.jpg" height="280px" style="border-radius: 50%">          
        </div>

        <h4 class="text-center text-light my-3">Chamara Madhushanka</h4>

        <div class="list-group text-center mb-3 mx-2">
          <a href="my_account.php" class="list-group-item list-group-item-action pb-3"><i class="fa fa-list"></i> My Order</a>
          <a href="edit_my_account.php" class="list-group-item list-group-item-action pb-3"><i class="fa fa-pencil"></i> Edit Account</a>
          <a href="change_password.php" class="list-group-item list-group-item-action pb-3"><i class="fa fa-user"></i> Change Password</a>
          <a href="delete_my_account.php" class="list-group-item list-group-item-action pb-3"><i class="fa fa-trash"></i> Delete Account</a>
          <a href="logout_my_account.php" class="list-group-item list-group-item-action pb-3"><i class="fa fa-power-off"></i> Logout</a>
        </div>
      </div>