<div class="container mt-3" style="height:40px">
  <div class="row col-md-12">

    <ul class="navbar-nav mr-auto offset-11">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle border p-1 text-center" href="shop.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #6D6D6D; width: 80px; height: 30px; border-radius: 10%; font-size: 13px">Sort by</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="font-size: 13px">
          <a class="dropdown-item" href="filter_by_latest_arrival.php">Latest Arrivals</a>
          <a class="dropdown-item" href="filter_by_name_asc.php">Name: A to Z</a>
          <a class="dropdown-item" href="filter_by_name_desc.php">Name: Z to A</a>
          <a class="dropdown-item" href="filter_by_price_asc.php">Price: Low to High</a>
          <a class="dropdown-item" href="filter_by_price_desc.php">Price: High to Low</a>
        </div>
      </li>
    </ul>
  </div>
</div>