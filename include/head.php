<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap CSS -->

    <!-- favicon -->
    <link href="img/logo.png" rel="icon"/>
    <!-- favicon -->

    <!-- owl carousel -->
    <link rel="stylesheet" href="css/frontend_css/owl.carousel.css">
    <link rel="stylesheet" href="css/frontend_css/owl.theme.default.css">
    <!-- owl carousel -->

    <!-- bzoom -->
    <link rel="stylesheet" type="text/css" href="css/frontend_css/styleBzoom4.css">
    <!-- bzoom -->

    <!-- datatable -->
    <link href="css/frontend_css/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- datatable -->

    <!--<link rel="stylesheet" href="css/custom.css" />-->
    <title>Ransi's Arcade</title>

    <!-- <link rel="stylesheet" type="text/css" href="css/frontend_css/style.css"> -->

    <style>
        /* body{
            font-family: Comic Sans MS;
        }  */

        input:focus { 
            background-color: yellow;
        }


        #nav-bar-top a{
            font-size: 13px;
            text-decoration: none;
        }

        .nav-right > a{
            font-size: 13px;
            color: gray;
            float: right;
        }

        .navbar > a{
            font-size: 15px;
        }

        .navbar a:hover{
            color: #D20000;
            text-decoration: none;
        }

        .search-area{
            color: #6D6D6D;
            width: 300px;
            padding-left: 5px;
            border-radius: 5px;
            border-width: thin;
            border-style: solid;
            border-color: #CCC;
            margin-top: 05px;
        }

        .search-area:focus{
            background-color: #fff;
            border: 2px gray solid;
            border-radius: 5px;
            box-shadow: none;
            outline: none;
        }

        .search-btn{
            background-color: transparent;
            border: none;
            font-size: 15px;
            margin-left: -30px;
        }

        .search-btn:hover{
            color: #CC2C2F;
        }

        .navbar2-link{
            width: 40px;
            height: 40px;
            background-color: gray;
            border-radius: 50%;
            text-align: center;
        }

        .navbar2 p{
            font-size: 12px;
            padding: 0;
            margin: 0;
        }

        .navbar2-link i{
            color: #fff;
        }

        .navbar2-link:hover{
            color: #fff;
            border-radius: 50%;
            background-color: #f37736;
            text-align: center;
        }

        #side-contact{
            position: fixed;
            top: 40%;
            right: 30px;
            z-index: 12;
        }

        #side-contact p{
            padding-top: 5px;
            margin: 0;
        }

        #side-contact i{
            width: 35px;
            height: 35px;
            background-color: gray;
            border-radius: 50%;
            text-align: center;
            color: #fff;
            padding: 10px;
        }

        #side-contact i:hover{
            background-color: #DD3A3A;
            color: #fff;
        }

        .side-contact-phone span{
            display: none;
        }

        .side-contact-phone:hover span{
            display: inline;
            font-size: 14px;
            background-color: #000;
            color: #fff;
            padding: 5px 25px 5px 10px;
            margin-right: -15px;
            border-radius: 5px;
        }

        #clock{
            position: fixed;
            float: left;
            top: 50%;
            right: 20px;
        }

        .card:hover{
            opacity: .7;
        }

        .advert{
            overflow: hidden;
            height: 120px;
        }

        .advert-img:hover{
            transform:scale(1.2);
            transition:all .4s ease;
        }

        .social_media{
            color: #000;
            margin: auto;
        }

        .social_media:hover{
            color: #fff;
        }

        .footer{
            color: #D3D3D3;
            line-height: 30px; 
            font-size: 14px;
        }

        .footer a{
            color: #D3D3D3;
        }

        .list-group-item{
            line-height:10px;
            padding:8px;
            border-width:0px;
        }

        .list-group a{
            color:#9B9B9B;
        }

        .product{
            width:175px;
            float:left;
        }

        .product:hover{
            opacity: .7;
        }

        .accordion .card-header:after{
            font-family: 'fontAwesome';
            content: "\f106";
            float: right;
        }

        .accordion .collapsed:after{
            content: "\f107";
        }

        .carousel-control-prev-icon{
            margin-left: -275px;
            height: 30px;
        }

        .carousel-control-next-icon{
            margin-right: -275px;
            height: 30px;
        }

        .back-to-top{
             background: none;
             margin: 0;
             position: fixed;
             bottom: 0;
             right: 10px;
             width: 60px;
             height: 60px;
             z-index: 100;
             display: none;
             text-decoration: none;
             color: #fc7700;
             background-color: transparent;
         }

        .back-to-top i {
             font-size: 40px;
         }

        /*pagination*/

        #pagin{
            font-size: 14px;
            float: right;
        }

        #pagin ul{
            align: center;
        }


        #pagin ul li{
            margin: 2px;
            align: center;
        }

        #pagin ul li a{
            color:white;
            padding: 2px 15px 2px 15px;
            background-color: gray;
            height:10px;
            text-decoration:none;
            font-size: 16px;
        }

        #pagin ul li a:hover{
            background-color: #DA4747;
        }
        /*pagination*/

    </style>

    <!-- navbar date & time -->
    <script>
    function startTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      m = checkTime(m);
      s = checkTime(s);
      document.getElementById('txt').innerHTML =
      h + ":" + m + ":" + s;
      var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
      if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
      return i;
    }
    </script>
  </head>

  <body onload="startTime()">