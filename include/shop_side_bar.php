<!-- starts container-->
<div class="container mt-4">
  <div class="row"><!-- sidebar start-->
    
    <div class="col-md-3">

      <div class="list-group">
        <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Likes</h5>
        <div class="dropdown-divider mb-3"></div>
        <?php 
        include "include/db_connection.php";

        $get_like = "SELECT like_index, pro_like, COUNT(DISTINCT pro_id) AS count_like FROM product_like GROUP BY like_index";
        $run_like = mysqli_query($con,$get_like);

          while($res_like = mysqli_fetch_array($run_like)){

            switch ($res_like['like_index']) {

                case "1":
                    $img = 'a.png';
                    break;
                case "2":
                    $img = 'b.png';
                    break;
                case "3":
                    $img = 'c.png';
                    break;
                default:
                    $img = 'e.png';
            } 
        ?>

        <a href="filter_by_like.php?like=<?php echo $res_like['pro_like'] ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"><span class="text-capitalize"><img src="img/like/<?php echo $img ?>" class="rounded-circle" width="20" height="20" title="<?php echo $res_like['pro_like']; ?>"/> - <?php echo $res_like['pro_like']; ?></span><span class="badge badge-dark badge-pill"><?php echo $res_like['count_like'] ?></span></a>
        
        <?php } ?>
      </div>

      <div class="dropdown-divider mb-3"></div>

      <div class="list-group">
        <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Comments</h5>
        <div class="dropdown-divider mb-3"></div>
        <?php 

        $get_comment = "SELECT COUNT(DISTINCT pro_id) AS count_comment FROM comment";
        $run_comment = mysqli_query($con,$get_comment);

          while($res_comment = mysqli_fetch_array($run_comment)){
 
        ?>

        <a href="filter_by_comment.php" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Comment <span class="badge badge-dark badge-pill"><?php echo $res_comment['count_comment'] ?></span></a>
        
        <?php } ?>
      </div>

      <div class="dropdown-divider mb-3"></div>

      <div class="list-group">
        <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Category</h5>
        <div class="dropdown-divider mb-3"></div>
        <?php 

        $get_style = "SELECT * FROM style";
        $run_style = mysqli_query($con,$get_style);

          while($res_style = mysqli_fetch_array($run_style)){

            $style = $res_style['style'];

            if ($res_style['status'] == '1') {  

              $count_style_product = "SELECT * FROM product WHERE status = '1' AND style = '$style'";
              $run_count_style_product = mysqli_query($con,$count_style_product);
              $res_count_style_product = mysqli_num_rows($run_count_style_product);
        ?>

        <a href="filter_by_style.php?style=<?php echo $style ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"><?php echo $style ?> <span class="badge badge-dark badge-pill"><?php echo $res_count_style_product; ?></span></a>
        
        <?php } } ?>
      </div>

      <div class="dropdown-divider mb-3"></div>

      <div class="list-group">
        <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Fabric</h5>
        <div class="dropdown-divider mb-3"></div>

        <?php 

        $get_fabric = "SELECT * FROM fabric";
        $run_fabric = mysqli_query($con,$get_fabric);

          while($res_fabric = mysqli_fetch_array($run_fabric)){

            $fabric = $res_fabric['fabric'];

            if ($res_fabric['status'] == '1') {  

              $count_fabric_product = "SELECT * FROM product WHERE status = '1' AND fabric = '$fabric'";
              $run_count_fabric_product = mysqli_query($con,$count_fabric_product);
              $res_count_fabric_product = mysqli_num_rows($run_count_fabric_product);
        ?>

        <a href="filter_by_fabric.php?fabric=<?php echo $fabric ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"><?php echo $fabric ?><span class="badge badge-dark badge-pill"><?php echo $res_count_fabric_product ?></span></a>
        
        <?php } } ?>
      </div>

      <div class="dropdown-divider mb-3"></div>

      <div class="list-group">
        <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Occasion</h5>
        <div class="dropdown-divider mb-3"></div>

        <?php 

          $count_casual_wear_product = "SELECT * FROM product WHERE status = '1' AND casual_wear = 'casual'";
          $run_count_casual_wear_product = mysqli_query($con,$count_casual_wear_product);
          $res_count_casual_wear_product = mysqli_num_rows($run_count_casual_wear_product);

        ?>
        <a href="filter_by_occasion.php?occasion=casual" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Casual<span class="badge badge-dark badge-pill"><?php echo $res_count_casual_wear_product ?></span></a>

        <?php 

          $count_party_wear_product = "SELECT * FROM product WHERE status = '1' AND party_wear = 'party'";
          $run_count_party_wear_product = mysqli_query($con,$count_party_wear_product);
          $res_count_party_wear_product = mysqli_num_rows($run_count_party_wear_product);

        ?>
        <a href="filter_by_occasion.php?occasion=party" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Party<span class="badge badge-dark badge-pill"><?php echo $res_count_party_wear_product ?></span></a>

        <?php 

          $count_office_wear_product = "SELECT * FROM product WHERE status = '1' AND office_wear = 'office'";
          $run_count_office_wear_product = mysqli_query($con,$count_office_wear_product);
          $res_count_office_wear_product = mysqli_num_rows($run_count_office_wear_product);

        ?>
        <a href="filter_by_occasion.php?occasion=office" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Office<span class="badge badge-dark badge-pill"><?php echo $res_count_office_wear_product ?></span></a>

        <?php 

          $count_cocktail_wear_product = "SELECT * FROM product WHERE status = '1' AND cocktail_wear = 'cocktail'";
          $run_count_cocktail_wear_product = mysqli_query($con,$count_cocktail_wear_product);
          $res_count_cocktail_wear_product = mysqli_num_rows($run_count_cocktail_wear_product);

        ?>
        <a href="filter_by_occasion.php?occasion=cocktail" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Cocktail<span class="badge badge-dark badge-pill"><?php echo $res_count_cocktail_wear_product ?></span></a>

        <?php 

          $count_wedding_wear_product = "SELECT * FROM product WHERE status = '1' AND wedding_wear = 'wedding & engagement'";
          $run_count_wedding_wear_product = mysqli_query($con,$count_wedding_wear_product);
          $res_count_wedding_wear_product = mysqli_num_rows($run_count_wedding_wear_product);

        ?>
        <a href="filter_by_occasion.php?occasion=wedding & engagement" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Wedding & Engagement<span class="badge badge-dark badge-pill"><?php echo $res_count_wedding_wear_product ?></span></a>
        

      </div>

      <div class="dropdown-divider mb-3"></div>

      <div class="list-group">
        <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Price</h5>
        <div class="dropdown-divider mb-3"></div>
        <?php 

        $get_price_range = "SELECT * FROM price_range";
        $run_price_range = mysqli_query($con,$get_price_range);

          while($res_price_range = mysqli_fetch_array($run_price_range)){

            $min = $res_price_range['min'];
            $max = $res_price_range['max'];

            $count_price_product = "SELECT * FROM product  WHERE price >= '$min' AND price < '$max'";
            $run_count_price_product = mysqli_query($con,$count_price_product);
            $res_count_price_product = mysqli_num_rows($run_count_price_product);
        ?>

        <a href="filter_by_price.php?min=<?php echo $min ?>&max=<?php echo $max ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">Rs. <?php echo $min ?> - Rs. <?php echo $max ?><span class="badge badge-dark badge-pill"><?php echo $res_count_price_product; ?></span></a>

        <?php } ?>
      </div>

      <div class="dropdown-divider mb-3 mt-4"></div>

      <h5 class="list-group-item list-group-item text-uppercase" style="color: #6D6D6D">Color</h5>
      <div class="dropdown-divider mb-3"></div>

      <?php 

      $get_color_range = "SELECT * FROM color_range";
      $run_color_range = mysqli_query($con,$get_color_range);

        while($res_color_range = mysqli_fetch_array($run_color_range)){

          $name = $res_color_range['name'];
          $code = $res_color_range['code'];

          if ($res_color_range['status'] == '1') { 

          $count_color_product = "SELECT * FROM product  WHERE color LIKE '%$name%'";
          $run_count_color_product = mysqli_query($con,$count_color_product);
          $res_count_color_product = mysqli_num_rows($run_count_color_product);
      ?>

      <a href="filter_by_color.php?color=<?php echo $name ?>" class="list-group-item list-group-item-action"><button class="btn btn-sm border" role="button" style="background-color: <?php echo $code ?>"></button> - <?php echo $name ?><span class="badge badge-dark badge-pill float-right"><?php echo $res_count_color_product; ?></span></a>

      <?php } } ?>

    </div>