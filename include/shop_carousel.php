 <!-- starts carousel -->
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>

    <?php

    include "include/db_connection.php";

    $select_shop_banner = mysqli_query($con,"SELECT * FROM page_banner");

    while($res_shop_banner = mysqli_fetch_array($select_shop_banner)){

        $shop1 = $res_shop_banner['shop1'];
        $shop2 = $res_shop_banner['shop2'];
        $shop3 = $res_shop_banner['shop3'];
    }
    ?>

    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/page_banner/<?php echo $shop1 ?>" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <h2>Shop</h2>
          <p>exclusive sarees from our flagship store!!</p>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/page_banner/<?php echo $shop2 ?>" alt="Second slide">
        <div class="carousel-caption d-none d-md-block">
          <h4>Handloom Sarees</h4>
          <p>exclusive sarees from our flagship store!!</p>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/page_banner/<?php echo $shop3 ?>" alt="Third slide">
        <div class="carousel-caption d-none d-md-block">
          <h4>Wedding Sarees</h4>
          <p>exclusive sarees from our flagship store!!</p>
        </div>
      </div>
    </div>

  </div>
  <!-- ends carousel -->