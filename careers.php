<?php 
	include "include/head.php";
	include "include/navbar.php";
?>
    
    <!-- starts jumbotron -->
	<div class="jumbotron mb-0" style="background-image:url(img/page_banner/career.jpg); height: 250px; width: 100%;">
	    <div class="container">
	        <div class="pt-5">
	          <h1 class="text-center" style="color: #fff;">Careers</h1>
	        </div>
    	</div>
    </div>
    <!-- ends jumbotron -->
	
	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-white">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="careers.php" style="text-decoration: none">Careers</a></li>
	  </ol>
	</nav>
	<!-- ends breadcrumb -->

    <div class="container"><!-- starts container-->

    	<section class="text-center mb-5"><!-- starts FAQ intro-->  
	      <h3 class="text-muted mb-3 mt-3">Join Our Family</h3>
	      <p><span style="font-size: 14px; font-weight: 500; color: #6D6D6D">If you are interested to be part of the latest growing e-commerce saree shop in Sri Lanka, then please join us!</span></p>   

	      <table class="table table-striped text-left mt-5">
			  <thead>
			    <tr>
			      <th scope="col">Job Type</th>
			      <th scope="col">Position</th>
			      <th scope="col" width="150px">Closing Date</th>
			      <th scope="col"></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>Full Time</td>
			      <td>Sales Assistant</td>
			      <td>1st April 2019</td>
			      <td class="text-center"><a href="img/career/Job Vacancies Sri Lanka 2018.jpg" class="btn btn-danger btn-sm">View</a></td>
			    </tr>
				<tr><td colspan="4" align="center">There are no current vacancies.</td></tr>
			  </tbody>
			</table>
	    </section><!-- ends FAQ intro-->

    </div><!-- ends container-->

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>