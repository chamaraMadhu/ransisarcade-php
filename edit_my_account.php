<?php 
	include "include/head.php";
	include "include/navbar.php";
?>

<?php
      $select_career_banner = mysqli_query($con,"SELECT career FROM page_banner");
    while($res_career_banner = mysqli_fetch_array($select_career_banner)){

    $career_banner = $res_career_banner['career'];
  }
     ?>

  <!-- starts jumbotron -->
  <div class="jumbotron mb-0" style="background-image:url(img/page_banner/<?php echo $career_banner ?>); height: 250px; width: 100%;">
      <div class="container">
          <div class="pt-5">
            <h1 class="text-center" style="color: #fff">My Account</h1>
          </div>
      </div>
    </div>
    <!-- ends jumbotron -->

	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light" style="font-size: 15px">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="shop.php" style="text-decoration: none">My Account</a></li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

  <!-- my account -->
  <?php include "include/my_account_side_bar.php" ?>

      <div class="col-sm-9 bg-light">
        <div class="my-account-order my-4">
          <h3 class="text-center">Edit My Account</h3>
          <hr>

          <div class="bg-white p-4">
            <form>
              <div class="form-group">
                <label for="inputEmail4">Name</label>
                <input type="text" class="form-control" id="inputEmail4">
              </div>
              <div class="form-group">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4">
              </div>
              <div class="form-group">
                <label for="inputAddress">Country</label>
                <input type="text" class="form-control" id="inputAddress">
              </div>
              <div class="form-group">
                <label for="inputAddress">City</label>
                <input type="text" class="form-control" id="inputAddress">
              </div>
              <div class="form-group">
                <label for="inputAddress">Address</label>
                <input type="text" class="form-control" id="inputAddress">
              </div>
              <div class="form-group">
                <label for="inputAddress2">Contact</label>
                <input type="text" class="form-control" id="inputAddress2">
              </div>
              <label for="image">Image</label>
              <div class="custom-file form-group">
                <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                <div class="invalid-feedback">Example invalid custom file feedback</div>
              </div>
              <div class="mt-2"> 
                <img src="img/4.jpg" width="180px">                
              </div>
              <button type="submit" class="btn btn-success mt-3">Edit My Account</button>
            </form>         
          </div>
        </div>
      </div>  
    </div>    
  </div>
  <!-- my account -->
  


<?php   
	include "include/socialMedia.php";
	include "include/footer.php";
?>