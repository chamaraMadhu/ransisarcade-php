<?php 
	include "include/head.php";
	include "include/navbar.php";
?>
	
	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light mb-5">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
      <li class="breadcrumb-item"><a href="shop.php" style="color: #422C2F; font-weight: 500; text-decoration: none">shop</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><?php $name = $_GET['name']; echo $name ?></li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

  <div class="container">
    <?php
      if(isset($_GET['success_msg'])){
    ?>
                <div class="alert alert-success alert-block col-md-12 mt-4 mb-4">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['success_msg']; ?> </strong> 
                </div>

    <?php }elseif(isset($_GET['fail_msg'])){ ?>

        <div class="alert alert-danger alert-block col-md-12 mt-4 mb-4">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong><?php echo $_GET['fail_msg']; ?></strong> 
                </div>
    <?php } ?>
  </div>

  <main class="page product-page">
      <section class="clean-block clean-product dark">
          <div class="container">

            <?php

            $id = $_GET['id'];

            $get_product = "SELECT * FROM product WHERE id = $id";
            $run_product = mysqli_query($con,$get_product);

            while($res_product = mysqli_fetch_array($run_product)){

              $product_name = $res_product['name'];
              $style = $res_product['style'];
              $status = $res_product['status'];
              $img_front = $res_product['image_front'];
              $img_jacket = $res_product['image_jacket'];
              $img_border = $res_product['image_border'];
              $img_back = $res_product['image_back'];
              $color = $res_product['color'];
              $pro_price = $res_product['price'];
              $desc = $res_product['description'];
              $keywords = $res_product['keywords'];

              if ($res_product['status'] == '1') {

            ?>
              <div class="block-content">
                  <div class="product-info">
                      <div class="row">

                          <div class="col-md-1 mt-3">
                          </div>

                          <div class="col-sm-4 mt-3">
                              <div class="bzoom_wrap" style="width: 300px; height: 500px">
                                <ul id="bzoom">
                                    <li>
                                        <img class="bzoom_thumb_image border" src="img/product/front/<?php echo $img_front ?>"/>
                                        <img class="bzoom_big_image border" src="img/product/front/<?php echo $img_front ?>"/>
                                    </li>
                                    <li>
                                        <img class="bzoom_thumb_image border" src="img/product/jacket/<?php echo $img_jacket ?>"/>
                                        <img class="bzoom_big_image border" src="img/product/jacket/<?php echo $img_jacket ?>"/>
                                    </li>
                                    <li>
                                        <img class="bzoom_thumb_image border" src="img/product/border/<?php echo $img_border ?>"/>
                                        <img class="bzoom_big_image border" src="img/product/border/<?php echo $img_border ?>"/>
                                    </li>
                                    <li>
                                        <img class="bzoom_thumb_image border" src="img/product/back/<?php echo $img_back ?>"/>
                                        <img class="bzoom_big_image border" src="img/product/back/<?php echo $img_back ?>"/>
                                    </li>
                                </ul>
                              </div>
                          </div>

                          <div class="col-md-6 mt-3 bg-light px-4 py-3" style="min-height: 500px">
                              <h1 class="text-muted"><?php echo $product_name ?></h1>
                              <h6 class="text-muted mb-1 ">Product ID : <?php echo $res_product['pro_id']; ?></h6>
                              <div class="row">
                                <div class="col">
                                <?php

                                    $get_like = "SELECT like_index, pro_like, COUNT(pro_like) AS count_like FROM product_like WHERE pro_id = '$res_product[pro_id]' GROUP BY like_index";
                                    $run_like  = mysqli_query($con,$get_like );

                                    while($res_like  = mysqli_fetch_array($run_like )){

                                      switch ($res_like['pro_like']) {

                                          case "like":
                                              $img = 'a.png';
                                              break;
                                          case "love":
                                              $img = 'b.png';
                                              break;
                                          case "wow":
                                              $img = 'c.png';
                                              break;
                                          default:
                                              $img = 'e.png';
                                      }

                                  ?>
                              
                                <img src="img/like/<?php echo $img ?>" class="rounded-circle mt-2" width="20" height="20" title="<?php echo $res_like['pro_like']; ?>"><span class="text-muted mr-2" style="font-size: 11px"> <?php echo $res_like['count_like'] ?></span>

                                <?php } ?>

                                <?php

                                  $get_comment = "SELECT COUNT(comment) AS count_comment FROM comment WHERE pro_id = '$res_product[pro_id]'";
                                  $run_comment  = mysqli_query($con,$get_comment);

                                  while($res_comment  = mysqli_fetch_array($run_comment)){

                                ?>
                                <?php if($res_comment['count_comment']>0){ ?>
                                  <span class="text-muted mt-2" style="font-size: 14px; float: right"> <?php echo $res_comment['count_comment']; echo ' Comments' ?></span>
                                <?php } } ?>
                                </div>
                              </div>

                              <div class="dropdown-divider mt-2 mb-3"></div>
                              <h4 class="mb-3 mt-3" style="color:#D95F2A"><b>Rs. <?php echo $pro_price ?></b></h4> 
                              <p class="text-muted text-justify mt-4"><?php echo $desc?></p>
                              <div class="dropdown-divider mt-4 mb-3"></div>

                              <p class="text-muted"><b>Please like...</b></p>
    
                              <form action="product_like_query.php" method="GET" style="display: inline">
                                  <input type="hidden" name="id" value="<?php echo $res_product['id'] ?>"/>
                                  <input type="hidden" name="pro_id" value="<?php echo $res_product['pro_id'] ?>"/>
                                  <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>"/>
                                  <input type="hidden" name="pro_like" value="like"/>
                                  <button class="btn btn-sm p-0 mr-3" type="submit" name="submit" style="background-color: #F9F9F9; border-radius: 50%"><img src="img/like/a.png" class="rounded-circle" width="30" height="30" title="like"></button>
                              </form>

                              <form action="product_like_query.php" method="GET" style="display: inline">
                                  <input type="hidden" name="id" value="<?php echo $res_product['id'] ?>"/>
                                  <input type="hidden" name="pro_id" value="<?php echo $res_product['pro_id'] ?>"/>
                                  <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>"/>
                                  <input type="hidden" name="pro_like" value="love"/>
                                  <button class="btn btn-sm p-0 mr-3" type="submit" name="submit" style="background-color: #F9F9F9; border-radius: 50%"><img src="img/like/b.png" class="rounded-circle" width="30" height="30" title="love"></button>
                              </form>

                              <form action="product_like_query.php" method="GET" style="display: inline">
                                  <input type="hidden" name="id" value="<?php echo $res_product['id'] ?>"/>
                                  <input type="hidden" name="pro_id" value="<?php echo $res_product['pro_id'] ?>"/>
                                  <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>"/>
                                  <input type="hidden" name="pro_like" value="wow"/>
                                  <button class="btn btn-sm p-0 mr-3" type="submit" name="submit" style="background-color: #F9F9F9; border-radius: 50%"><img src="img/like/c.png" class="rounded-circle" width="30" height="30" title="wow"></button>
                              </form>

                              <form action="product_like_query.php" method="GET" style="display: inline">
                                  <input type="hidden" name="id" value="<?php echo $res_product['id'] ?>"/>
                                  <input type="hidden" name="pro_id" value="<?php echo $res_product['pro_id'] ?>"/>
                                  <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>"/>
                                  <input type="hidden" name="pro_like" value="dislike"/>
                                  <button class="btn btn-sm p-0 mr-3" type="submit" name="submit" style="background-color: #F9F9F9; border-radius: 50%"><img src="img/like/e.png" class="rounded-circle" width="30" height="30" title="dislike"></button>
                              </form>


                              <div class="dropdown-divider mt-3 mb-3"></div>
                              <button class="btn btn-danger mb-3" type="button" data-toggle="modal" data-target="#Modal1<?php echo $id ?>" style="width: 100%">Inquiry</button> 
                            </div>
                          </div>

                      </div>
                  </div>

                  <!-- popper for Inqiry form -->
                  <div class="modal fade" id="Modal1<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true ">
                    <div class="modal-dialog" role="document ">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="modal-title" id="exampleModalLabel">Inquery of <?php echo $product_name ?></h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true ">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form method="POST" action="contact_form_query_product_detail.php">
                                    <div class="row mb-4">
                                        <div class="col">
                                            <input type="text" class="form-control" name="fname" placeholder="First name" required>
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" name="lname" placeholder="Last name" required>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <div class="col">
                                            <input type="email" class="form-control" name="mail" placeholder="Email" required>
                                        </div>
                                    </div>

                                    <div class="row mb-4">
                                        <div class="col">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject" value="<?php echo $res_product['pro_id'].' - '.$product_name.' - '.$color ?>" required>
                                        </div>
                                    </div>

                                    <div class="row mb-4" id="map">
                                        <div class="col">
                                            <textarea class="form-control" placeholder="Massage" name="msg" style="height:150px" required></textarea>
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $res_product['id'] ?>">
                                    <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>">

                                    <div class="row">
                                        <div class="col">
                                        <a href="#"><input type="submit" class="btn mb-4" value="Send" name="submit" style="background-color:#EF5B00; color: #fff; width:100%"></a>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                  </div>
                  <!-- popper for Inqiry form -->

                  <div class="product-info mt-5">
                      <div>
                          <ul class="nav nav-tabs" id="myTab">
                              <li class="nav-item"><a class="nav-link active text-muted" role="tab" data-toggle="tab" href="#description" id="description-tab"><b>Description</b></a></li>
                              <li class="nav-item"><a class="nav-link text-muted" role="tab" data-toggle="tab" href="#comments" id="reviews-tab"><b>Comments</b></a></li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane active fade show description p-5 pb-3" role="tabpanel" id="description">
                                  <p class="mb-4"><?php echo $desc ?></p>

                                  <div class="table-responsive">
                                      <table cellpadding="5px">
                                          <tbody style="font-size: 15px; font-weight: 500; color: #353535">
                                              <tr>
                                                  <td class="stat"><b>Color</b></td>
                                                  <td>: <?php echo $res_product['color'] ?></td>
                                              </tr>
                                              <tr>
                                                  <td class="stat"><b>Category</b></td>
                                                  <td>: <?php echo $res_product['style'] ?></td>
                                              </tr>
                                              <tr>
                                                  <td class="stat"><b>Fabric</b></td>
                                                  <td>: <?php echo $res_product['fabric'] ?></td>
                                              </tr>
                                              <tr>
                                                  <td class="stat"><b>Occasion</b></td>
                                                  <td>: <?php echo $res_product['casual_wear'].', '.$res_product['party_wear'].', '.$res_product['office_wear'].', '.$res_product['cocktail_wear'].', '.$res_product['wedding_wear'] ?></td>
                                              </tr>
                                              <tr>
                                                  <td class="stat"><b>Wash and Care</b></td>
                                                  <td>: <?php echo $res_product['wash_care'] ?></td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </div>

                                  <div class="row mt-5 ml-5 mr-5">
                                      <div class="col-md-3">
                                          <figure class="figure bg-danger text-light text-center pb-2"><img class="img-fluid figure-img" src="img/product/front/<?php echo $img_front ?>" width="250px">Front view</figure>
                                      </div>
                                      <div class="col-md-3">
                                          <figure class="figure  bg-danger text-light text-center pb-2"><img class="img-fluid figure-img" src="img/product/jacket/<?php echo $img_jacket ?>" width="250px">Jacket view</figure>
                                      </div>
                                      <div class="col-md-3">
                                          <figure class="figure  bg-danger text-light text-center pb-2"><img class="img-fluid figure-img" src="img/product/border/<?php echo $img_border ?>" width="250px">Border view</figure>
                                      </div>
                                      <div class="col-md-3">
                                          <figure class="figure  bg-danger text-light text-center pb-2"><img class="img-fluid figure-img" src="img/product/back/<?php echo $img_back ?>" width="250px">Back view</figure>
                                      </div>
                                  </div>
                              </div>

                              <div class="tab-pane fade show p-4" role="tabpanel" id="comments">
                                  <form action="add_comment_query.php" method="POST" class="needs-validation mt-3" validate>
                                      <div class="form-row p-3 m-4 border bg-light">
                                          <h6 class="text-muted"><b>Add a comment</b></h6>

                                          <div class="col-sm-6 col-md-12 mb-3 form-group row">
                                              <label class="col-sm-2 col-form-label text-right">Name <b class="text-danger">*</b></label>
                                              <div class="col-sm-4">
                                                  <input type="text" name="name" class="form-control" id="validationCustom02" required>
                                                  <div class="invalid-feedback">
                                                  Please insert your name.
                                                  </div>
                                              </div>
                                          </div>

                                          <div class="col-sm-6 col-md-12 mb-3 form-group row">
                                              <label class="col-sm-2 col-form-label text-right">Comment <b class="text-danger">*</b></label>
                                              <div class="col-sm-4 col-md-10">
                                                  <textarea class="form-control" rows="3" name="comment" id="validationCustom03" required></textarea>
                                                  <div class="invalid-feedback">
                                                  Please insert your comment.
                                                  </div>
                                              </div>
                                          </div>

                                          <div class="col-sm-6 col-md-12 mb-3 form-group row">
                                              <label class="col-sm-2 col-form-label text-right"><b class="text-danger">*</b></label>
                                              <div class="col-sm-4">
                                                <table cellpadding="5px">
                                                    <tr>
                                                        <td><img src="img/like/a.png" class="rounded-circle ml-2 mt-1" width="30" height="30" title="like"></td>
                                                        <td> <input type="radio" name="icon" value="like" id="validationCustom02" required style="cursor: pointer"></td>
                                                        <td><img src="img/like/b.png" class="rounded-circle ml-2 mt-1" width="30" height="30" title="love"></td>
                                                        <td> <input type="radio" name="icon" value="love" id="validationCustom02" required style="cursor: pointer"></td>
                                                        <td><img src="img/like/c.png" class="rounded-circle ml-2 mt-1" width="30" height="30" title="wow"></td>
                                                        <td> <input type="radio" name="icon" value="wow" id="validationCustom02" required style="cursor: pointer"></td>                          
                                                        <td><img src="img/like/e.png" class="rounded-circle ml-2 mt-1" width="30" height="30" title="dis like"></td>
                                                        <td> <input type="radio" name="icon" value="dis_like" id="validationCustom02" required style="cursor: pointer"></td>
                                                    </tr>
                                                </table>
                                              </div>
                                          </div>

                                          <input type="hidden" name="id" value="<?php echo $id ?>">
                                          <input type="hidden" name="pro_id" value="<?php echo $res_product['pro_id'] ?>">
                                          <input type="hidden" name="name" value="<?php echo $res_product['name'] ?>">

                                          <div class="col-md-12 mb-0 form-group row">
                                              <label class="col-sm-2 col-form-label text-right"></label>
                                              <div class="col-sm-4">
                                                  <button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Sunmit</button>
                                              </div>
                                          </div>

                                          <div class="col-md-12 mb-0 form-group row">
                                              <label class="col-sm-2 col-form-label text-right"></label>
                                              <div class="col-sm-4">
                                                  <b class="text-danger" style="font-size: 14px">* Denotes Required</b>
                                              </div>
                                          </div>
                                      </div>
                                  </form>

                                  <?php

                                  $get_comment = "SELECT * FROM comment WHERE pro_id = '$res_product[pro_id]'";
                                  $run_comment  = mysqli_query($con,$get_comment );

                                  while($res_comment  = mysqli_fetch_array($run_comment )){

                                  ?>

                                  <div class="reviews border p-3 m-4">
                                      <div class="review-item">

                                        <?php

                                        switch ($res_comment['icon_comment']) {

                                            case "like":
                                                $img = 'a.png';
                                                break;
                                            case "love":
                                                $img = 'b.png';
                                                break;
                                            case "wow":
                                                $img = 'c.png';
                                                break;
                                            case "crazy":
                                                $img = 'd.png';
                                                break;
                                            default:
                                                $img = 'e.png';
                                        }

                                        ?>
                                          <span class="text-muted"><b><?php echo $res_comment['name'] ?></b> <?php echo $res_comment['date_time'] ?></span></br>
                                          <p class="text-muted"><img src="img/like/<?php echo $img ?>" class="rounded-circle ml-2 mt-1" width="30" height="30"> <?php echo $res_comment['comment'] ?></p>
                                      </div>
                                  </div>

                                  <?php } ?>
                              </div>

                          </div>
                      </div>
                  </div>

                  <div class="clean-related-items mb-5">
                      <h4 class="text-muted text-center">Related Products</h4>
                      <hr width="50px" style="border-style: solid; border-width: 1px; border-color: #E05656">
                      <div class="items mt-4">
                          <div class="row justify-content-center">
                              <div class="owl-carousel owl-theme">

                                <?php

                                $get_style = "SELECT * FROM product WHERE id = $id";
                                $run_style = mysqli_query($con,$get_style);

                                while($res_style = mysqli_fetch_array($run_style)){

                                    $product_style = $res_style['style'];

                                    $get_related_product = "SELECT * FROM product WHERE style = '$product_style' AND id<>'$id'";
                                    $run_related_product = mysqli_query($con,$get_related_product);

                                    while($res_related_product = mysqli_fetch_array($run_related_product)){

                                      $pro_id = $res_related_product ['id'];
                                      $product_name = $res_related_product ['name'];
                                      $style = $res_related_product ['style'];
                                      $img_front = $res_related_product ['image_front'];
                                      $pro_price = $res_related_product ['price'];

                                      if ($res_related_product['status'] == '1') {

                                ?>

                                <div class="card my-2 border">
                                  <a href="product_detail.php?id=<?php echo $pro_id ?>&&name=<?php echo $product_name ?>"><img src="img/product/front/<?php echo $img_front ?>" alt="Card image cap" width="149px"></a>
                                  <div class="px-3 py-2" style="height: 145px">
                                    <p class="text-muted text-sm mb-0" style="height: 25px; font-size: 13px"><?php echo $style ?></p>
                                    <h6 class="mb-0" style="height: 50px; font-size: 14px"><?php echo $product_name ?></h6>
                                    <span class="card-text text-sm mb-0 text-muted" style="height: 25px; font-size: 13px">Rs. <?php echo $pro_price ?></span><?php if($res_related_product['old_price']<>0){ ?><span class="text-danger" style="font-size: 9px;"> &nbsp; <strike>Rs.<?php echo $res_related_product['old_price'] ?></strike></span><?php } ?> 
                                    <div class="row px-3 mt-1">
                                        <?php

                                          $get_like = "SELECT like_index, pro_like, COUNT(pro_like) AS count_like FROM product_like WHERE pro_id = '$res_related_product[pro_id]' GROUP BY like_index";
                                          $run_like  = mysqli_query($con,$get_like );

                                          while($res_like  = mysqli_fetch_array($run_like )){

                                            switch ($res_like['like_index']) {

                                                case "1":
                                                    $img = 'a.png';
                                                    break;
                                                case "2":
                                                    $img = 'b.png';
                                                    break;
                                                case "3":
                                                    $img = 'c.png';
                                                    break;
                                                case "4":
                                                    $img = 'd.png';
                                                    break;
                                                default:
                                                    $img = 'e.png';
                                            }
                                        ?>

                                          <span><img src="img/like/<?php echo $img ?>" class="rounded-circle"  style="margin-right: 1px; width: 12px; float: left"/></span>
                                        <?php  } ?>

                                        <?php

                                          $get_lik = "SELECT pro_like, COUNT(pro_like) AS count FROM product_like WHERE pro_id = '$res_related_product[pro_id]'";
                                          $run_lik  = mysqli_query($con,$get_lik);

                                          while($res_lik  = mysqli_fetch_array($run_lik)){

                                        ?>
                                        <?php if($res_lik['count']>0){ ?>
                                          <span class="text-muted" style="font-size: 9px; margin-top: -3px; padding-left: 1px"> <?php echo $res_lik['count']; } ?></span>
                                        <?php  } ?>
                                      </div>

                                      <div class="row px-3"> 
                                      <?php

                                          $get_comment = "SELECT COUNT(comment) AS count_comment FROM comment WHERE pro_id = '$res_related_product[pro_id]'";
                                          $run_comment  = mysqli_query($con,$get_comment);

                                          while($res_comment  = mysqli_fetch_array($run_comment)){

                                        ?>
                                        <?php if($res_comment['count_comment']>0){ 


                                          $get_lik = "SELECT pro_like, COUNT(pro_like) AS count FROM product_like WHERE pro_id = '$res_related_product[pro_id]'";
                                          $run_lik  = mysqli_query($con,$get_lik);

                                          while($res_lik  = mysqli_fetch_array($run_lik)){ ?>

                                          <span class="text-muted <?php if($res_lik['count']==0)echo 'mt-3'; ?>" style="font-size: 10px; margin-left: 75px"> <?php echo $res_comment['count_comment']; echo ' Comments'; } } ?></span>
                                        <?php  } ?>                 
                                      </div>

                                    </div>

                                    <div class="mt-0">
                                      <a class="btn pt-1 m-0 px-0" href="product_detail.php?id=<?php echo $res_related_product['id'] ?>&&name=<?php echo $res_related_product['name'] ?>" style="width: 100%; background-color: gray; color: #fff; font-size: 11px">View</a>
                                    </div>

                                  </div>

                                <?php } } } ?>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>

            <?php } } ?>

          </div>
      </section>
  </main>

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>
