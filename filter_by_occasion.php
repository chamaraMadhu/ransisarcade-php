<?php 
  include "include/head.php";
  include "include/navbar.php";
  include "include/shop_carousel.php";
?>
	
<!-- starts breadcrumb -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-light">
    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500">Home</a></li>
    <li class="breadcrumb-item"><a href="shop.php" style="color: #422C2F; font-weight: 500">shop</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php $occasion = $_GET['occasion']; echo $occasion ?></li>
  </ol>
</nav>
<!-- ends breadcrumb -->
  
   
<?php 
  include "include/sort_by.php";
  include "include/shop_side_bar.php";
?>
        <div class="col-md-9">

          <?php

            $results_per_page = 20;

            switch ($_GET['occasion']) {

                case "casual":
                    $occasion = 'casual';
                    break;
                case "party":
                    $occasion = 'party';
                    break;
                case "office":
                    $occasion = 'office';
                    break;
                case "cocktail":
                    $occasion = 'cocktail';
                    break;
                default:
                    $occasion = 'wedding & engagement';
            }

            switch ($_GET['occasion']) {

                case "casual":
                    $occasion_db = 'casual_wear';
                    break;
                case "party":
                    $occasion_db = 'party_wear';
                    break;
                case "office":
                    $occasion_db = 'office_wear';
                    break;
                case "cocktail":
                    $occasion_db = 'cocktail_wear';
                    break;
                default:
                    $occasion_db = 'wedding_wear';
            }

            $sql= "SELECT * FROM product WHERE $occasion_db = '$occasion'";
            $res = mysqli_query($con,$sql);
            $num_of_results = mysqli_num_rows($res);

            $num_of_pages = ceil($num_of_results/$results_per_page);

            if (!isset($_GET['page'])) {
              $page = 1;
            }else{
              $page = $_GET['page'];
            }

            $starting_page = ($page-1)*$results_per_page;

            $get_product = "SELECT * FROM product WHERE $occasion_db = '$occasion' LIMIT $starting_page, $results_per_page";
            $run_product = mysqli_query($con,$get_product);


            while($res_product = mysqli_fetch_array($run_product)){

              $pro_id= $res_product['id'];
              $product_name = $res_product['name'];
              $pro_style = $res_product['style'];
              $status = $res_product['status'];
              $img_front = $res_product['image_front'];
              $price = $res_product['price'];
              $color = $res_product['color'];

              if ($res_product['status'] == '1') {

            ?>
          
          <div class="card product  ml-4 my-2 border">
            <a href="product_detail.php?id=<?php echo $pro_id ?>&&name=<?php echo $product_name ?>"><img src="img/product/front/<?php echo $img_front ?>" alt="Card image cap" width="100%" height="220"></a>
            <div class="px-3 py-2" style="height: 150px">
              <p class="text-muted text-sm mb-0 pt-1" style="height: 25px; font-size: 13px"><?php echo $pro_style ?></p>
              <h6 class="mb-0" style="height: 50px; font-size: 14px"><?php echo $product_name ?></h6>
              <span class="card-text text-sm mb-0 text-muted" style="height: 25px; font-size: 13px">Rs. <?php echo $price ?><?php if($res_product['old_price']<>0){ ?><span class="text-danger" style="font-size: 9px;"> &nbsp; <strike>Rs.<?php echo $res_product['old_price'] ?></strike></span><?php } ?> </span>

              <div class="row px-3 mt-0">
                  <?php

                    $get_like = "SELECT like_index, pro_like, COUNT(pro_like) AS count_like FROM product_like WHERE pro_id = '$res_product[pro_id]' GROUP BY like_index";
                    $run_like  = mysqli_query($con,$get_like );

                    while($res_like  = mysqli_fetch_array($run_like )){

                      switch ($res_like['like_index']) {

                          case "1":
                              $img = 'a.png';
                              break;
                          case "2":
                              $img = 'b.png';
                              break;
                          case "3":
                              $img = 'c.png';
                              break;
                          case "4":
                              $img = 'd.png';
                              break;
                          default:
                              $img = 'e.png';
                      }

                  ?>
                    <span><img src="img/like/<?php echo $img ?>" class="rounded-circle" width="12" height="12" style="margin-right: 1px"/></span>
                  <?php  } ?>

                  <?php

                    $get_lik = "SELECT pro_like, COUNT(pro_like) AS count FROM product_like WHERE pro_id = '$res_product[pro_id]'";
                    $run_lik  = mysqli_query($con,$get_lik);

                    while($res_lik  = mysqli_fetch_array($run_lik)){

                  ?>
                  <?php if($res_lik['count']>0){ ?>
                    <span class="text-muted p-1" style="font-size: 9px; margin-top: -1px"> <?php echo $res_lik['count']; } ?></span>
                  <?php  } ?>
                </div>

                <div class="row my-0"> 
                <?php

                    $get_comment = "SELECT COUNT(comment) AS count_comment FROM comment WHERE pro_id = '$res_product[pro_id]'";
                    $run_comment  = mysqli_query($con,$get_comment);

                    while($res_comment  = mysqli_fetch_array($run_comment)){

                  ?>
                  <?php if($res_comment['count_comment']>0){ 


                    $get_lik = "SELECT pro_like, COUNT(pro_like) AS count FROM product_like WHERE pro_id = '$res_product[pro_id]'";
                    $run_lik  = mysqli_query($con,$get_lik);

                    while($res_lik  = mysqli_fetch_array($run_lik)){ ?>

                    <span class="text-muted <?php if($res_lik['count']==0)echo 'mt-4'; ?>" style="font-size: 10px; margin-left: 95px"> <?php echo $res_comment['count_comment']; echo ' Comments'; } } ?></span>
                  <?php  } ?>                 
                </div>

              </div>

              <div class="mt-0">
                <a class="btn pt-1 m-0 px-0" href="product_detail.php?id=<?php echo $pro_id ?>&&name=<?php echo $product_name ?>" style="width: 100%; background-color: gray; color: #fff; font-size: 11px">View</a>
              </div>

            </div>

          <?php } } ?>
          
        </div>

      </div><!-- sidebar end-->
    </div><!-- ends container-->

  <div class="container mb-5 pb-2">
    <ul class="pagination" style="float: right">
        <li class="page-item"><a class="page-link text-light" href="filter_by_occasion.php?page=1&&occasion=<?php echo $_GET['occasion'] ?>" aria-label="First" style="background-color: gray"><span aria-hidden="true">First</span></a></li>

  <?php

  for ($page=1; $page <=$num_of_pages ; $page++) { 
  
  ?>
        <li class="page-item"><a class="page-link bg-danger text-light" href="filter_by_occasion.php?page=<?php echo $page ?>&&occasion=<?php echo $_GET['occasion'] ?>"><?php echo $page ?></a></li>

  <?php

  } 

  ?>
      <li class="page-item"><a class="page-link text-light" href="filter_by_occasion.php?page=<?php echo $num_of_pages ?>&&occasion=<?php echo $_GET['occasion'] ?>" aria-label="Last" style="background-color: gray"><span aria-hidden="true">Last</span></a></li>
    </ul>
  </div>

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>