<?php 
	include "include/head.php";
	include "include/navbar.php";
?>
	
	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light mb-5">
	    <li class="breadcrumb-item"><a href="index.php.php" style="color: #422C2F; font-weight: 500">Home</a></li>
      <li class="breadcrumb-item"><a href="shop.php" style="color: #422C2F; font-weight: 500">shop</a></li>
      <li class="breadcrumb-item">Handloom saree</li>
	    <li class="breadcrumb-item active" aria-current="page">Dark blue handloom saree</li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

    <div class="container"><!-- starts container-->
      <div class="row">
        <div class="col-1"></div>
        <div class="col-3 ml-4">
        <?php

          $id = $_GET['id'];

          $get_product = "SELECT * FROM product WHERE id = $id";
          $run_product = mysqli_query($con,$get_product);

          while($res_product = mysqli_fetch_array($run_product)){

            $product_name = $res_product['name'];
            $style = $res_product['style'];
            $status = $res_product['status'];
            $img_front = $res_product['image_front'];
            $img_jacket = $res_product['image_jacket'];
            $img_border = $res_product['image_border'];
            $img_back = $res_product['image_back'];
            $color = $res_product['color'];
            $pro_price = $res_product['price'];
            $desc = $res_product['description'];
            $keywords = $res_product['keywords'];

            if ($res_product['status'] == '1') {

          ?>
          <div class="bzoom_wrap">
            <ul id="bzoom">
                <li>
                    <img class="bzoom_thumb_image border" src="img/product/front/<?php echo $img_front ?>" title="first img"/>
                    <img class="bzoom_big_image border" src="img/product/front/<?php echo $img_front ?>"/>
                </li>
                <li>
                    <img class="bzoom_thumb_image border" src="img/product/jacket/<?php echo $img_jacket ?>"/>
                    <img class="bzoom_big_image border" src="img/product/jacket/<?php echo $img_jacket ?>"/>
                </li>
                <li>
                    <img class="bzoom_thumb_image border" src="img/product/border/<?php echo $img_border ?>"/>
                    <img class="bzoom_big_image border" src="img/product/border/<?php echo $img_border ?>"/>
                </li>
                <li>
                    <img class="bzoom_thumb_image border" src="img/product/back/<?php echo $img_back ?>"/>
                    <img class="bzoom_big_image border" src="img/product/back/<?php echo $img_back ?>"/>
                </li>
          </div>
        </div>

        <div class="col-6 ml-5 bg-light" style="min-height: 500px">
          <h1 class="text-muted mt-3"><?php echo $product_name ?></h1>
          <h6 class="text-muted mb-3 ">Product ID : <?php echo $res_product['pro_id']; ?></h6>
          <div class="dropdown-divider mt-3 mb-3"></div>
          <h4 class="mb-3 mt-3" style="color:#D95F2A"><b>Rs. <?php echo $pro_price ?></b></h4> 
          <p class="text-muted text-justify"><?php echo $desc?></p>
          <p class="text-muted" style="font-size: 14px; font-weight: 500; color: #353535"><b>Type :</b> Bansari</p>
          <p class="text-muted" style="font-size: 14px; font-weight: 500; color: #353535"><b>Color :</b> <?php echo $color ?></p>
          <p class="text-muted" style="font-size: 14px; font-weight: 500; color: #353535"><b>Wash and Care :</b> <?php echo $res_product['wash_care'] ?></p>
          <p class="text-muted" style="font-size: 14px; font-weight: 500; color: #353535"><b>Fabric :</b> <?php echo $res_product['fabric'] ?> </p>
          <p class="text-muted" style="font-size: 14px; font-weight: 500; color: #353535"><b>Occasion :</b> <?php echo $res_product['occasion'] ?></p>
          <p class="text-muted" style="font-size: 14px; font-weight: 500; color: #353535"><b>Dimension:</b> <?php echo $res_product['dimension'] ?></p>

          <div class="dropdown-divider mt-3 mb-3"></div>
          <i class="fa fa-smile-o fa-2x mx-2" aria-hidden="true"></i>
          <i class="fa fa-smile-1 fa-2x mx-2" aria-hidden="true"></i>
          <div class="dropdown-divider mt-3 mb-3"></div>
          <button class="btn btn-danger mb-3" type="button" data-toggle="modal" data-target="#Modal1<?php echo $id ?>" style="width: 100%">Inquiry</button> 
        </div>
        <?php } } ?>

      </div>
    </div>
    <!-- ends container-->
    
  <!-- popper for Inqiry form -->
  <div class="modal fade" id="Modal1<?php echo $id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true ">
    <div class="modal-dialog" role="document ">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Inquery of <?php echo $product_name ?></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true ">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form method="POST" action="contact_form.php">
                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="fname" placeholder="First name" required>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" name="lname" placeholder="Last name" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="email" class="form-control" name="mail" placeholder="Email" required>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <input type="text" class="form-control" name="subject" placeholder="Subject" value="<?php echo $product_name.' - color: '.$color ?>" required>
                        </div>
                    </div>

                    <div class="row mb-4" id="map">
                        <div class="col">
                            <textarea class="form-control" placeholder="Massage" name="msg" style="height:150px" required></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                        <a href="#"><input type="submit" class="btn mb-4" value="Send" name="submit" style="background-color:#EF5B00; color: #fff; width:100%"></a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
  </div>
  <!-- popper for Inqiry form -->


    <div class="container px-5 mt-5 mb-3"><!-- starts container-->
      <h4 class="text-muted text-center">Related Products</h4>
      <hr width="50px" style="border-style: solid; border-width: 1px; border-color: #E05656">

      <div class="row mx-0 px-1 mt-4">
        <div class="owl-carousel owl-theme">

          <?php

          $get_style = "SELECT * FROM product WHERE id = $id";
          $run_style = mysqli_query($con,$get_style);

          while($res_style = mysqli_fetch_array($run_style)){

              $product_style = $res_style['style'];

              $get_related_product = "SELECT * FROM product WHERE style = '$product_style' AND id<>'$id'";
              $run_related_product = mysqli_query($con,$get_related_product);

              while($res_related_product = mysqli_fetch_array($run_related_product)){

                $pro_id = $res_related_product ['id'];
                $product_name = $res_related_product ['name'];
                $style = $res_related_product ['style'];
                $img_front = $res_related_product ['image_front'];
                $pro_price = $res_related_product ['price'];

                if ($res_related_product['status'] == '1') {

          ?>

          <div class="card my-2 border">
            <img src="img/product/front/<?php echo $img_front ?>" alt="Card image cap" width="149px">
            <div class="px-3 py-2">
              <p class="text-muted text-sm mb-0" style="height: 25px; font-size: 13px"><?php echo $style ?></p>
              <h6 class="mb-0" style="height: 50px; font-size: 14px"><?php echo $product_name ?></h6>
              <p class="card-text text-sm mb-1 text-muted" style="height: 25px; font-size: 13px">Rs. <?php echo $pro_price ?></p>
              <div>

                <a class="btn py-1" href="productDetail.php?id=<?php echo $pro_id ?>" style="width: 100%; background-color: gray; color: #fff; font-size: 11px">View</a>
                <!-- <a href="" class="btn-arrivals btn-danger btn-sm" style="font-size: 11px">Inquiry</a> -->

              </div>
            </div>
          </div>

          <?php } } } ?>

        </div>
      </div>
    </div><!-- ends container-->  

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>