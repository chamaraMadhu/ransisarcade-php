<?php 
	include "include/head.php";
	include "include/navbar.php";
?>

    <!-- starts jumbotron -->
    <div class="jumbotron mb-0" style="background-image:url(img/page_banner/homeBanner.jpg); background-size: cover; width: 100% min-height: 350px">

        <div class="col-4 offset-6 mt-2">
          <h1>Women's Sarees Collection</h1>
          <p class="lead col-sm mb-0">Exclusive sarees from </p>
          <p class="lead col-sm mb-1">India and Bangaladesh</p>
          <button class="btn btn-md btn-danger mt-2 ml-3"><a class="text-light" href="shop.php" style="text-decoration: none">Shop Now</a></button>
        </div>

    </div>
    <!-- ends jumbotron -->

    <!-- starts breadcrumb -->
    <nav aria-label="breadcrumb breadcrumb">
      <ol class="breadcrumb mb-0 bg-light">
        <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="index.php" style="text-decoration: none">Home</a></li>
      </ol>
    </nav>
    <!-- ends breadcrumb -->

    <!-- start categories -->
    <div class="container px-1"><!-- starts container-->
      <h5 class="text-muted mb-3 mt-4 text-uppercase"><i class="fa fa-th text-danger" aria-hidden="true"></i> Category </h5>
      <div class="row mx-0">
        <div class="owl-carousel owl-theme">

          <div class="item">
            <div class="card px-0 my-2 border bg-danger">
              <a href="#"><img src="img/style/1.1.jpg" alt="Card image cap" width="158px" height="210px"></a>
              
              <div style="background-color: gray; height: 40px">
                <a href="#" style="text-decoration: none"><p class="card-title text-center py-2" style="color: #fff; font-size: 13px">Bangalore Silk</p></a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div><!-- end categories -->
    <!-- end categories -->

    <!-- start categories -->
    <div class="container px-1"><!-- starts container-->
      <h5 class="text-muted mb-3 mt-4 text-uppercase"><i class="fa fa-th text-danger" aria-hidden="true"></i> Fabric </h5>
      <div class="row mx-0">
        <div class="owl-carousel owl-theme">

          <div class="item">
            <div class="card px-0 my-2 border bg-danger">
              <a href="filter_by_fabric.php?fabric=<?php echo $fabric ?>"><img src="img/fabric/2.1.jpg" alt="Card image cap" width="158px" height="210px"></a>
              
              <div class="" style="background-color: gray; height: 40px">
                <a href="filter_by_fabric.php?fabric=<?php echo $fabric ?>" style="text-decoration: none"><p class="card-title text-center py-2" style="color: #fff; font-size: 13px"><?php echo $fabric ?></p></a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div><!-- end categories -->
    <!-- end categories -->

    <!-- start new arrivals -->
    <div class="container mt-4 mb-4 px-1"><!-- starts container-->
      <h5 class="text-muted  mb-4 text-uppercase"><i class="fa fa-th text-danger" aria-hidden="true"></i> New Arrivals</h5>
      <div class="row mx-0">
        <div class="owl-carousel owl-theme">

          <div class="card">
            <a href="#"><img class="card-img-top" src="img/product/front/3.1.jpg" alt="Card image cap" width="100%" style="height: 210px"></a>
            <div class="card-body px-3 pt-1" style="height: 145px">
              <p class="text-muted text-sm mb-0 pt-1" style="height: 25px; font-size: 13px">Bangalore Silk</p>
              <h6 class="mb-0" style="height: 50px; font-size: 14px">Cotton Silk Maheshwari Woven Saree</h6>
              <p class="card-text text-sm mb-2 text-muted" style="height: 25px; font-size: 13px">Rs.18000.00 <span class="text-danger" style="font-size: 9px;"> <strike>Rs.15000.00 </strike></span></p>

              <div class="row px-3 mt-1">
                  <span><img src="img/like/a.png" class="rounded-circle"  style="margin-right: 1px; width: 12px; float: left"/></span>

                  <span class="text-muted" style="font-size: 9px; margin-top: -3px; padding-left: 1px">2</span>
              </div>

              <div class="row px-3"> 
                  <span class="text-muted" style="font-size: 10px; margin-left: 75px">2 Comments</span>                
              </div>

            </div>

            <div class="mt-0">
              <a class="btn pt-1 m-0 px-0" href="#" style="width: 100%; background-color: gray; color: #fff; font-size: 11px">View</a>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end new arrivals -->

    <!-- liked -->
    <div class="container mt-4 mb-4 px-1">
      <h5 class="text-muted  mb-4 text-uppercase"><i class="fa fa-th text-danger" aria-hidden="true"></i> Top Liked</h5>
      <div class="row mx-0">
        <div class="owl-carousel owl-theme">

          <div class="card">
            <a href="#"><img class="card-img-top" src="img/product/front/4.1.jpg" alt="Card image cap" width="100%" style="height: 210px"></a>
            <div class="card-body px-3 pt-1" style="height: 145px">
              <p class="text-muted text-sm mb-0 pt-1" style="height: 25px; font-size: 13px">Bangalore Silk</p>
              <h6 class="mb-0" style="height: 50px; font-size: 14px">Cotton Silk Maheshwari Woven Saree</h6>
              <p class="card-text text-sm mb-2 text-muted" style="height: 25px; font-size: 13px">Rs.18000.00<span class="text-danger" style="font-size: 11px;"> <strike>Rs.15000.00 </strike></span></p>

              <div class="row px-3 mt-1">
                  <span><img src="img/like/b.png" class="rounded-circle"  style="margin-right: 1px; width: 12px; float: left"/></span>

                  <span class="text-muted" style="font-size: 9px; margin-top: -3px; padding-left: 1px"> 
              </div>

              <div class="row px-3"> 
                  <span class="text-muted mt-4" style="font-size: 10px; margin-left: 75px">2 Comments</span>               
              </div>

            </div>

            <div class="mt-0">
              <a class="btn pt-1 m-0 px-0" href="#" style="width: 100%; background-color: gray; color: #fff; font-size: 11px">View</a>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!--  liked -->

    <!-- starts advertisement -->
    <div class="container-fluid bg-white">
      <h4 class="text-muted mt-0 mb-3 mt-3 text-uppercase">Advertisements</h4>

      <div class="row mb-3">
        <div class="col">
          <a href="img/pic3.jpg">
            <figure class="advert">
              <div class="advert-img border"><img src="img/pic3.jpg" class="figure-img img-fluid rounded"></div>
            </figure>
          </a>
          <figcaption class="figure-caption">30% discount</figcaption>
        </div>

       </div>
    </div>
    <!-- ends advertisement -->

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>