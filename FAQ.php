<?php 
	include "include/head.php";
	include "include/navbar.php";
?>
  
  <!-- starts jumbotron -->
	<div class="jumbotron mb-0" style="background-image:url(img/page_banner/faq.jpg); height: 250px;">
    <div class="container">
        <div class="pt-5">
          <h2 class="text-center" style="color: #fff;">FAQ</h2>
        </div>
  	</div>
  </div>
  <!-- ends jumbotron -->
	
	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="faq.php" style="text-decoration: none">FAQ</a></li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

  <div class="container"><!-- starts container-->
    <section class="text-center mb-5"><!-- starts FAQ intro-->  
      <h3 class="text-muted mb-3 mt-3">Frequenty Asked Quections</h3>
      <p><span style="font-size: 14px; font-weight: 500; color: #6D6D6D">Here you will find all quections you are facing and full knowledge base</span></p>   
    </section><!-- ends FAQ intro-->

    <section"><!-- starts FAQ accordion--> 
      <div id="accordion" class="accordion mb-4">

        <h6 class="text-danger mb-3 mt-3">- About Ransi's Arcade </h6>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse1" aria-expanded="true" style="cursor: pointer">
            <a class="card-title" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Where Is Ransi's Arcade Located?</a>
          </div>

          <div class="card-block collapse" id="collapse1">
            <div class="card-body text-muted">
              The Ransi's Arcade is in Mahaniyara Junction at: No. 72/2, Buthgamuwa Road, Rajagiriya, Sri Lanka which is in front of the "GL Piyadasa Eco Friendly Bird Park, Rajagiriya". 
            </div>
          </div>
        </div>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse2" aria-expanded="true" style="cursor: pointer">
            <a class="card-title" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Are Ransi's Arcade Products Available Internationally?</a>
          </div>

          <div class="card-block collapse" id="collapse2">
            <div class="card-body text-muted">
              No, we are a local business located in Rajagiriya, Sri Lanka.
            </div>
          </div>
        </div>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse3" aria-expanded="true" style="cursor: pointer">
            <a class="card-title" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Are Gift Vouchers Available At The Store?</a>
          </div>

          <div class="card-block collapse" id="collapse3">
            <div class="card-body text-muted">
              Yes. we are selling gift vouchers.
            </div>
          </div>
        </div>

        <h6 class="text-danger mb-3 mt-4">- About Our Products </h6>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse4" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> What Kind of sarees do you have?</a>
          </div>

          <div class="card-block collapse" id="collapse4">
            <div class="card-body text-muted">
              Bangalore Silk, Banarasi Silk, Chanderi, Kanchipuram Silk, Jamdani etc
            </div>
          </div>
        </div>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse5" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Do you manufacture sarees?</a>
          </div>

          <div class="card-block collapse" id="collapse5">
            <div class="card-body text-muted">
              No, we import sarees from India and Bangaladesh.
            </div>
          </div>
        </div>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse14" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Are you selling bridle sarees?</a>
          </div>

          <div class="card-block collapse" id="collapse14">
            <div class="card-body text-muted">
              No, we are not selling bridle sarees.
            </div>
          </div>
        </div>

        <h6 class="text-danger mb-3 mt-4">- Payment Method </h6>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#colla7" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> How can I pay for the products?</a>
          </div>

          <div class="card-block collapse" id="colla7">
            <div class="card-body text-muted pb-2">
              The following payment methods are accepted.

              <ul class="mt-2" style="font-size: 14px">
                <li>Cash</li>
                <li>Credit Cards: Domestic and International Visa, MasterCard and American Express credit cards.</li>
                <li>Debit Cards: Debit cards issued by most of the major banks.</li>
              </ul>
            </div>
          </div>
        </div>

        <h6 class="text-danger mb-3 mt-4">- Delivery </h6>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse7" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Do you have any delivery system?</a>
          </div>

          <div class="card-block collapse" id="collapse7">
            <div class="card-body text-muted">
              No, you have to come our store and buy them.
            </div>
          </div>
        </div>

        <h6 class="text-danger mb-3 mt-4">- Returns & Exchanges </h6>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse8" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Do you allow return and exchange?</a>
          </div>

          <div class="card-block collapse" id="collapse8">
            <div class="card-body text-muted pb-2">
              Yes, but we have following conditions.
              <ul class="mt-2" style="font-size: 14px">
                <li>You must return and exchange the saree within 10 days from the date of delivery.</li>
                <li>Please ensure that the returned merchandize is unused, unworn, unwashed, undamaged and is in a saleable condition.</li>
                <li>We request you to maintain the original packaging of the items to be returned, including the tags.</li>
                <li>Ransi's Arcade reserves the right to reject returns from any particular customer, in case of frequent returns by such customer, which shall mean returns on more than 3 occasions in a financial year.</li>
              </ul>
            </div>
          </div>
        </div>


        <h6 class="text-danger mb-3 mt-4">- Customer Service </h6>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse9" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i>How Do I Contact Customer Service?</a>
          </div>

          <div class="card-block collapse" id="collapse9">
            <div class="card-body text-muted">
              You can contact us through land-line, mobile, email, facebook, contact-form giving in our Contact Us page.
            </div>
          </div>
        </div>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse10" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> Can I Email My Order?</a>
          </div>

          <div class="card-block collapse" id="collapse10">
            <div class="card-body text-muted">
              Yes. You can email us your order at ransir@gmail.com.
            </div>
          </div>
        </div>

        <div class="mb-2 border">
          <div class="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white" data-toggle="collapse"  data-parent="#accordion" href="#collapse11" aria-expanded="true" style="cursor: pointer">
            <a class="card-title text-capitalize" style="font-size: 15px; font-weight: 600; color: #353535"><i class="fa fa-search" aria-hidden="true" style="color: red">. </i> If I Have Problems With My Order, Whom Should I Contact?</a>
          </div>

          <div class="card-block collapse" id="collapse11">
            <div class="card-body text-muted">
              If you have any problems or concerns about your order, please email us at ransir@gmail.com.
            </div>
          </div>
        </div>

      </div>
    </section><!-- ends FAQ accordion-->

  </div><!-- ends container-->

<?php
	include "include/socialMedia.php";
	include "include/footer.php";
?>