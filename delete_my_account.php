<?php 
	include "include/head.php";
	include "include/navbar.php";
?>

<?php
      $select_career_banner = mysqli_query($con,"SELECT career FROM page_banner");
    while($res_career_banner = mysqli_fetch_array($select_career_banner)){

    $career_banner = $res_career_banner['career'];
  }
     ?>

  <!-- starts jumbotron -->
  <div class="jumbotron mb-0" style="background-image:url(img/page_banner/<?php echo $career_banner ?>); height: 250px; width: 100%;">
      <div class="container">
          <div class="pt-5">
            <h1 class="text-center" style="color: #fff">My Account</h1>
          </div>
      </div>
    </div>
    <!-- ends jumbotron -->

	<!-- starts breadcrumb -->
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb bg-light" style="font-size: 15px">
	    <li class="breadcrumb-item"><a href="index.php" style="color: #422C2F; font-weight: 500; text-decoration: none">Home</a></li>
	    <li class="breadcrumb-item active" aria-current="page"><a class="text-muted" href="shop.php" style="text-decoration: none">My Account</a></li>
	  </ol>
	</nav>
  <!-- ends breadcrumb -->

  <!-- my account -->
  <?php include "include/my_account_side_bar.php" ?>

      <div class="col-sm-9 bg-light">
        <div class="my-account-order my-4">
          <h3 class="text-center">Delete My Account</h3>
          <hr>

          <div class="bg-white p-4">
              <div class="">
                <h5 class="text-center">Do you realy want to delete your account?</h5>
              </div>
              <div class="">
                <a href="my_account.php" type="submit" class="btn btn-danger mt-3">No, I Don't Want To Delete</a>
                <a href="#" type="submit" class="btn btn-success mt-3">Yes, I Want To Delete</a>
              </div>
            </form>         
          </div>
        </div>
      </div>  
    </div>    
  </div>
  <!-- my account -->
  


<?php   
	include "include/socialMedia.php";
	include "include/footer.php";
?>